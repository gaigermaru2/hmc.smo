﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Hmc.Smo.Wcf
{
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        WebConfig GetWebConfig();

        #region OrderTemplates
        [OperationContract]
        OrderTemplate CreateOrderTemplate(OrderTemplate orderTemplate);
        [OperationContract]
        OrderTemplate UpdateOrderTemplate(OrderTemplate orderTemplate);
        [OperationContract]
        OrderTemplate DeleteOrderTemplate(OrderTemplate orderTemplate);
        [OperationContract]
        OrderTemplate SelectOrderTemplateByPK(string iD, string customerNo);
        [OperationContract]
        List<OrderTemplateForList> SelectOrderTemplatesForList(string customerNo, bool? allowDownload);

        #endregion


        [OperationContract]
        void _Test();

        [OperationContract]
        void SendEmailForInvoice(Customer c, List<Invoice> items, string fileName);

        #region ReportShipmentBalances
        [OperationContract]
        ReportShipmentBalance CreateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance);
        [OperationContract]
        ReportShipmentBalance UpdateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance);
        [OperationContract]
        ReportShipmentBalance DeleteReportShipmentBalance(ReportShipmentBalance reportShipmentBalance);
        [OperationContract]
        ReportShipmentBalance SelectReportShipmentBalanceByPK(string reportShipmentBalanceID);
        [OperationContract]
        List<ReportShipmentBalance> SelectReportShipmentBalancesForList(SearchShipmentCriteria s);
        #endregion

        //    [OperationContract]
        ////    List<Constant> SelectConstantsForList();
        //[OperationContract]
        //void BAPI_Test();

        [OperationContract]
        Constant SelectConstantByPK(string key);

        #region Errors
        [OperationContract]
        string SelectErrorMsgByPK(string errorCode);
        #endregion

        #region Orders
        [OperationContract]
        Order CreateOrder(Order order, List<OrderItem> orderItems, string tempIdForAttachment);
        [OperationContract]
        Order UpdateOrder(Order order, List<OrderItem> orderItems);
        [OperationContract]
        void MarkOrderStatusToBeCancelled(string orderStatusCode);
        [Obsolete]
        [OperationContract]
        void MarkOrderStatusToBeCompletedWithPdf(string saleConfirmNo, byte[] pdfContent);
        [OperationContract]
        void MarkOrderStatusToBeCompleted(List<SaleConfirmStatusSMO13> saleConfirmNos);
        [OperationContract]
        Order ChangeOrderStatus(string orderNo, string changeBy, string newStatus, string reason);
        [OperationContract]
        Order DeleteOrder(Order order);
        [OperationContract]
        Order SelectOrderByPK(string orderId);
        [OperationContract]
        List<Order> SelectOrdersForList(string customerName, string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo, string saleConfirmNo, string paymentTermCode,
            string productNo, string pONo, string orderNo, string orderStatusCode);
        [OperationContract]
        List<OrderForHMC> SelectOrdersForHMC(string customerName,
            string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo,
            string saleConfirmNoFrom, string saleConfirmNoTo,
            string paymentTermCode, string productNo,
            string pONo, string orderNo, string orderStatusCode,
            string segManagerCodeFrom, string segManagerCodeTo);
        #endregion

        #region OrderItems
        [OperationContract]
        OrderItem CreateOrderItem(OrderItem orderItem);
        [OperationContract]
        OrderItem UpdateOrderItem(OrderItem orderItem);
        [OperationContract]
        OrderItem DeleteOrderItem(OrderItem orderItem);
        [OperationContract]
        OrderItem SelectOrderItemByPK(string orderNo, int itemNo);
        [OperationContract]
        List<OrderItem> SelectOrderItemsByOrderNo(string orderNo);

        #endregion

        #region OrderHistory
        [OperationContract]
        OrderHistory CreateOrderHistory(OrderHistory orderHistory);
        [OperationContract]
        List<OrderHistory> SelectOrderHistoryByOrderNo(string orderNo);
        #endregion

        #region OrderStatus
        [OperationContract]
        OrderStatus CreateOrderStatus(OrderStatus orderStatus);
        [OperationContract]
        OrderStatus UpdateOrderStatus(OrderStatus orderStatus);
        [OperationContract]
        OrderStatus DeleteOrderStatus(OrderStatus orderStatus);
        [OperationContract]
        OrderStatus SelectOrderStatusByPK(string orderStatusCode);
        [OperationContract]
        List<OrderStatus> SelectOrderStatusForList();

        #endregion

        #region Users
        [OperationContract]
        User CreateUser(User user);
        [OperationContract]
        User UpdateUser(User user);
        //[OperationContract]
        //User DeleteUser(User user);
        //[OperationContract]
        //User SelectUserByPK(string customerNo, string username);
        [OperationContract]
        User UserAuthentication(string username, string password);
        [OperationContract]
        User SelectUserByPK(string customerNo, string username);
        [OperationContract]
        User SelectUserCreateByOrderNo(string orderNo);
        #endregion

        #region Customers
        [OperationContract]
        Customer CreateCustomer(Customer customer);
        [OperationContract]
        Customer UpdateCustomer(Customer customer);
        [OperationContract]
        Customer DeleteCustomer(Customer customer);
        [OperationContract]
        Customer SelectCustomerByPK(string customerNo, string saleOrg, string distCh, string division);
        [OperationContract]
        List<Customer> SelectCustomersForList(string customerNo);

        #endregion

        #region Materials
        [OperationContract]
        Material CreateMaterial(Material material);
        [OperationContract]
        Material UpdateMaterial(Material material);
        [OperationContract]
        Material DeleteMaterial(Material material);
        [OperationContract]
        Material SelectMaterialByPK(string materialNo);
        [OperationContract]
        List<Material> SelectMaterialsProductNo(string productNo);
        [OperationContract]
        Material SelectMaterialByByProductNoAndPackageSizeCode(string productNo, string packageSizeCode);

        #endregion

        #region ProductFavs
        [OperationContract]
        ProductFav CreateProductFav(ProductFav productFav);
        [OperationContract]
        ProductFav UpdateProductFav(ProductFav productFav);
        [OperationContract]
        ProductFav DeleteProductFav(ProductFav productFav);
        //[OperationContract]
        //ProductFav SelectProductFavByPK(string customerNo, string productNo, string packageSizeCode, string materialNo);

        #endregion

        #region ProductMaterialMaps
        [OperationContract]
        ProductMaterialMap CreateProductMaterialMap(ProductMaterialMap productMaterialMap);
        [OperationContract]
        ProductMaterialMap UpdateProductMaterialMap(ProductMaterialMap productMaterialMap);
        [OperationContract]
        ProductMaterialMap DeleteProductMaterialMap(ProductMaterialMap productMaterialMap);
        //[OperationContract]
        //ProductMaterialMap SelectProductMaterialMapByPK(string productNo, string packageSizeCode, string materialNo);

        #endregion

        #region Products
        [OperationContract]
        Product CreateProduct(Product product);
        [OperationContract]
        Product UpdateProduct(Product product);
        [OperationContract]
        Product DeleteProduct(Product product);
        [OperationContract]
        Product SelectProductByPK(string productNo);
        [OperationContract]
        List<Product> SelectProductByCustomerNo(string customerNo);
        [OperationContract]
        List<Product> SelectProductExceptCustomerNo(string customerNo);
        #endregion

        #region PackageSizes
        [OperationContract]
        PackageSize CreatePackageSize(PackageSize packageSize);
        [OperationContract]
        PackageSize UpdatePackageSize(PackageSize packageSize);
        [OperationContract]
        PackageSize DeletePackageSize(PackageSize packageSize);
        [OperationContract]
        PackageSize SelectPackageSizeByPK(string packageSizeCode);
        //[OperationContract]
        //List<PackageSize> SelectPackageSizesForList();
        [OperationContract]
        List<PackageSize> SelectPackageSizesByProductNo(string productNo);

        #endregion

        #region PaymentTerms
        [OperationContract]
        PaymentTerm CreatePaymentTerm(PaymentTerm paymentTerm);
        [OperationContract]
        PaymentTerm UpdatePaymentTerm(PaymentTerm paymentTerm);
        [OperationContract]
        PaymentTerm DeletePaymentTerm(PaymentTerm paymentTerm);
        [OperationContract]
        PaymentTerm SelectPaymentTermByPK(string paymentTermCode);
        [OperationContract]
        List<PaymentTerm> SelectPaymentTermByCustomerNo(string customerNo);
        #endregion

        #region CustomerShipToMaps
        [OperationContract]
        CustomerShipToMap CreateCustomerShipToMap(CustomerShipToMap customerShipToMap);
        [OperationContract]
        CustomerShipToMap UpdateCustomerShipToMap(CustomerShipToMap customerShipToMap);
        [OperationContract]
        CustomerShipToMap DeleteCustomerShipToMap(CustomerShipToMap customerShipToMap);
        [OperationContract]
        CustomerShipToMap SelectCustomerShipToMapByPK(string customerNo, string saleOrg, string distCh, string division, string shipToCode);
        [OperationContract]
        List<CustomerShipToMap> SelectCustomerShipToMapsByCustomerNo(string customerNo);

        #endregion

        #region CustomerGroups
        [OperationContract]
        CustomerGroup CreateCustomerGroup(CustomerGroup customerGroup);
        [OperationContract]
        CustomerGroup UpdateCustomerGroup(CustomerGroup customerGroup);
        [OperationContract]
        CustomerGroup DeleteCustomerGroup(CustomerGroup customerGroup);
        [OperationContract]
        CustomerGroup SelectCustomerGroupByPK(string customerGroupCode);
        [OperationContract]
        List<CustomerGroup> SelectCustomerGroupsForList();

        #endregion

        #region CustomerPaymentTermMaps
        [OperationContract]
        CustomerPaymentTermMap CreateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap);
        [OperationContract]
        CustomerPaymentTermMap UpdateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap);
        [OperationContract]
        CustomerPaymentTermMap DeleteCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap);
        [OperationContract]
        //CustomerPaymentTermMap SelectCustomerPaymentTermMapByPK(string customerNo, string paymentTermCode);
        //[OperationContract]
        List<CustomerPaymentTermMap> SelectCustomerPaymentTermMapsForList(string customerNo, string paymentTermCode);

        #endregion

        #region SendEmail

        //[OperationContract]
        //void SendEmailOneTo(string subject, string body, string sentTo);
        //[OperationContract]
        //void SendEmailOneToOneCc(string subject, string body, string sentTo, string cc);
        //[OperationContract]
        //void SendEmailOneToMultiCc(string subject, string body, string sentTo, string[] ccs);
        //[OperationContract]
        //void SendEmail(string subject, string body, string[] sentTo, string[] ccs);
        [OperationContract]
        void SendMail();

        /// <summary>
        ///Case	Responsible Action	Flow	                            To.	        Cc.
        ///1	Requestor	Draft	Req 	                            -	        -
        ///2	Requestor	Cancel	Req  End	                        -	        -
        ///3	Requestor	Submit	Req  APV	APV	Req
        ///4	Approver	Submit	Req  APV  HMC	                    Sales Co.   Req, APV
        ///5	HMC	        Submit	Req  APV  HMC SAP	            Sales Co.	-
        ///6	Segment     Approve	Req  APV  HMC SAP   SMO	        Req, APV	Sales Co.
        ///7	Approver	Reject	Req  APV  Req	                    Req	        APV
        ///8	Requestor	Cancel	Req  APV  Req  End	            APV	        Req
        ///9	Approver	Cancel	Req  APV  End	                    Req	        APV
        ///10	Sales Co.	Reject	Req  APV  HMC  Req	            Req	        APV, Sales Co.
        ///11	Requestor	Cancel	Req  APV  HMC  Req  End	        APV	        Req, Sales Co.
        ///12	Approver	Cancel	Req  APV  HMC  Req  APV  End	Req	        APV, Sales Co.
        ///13	Sales Co.	Cancel	Req  APV  HMC  End	            Req,APV	    Sales Co.
        /// </summary>
        /// <param name="caseNo"></param>
        /// <param name="currentUser"></param>
        /// <param name="order"></param>
        /// <param name="items"></param>
        /// <param name="redirectUrl"></param>
        [OperationContract]
        void SendEmailByCaseNo(int caseNo, User currentUser, Order order, List<OrderItem> items, string redirectUrl, byte[] pdf);
        #endregion

        #region SegmentManager
        [OperationContract]
        SegmentManager CreateSegmentManager(SegmentManager segmentManager);
        [OperationContract]
        SegmentManager UpdateSegmentManager(SegmentManager segmentManager);
        [OperationContract]
        SegmentManager DeleteSegmentManager(SegmentManager segmentManager);
        //[OperationContract]
        //SegmentManager SelectSegmentManagerByPK(string segmentManagerCode);
        [OperationContract]
        List<SegmentManager> SelectSegmentManagersForList();

        #endregion


        #region SAP

        [OperationContract]
        RS_ZIDOC_INPUT_QUOTATION BAPI_ZIDOC_INPUT_QUOTATION(RS_ZIDOC_INPUT_QUOTATION val);
        [OperationContract]
        RS_ZIDOC_INPUT_QUOTATION BAPI_ZIDOC_INPUT_QUOTATION_TEST();

        #endregion

        #region CreateOrModify

        [OperationContract]
        void CreateOrModifyPaymentTerms(List<PaymentTerm> paymentTerms);
        [OperationContract]
        void CreateOrModifyPackageSizes(List<PackageSize> packageSizes);
        [OperationContract]
        void CreateOrModifyCustomerGroups(List<CustomerGroup> customerGroups);
        [OperationContract]
        void CreateOrModifySegmentManagers(List<SegmentManager> segmentManagers);
        [OperationContract]
        void CreateOrModifyCustomers(List<Customer> customers);
        [OperationContract]
        void CreateOrModifyCustomerShipToMaps(List<CustomerShipToMap> customerShipToMaps);
        [OperationContract]
        void CreateOrModifyCustomerPaymentTermMaps(List<CustomerPaymentTermMap> customerPaymentTermMaps);
        [OperationContract]
        void CreateOrModifyMaterials(List<Material> materials);
        [OperationContract]
        void CreateOrModifyProducts(List<Product> products);
        [OperationContract]
        void CreateOrModifyProductMaterialMaps(List<ProductMaterialMap> productMaterialMaps);
        [OperationContract]
        void CreateOrModifyProductFavs(List<ProductFav> productFavs);
        [OperationContract]
        void CreateOrModifyUsers(List<User> users);
        [OperationContract]
        string CreateOrModifyReportShipmentDetails(List<ReportShipmentDetail> reportShipmentDetail, bool? isSendMail);
        [OperationContract]
        void CreateOrModifyReportShipmentBalances(List<ReportShipmentBalance> reportShipmentBalance);
        [OperationContract]
        void CreateOrModifyMailPDF(MailPDF mailPDF);
        #endregion


        #region SendMails
        [OperationContract]
        SendMail CreateSendMail(SendMail sendMail);
        [OperationContract]
        SendMail UpdateSendMail(SendMail sendMail);
        [OperationContract]
        SendMail DeleteSendMail(SendMail sendMail);
        [OperationContract]
        SendMail SelectSendMailByPK(string sendMailId);
        [OperationContract]
        List<SendMail> SelectSendMailsForList(bool isSent);

        #endregion

        #region MailPDFs
        [OperationContract]
        MailPDF CreateMailPDF(MailPDF mailPDF);
        [OperationContract]
        MailPDF DeleteMailPDF(MailPDF mailPDF);
        [OperationContract]
        MailPDF SelectMailPDFByPK(string saleConfirmNo);
        [OperationContract]
        List<MailPDFSelectForList> SelectMailPDFsSelectForList(string invoice);
        #endregion


        #region Attachments
        [OperationContract]
        Attachment CreateAttachment(Attachment attachment);
        [OperationContract]
        Attachment UpdateAttachment(Attachment attachment);
        [OperationContract]
        Attachment DeleteAttachment(Attachment attachment);
        [OperationContract]
        Attachment DeleteAttachmentByPK(string attachmentId);
        [OperationContract]
        Attachment SelectAttachmentByPK(string attachmentId);
        [OperationContract]
        List<AttachmentForList> SelectAttachmentsForList(string orderNo, string note);

        #endregion



        [OperationContract]
        decimal SelectTaxPercentByTaxClass(string taxClass);


        #region ReportShipmentDetails
        [OperationContract]
        ReportShipmentDetail CreateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail);
        [OperationContract]
        ReportShipmentDetail UpdateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail);
        [OperationContract]
        ReportShipmentDetail DeleteReportShipmentDetail(ReportShipmentDetail reportShipmentDetail);
        [OperationContract]
        ReportShipmentDetail SelectReportShipmentDetailByPK(string reportShipmentDetailID);
        [OperationContract]
        List<ReportShipmentDetailForList> SelectReportShipmentDetailsForList(SearchShipmentCriteria s);

        #endregion

    }


    [DataContract]
    public class RS_ZIDOC_INPUT_QUOTATION
    {
        [DataMember]
        public Order Order { get; set; }
        [DataMember]
        public List<OrderItem> OrderItems { get; set; }
        [DataMember]
        public Customer Customer { get; set; }
        [DataMember]
        public User User { get; set; }

        [DataMember]
        public string IDOC_QUOTATION { get; set; }
        [DataMember]
        public string IDOC_MESSAGE { get; set; }


        [DataMember]
        public string TEXT_HD { get; set; }
        [DataMember]
        public List<string> TEXT_IT { get; set; }
    }
}
