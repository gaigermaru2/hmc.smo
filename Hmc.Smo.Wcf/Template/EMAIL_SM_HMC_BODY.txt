﻿<HeaderEN>
<br>
We are pleased to inform you that your order via SmartOrder No. <DocNo> has been submitted to HMC and is being reviewed.
<br>
<br>
บริษัทฯ มีความยินดีจะแจ้งให้ท่านทราบว่าเอกสารการสั่งซื้อเลขที่ <DocNo>  ได้ถูกส่งให้กับบริษัทฯ แล้ว และอยู่ระหว่างการพิจารณาของบริษัทฯ
<br>
<br>
<LineItems>
<FooterEN>