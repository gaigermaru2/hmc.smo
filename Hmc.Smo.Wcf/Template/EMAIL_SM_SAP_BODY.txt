﻿Dear All,
<br>
<br>
This notification confirms that SmartOrder <DocNo> has been posted to SAP.
<br>
<br>
เอกสารการสั่งซื้อเลขที่ <DocNo>  ได้ถูกส่งเข้าระบบ SAP แล้ว
<br>
<br>
Sales Confirmation no. <SaleConfirmNo>
<br>
<br>
<LineItems>
<FooterEN>