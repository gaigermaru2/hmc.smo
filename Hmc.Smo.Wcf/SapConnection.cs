﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SAP.Middleware.Connector;

namespace Hmc.Smo.Wcf
{
    public class SapConnection
    {
        private RfcDestination RfcDest { get; set; }
        private RfcRepository RfcRep { get; set; }
        public IRfcFunction Function { get; set; }

        public void ConnectToSap()
        {
            RfcConfigParameters rfc = new RfcConfigParameters();

            SmoBC da = new SmoBC();

            var SAP_NAME = da.SelectConstantByPK("SAP_NAME").Value;
            var SAP_SYSID = da.SelectConstantByPK("SAP_SYSID").Value;
            var SAP_ASHOST = da.SelectConstantByPK("SAP_ASHOST").Value;
            var SAP_CLIENT = da.SelectConstantByPK("SAP_CLIENT").Value;
            var SAP_USER = da.SelectConstantByPK("SAP_USER").Value;
            var SAP_PASSWD = da.SelectConstantByPK("SAP_PASSWD").Value;
            var SAP_SYSNR = da.SelectConstantByPK("SAP_SYSNR").Value;
            var SAP_LANG = da.SelectConstantByPK("SAP_LANG").Value;
            var SAP_POOL_SIZE = da.SelectConstantByPK("SAP_POOL_SIZE").Value;
            var SAP_IDLE_TIMEOUT = da.SelectConstantByPK("SAP_IDLE_TIMEOUT").Value;

            rfc.Add(RfcConfigParameters.Name, SAP_NAME);
            rfc.Add(RfcConfigParameters.SystemID, SAP_SYSID);
            rfc.Add(RfcConfigParameters.AppServerHost, SAP_ASHOST);
            rfc.Add(RfcConfigParameters.Client, SAP_CLIENT);
            rfc.Add(RfcConfigParameters.User, SAP_USER);
            rfc.Add(RfcConfigParameters.Password, SAP_PASSWD);
            rfc.Add(RfcConfigParameters.SystemNumber, SAP_SYSNR);
            rfc.Add(RfcConfigParameters.Language, SAP_LANG);
            rfc.Add(RfcConfigParameters.PoolSize, SAP_POOL_SIZE);
            rfc.Add(RfcConfigParameters.IdleTimeout, SAP_IDLE_TIMEOUT);

            this.RfcDest = RfcDestinationManager.GetDestination(rfc);
            this.RfcRep = RfcDest.Repository;
        }

        public void CreateFunction(string functionName)
        {
            Function = RfcRep.CreateFunction(functionName);
        }

        public void ExcFunction()
        {
            Function.Invoke(this.RfcDest);
        }

        public SapConnection(string bapi)
        {
            this.ConnectToSap();
            this.CreateFunction(bapi);
        }
    }
}