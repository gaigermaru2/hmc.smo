﻿using Hmc.Smo.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text.RegularExpressions;
using System.Transactions;

namespace Hmc.Smo.Wcf
{
    public class SmoBC
    {
        #region OrderTemplates
        public OrderTemplate CreateOrderTemplate(OrderTemplate orderTemplate)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string iD = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                orderTemplate.ID = iD;
                orderTemplate = da.CreateOrderTemplate(orderTemplate);

                ts.Complete();
                return orderTemplate;
            }
        }
        public OrderTemplate UpdateOrderTemplate(OrderTemplate orderTemplate)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderTemplate = da.UpdateOrderTemplate(orderTemplate);

                ts.Complete();
                return orderTemplate;
            }
        }
        public OrderTemplate DeleteOrderTemplate(OrderTemplate orderTemplate)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderTemplate = da.DeleteOrderTemplate(orderTemplate);

                ts.Complete();
                return orderTemplate;
            }
        }
        public OrderTemplate SelectOrderTemplateByPK(string iD, string customerNo)
        {
            return new SmoDA().SelectOrderTemplateByPK(iD, customerNo);
        }
        public List<OrderTemplateForList> SelectOrderTemplatesForList(string customerNo, bool? allowDownload)
        {
            return new SmoDA().SelectOrderTemplatesForList(customerNo, allowDownload);
        }

        #endregion


        #region ReportShipmentBalances
        public ReportShipmentBalance CreateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string reportShipmentBalanceID = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                reportShipmentBalance.ReportShipmentBalanceID = reportShipmentBalanceID;
                reportShipmentBalance = da.CreateReportShipmentBalance(reportShipmentBalance);

                ts.Complete();
                return reportShipmentBalance;
            }
        }
        public ReportShipmentBalance UpdateReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                reportShipmentBalance = da.UpdateReportShipmentBalance(reportShipmentBalance);

                ts.Complete();
                return reportShipmentBalance;
            }
        }
        public ReportShipmentBalance DeleteReportShipmentBalance(ReportShipmentBalance reportShipmentBalance)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                reportShipmentBalance = da.DeleteReportShipmentBalance(reportShipmentBalance);

                ts.Complete();
                return reportShipmentBalance;
            }
        }
        public ReportShipmentBalance SelectReportShipmentBalanceByPK(string reportShipmentBalanceID)
        {
            return new SmoDA().SelectReportShipmentBalanceByPK(reportShipmentBalanceID);
        }
        public List<ReportShipmentBalance> SelectReportShipmentBalancesForList(SearchShipmentCriteria s)
        {
            return new SmoDA().SelectReportShipmentBalancesForList(s);
        }

        #endregion


        #region Constants

        //public List<Constant> SelectConstantsForList()
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectConstantsForList();
        //}


        public Constant SelectConstantByPK(string key)
        {
            SmoDA da = new SmoDA();
            return da.SelectConstantByPK(key);
        }

        #endregion Constants

        #region Errors
        public string SelectErrorMsgByPK(string errorCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectErrorMsgByPK(errorCode);
        }
        #endregion

        #region Orders

        public Order CreateOrder(Order order, List<OrderItem> orderItems, string tempIdForAttachment)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                string orderNo = da.GetNoAutonumbering();

                order.OrderNo = orderNo;
                order = da.CreateOrder(order);

                if (orderItems != null && orderItems.Count > 0)
                {
                    foreach (var op in orderItems)
                    {
                        op.OrderNo = orderNo;
                        da.CreateOrderItem(op);
                    }
                }

                if (!string.IsNullOrEmpty(tempIdForAttachment))
                {
                    var atts = da.SelectAttachmentsByNote(tempIdForAttachment);

                    foreach (var att in atts)
                    {
                        att.Note = null;
                        att.OrderNo = orderNo;

                        da.UpdateAttachment(att);
                    }
                }

                ts.Complete();
                return order;
            }
        }
        public Order ChangeOrderStatus(string orderNo, string changeBy, string newStatus, string reason)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                var order = da.SelectOrderByPK(orderNo);

                DateTime d = DateTime.Now;

                if (newStatus == "4" && !order.PricingDate.HasValue)
                {
                    order.PricingDate = d;

                    DateTime dValidity = GetValidityDate(order.PricingDate.Value);
                    order.ValidityDate = dValidity;
                }

                order.OrderStatusCode = newStatus;
                order = da.UpdateOrder(order);

                OrderHistory h = new OrderHistory()
                {
                    OrderNo = order.OrderNo,
                    ChangeBy = changeBy,
                    ChangeTime = d,
                    OrderStatusCode = newStatus,
                    OrderStatusText = newStatus,
                    Reason = reason
                };
                da.CreateOrderHistory(h);

                ts.Complete();
                return order;
            }
        }

        private static DateTime GetValidityDate(DateTime pricingDate)
        {
            DateTime dValidity;
            int cD = pricingDate.Day;

            if (cD <= 20)
            {
                var nextMonth = DateTime.Today.AddMonths(1);
                var firstOfNextMonth = new DateTime(nextMonth.Year, nextMonth.Month, 1);
                dValidity = firstOfNextMonth.AddDays(-1);
            }
            else
            {
                var next2Month = DateTime.Today.AddMonths(2);
                var firstOfNext2Month = new DateTime(next2Month.Year, next2Month.Month, 1);
                dValidity = firstOfNext2Month.AddDays(-1);
            }
            return dValidity;
        }

        public void MarkOrderStatusToBeCancelled(string orderStatusCode)
        {
            //SmoDA da = new SmoDA();
            //List<string> orderNos = new List<string>();
            //List<string> tos = new List<string>();
            //List<string> ccs = new List<string>();
            //var orders = da.SelectOrdersForList(null, null, null, null, null, null, null, null, null, null, orderStatusCode);

            //using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            //{
            //    foreach (var order in orders)
            //    {
            //        order.OrderStatusCode = OrderStatusCode.Canceled;
            //        da.UpdateOrder(order);

            //        OrderHistory oh = new OrderHistory()
            //        {
            //            OrderNo = order.OrderNo,
            //            OrderStatusCode = OrderStatusCode.Canceled,
            //            OrderStatusText = OrderStatusCode.Canceled,
            //            Reason = "Order has been automatically canceled by SYSTEM",
            //            ChangeBy = "SYSTEM",
            //            ChangeTime = DateTime.Now
            //        };

            //        da.CreateOrderHistory(oh);
            //    }

            //    ts.Complete();
            //}

            //foreach (var order in orders)
            //{
            //    SendEmailByCaseNo(20, null, order, null, string.Empty);
            //}
        }

        public void MarkOrderStatusToBeCompletedWithPdf(string saleConfirmNo, byte[] pdfContent)
        {
            SmoDA da = new SmoDA();
            List<string> orderNos = new List<string>();
            List<string> tos = new List<string>();
            List<string> ccs = new List<string>();
            Order order = new Order();

            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                order = da.SelectOrdersForList(null, null, null, null, null, saleConfirmNo, null, null, null, null, "0").FirstOrDefault();

                if (order != null)
                {
                    order.OrderStatusCode = OrderStatusCode.QuotationApprove;
                    da.UpdateOrder(order);

                    OrderHistory oh = new OrderHistory()
                    {
                        OrderNo = order.OrderNo,
                        OrderStatusCode = OrderStatusCode.QuotationApprove,
                        OrderStatusText = OrderStatusCode.QuotationApprove,
                        Reason = "Order has been automatically completed by SYSTEM",
                        ChangeBy = "SYSTEM",
                        ChangeTime = DateTime.Now
                    };

                    da.CreateOrderHistory(oh);
                }

                ts.Complete();
            }

            var lines = da.SelectOrderItemsByOrderNo(order.OrderNo);
            SendEmailByCaseNo(6, null, order, lines, string.Empty, pdfContent, "");
        }

        public void MarkOrderStatusToBeCompleted(List<SaleConfirmStatusSMO13> saleConfirmNos)
        {
            SmoDA da = new SmoDA();
            List<string> orderNos = new List<string>();
            List<string> tos = new List<string>();
            List<string> ccs = new List<string>();
            List<Order> orders = new List<Order>();

            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                foreach (var it in saleConfirmNos)
                {
                    string statusWeb = null;
                    string reason = null;
                    if (it.Status == "006")
                    {
                        statusWeb = OrderStatusCode.QuotationApprove;
                        reason = "Order has been automatically quotation approved by SYSTEM";
                    }
                    else if (it.Status == "007")
                    {
                        statusWeb = OrderStatusCode.SaleOrderComplete;
                        reason = "Order has been automatically sale order completed by SYSTEM";
                    }

                    var order = da.SelectOrdersForList(null, null, null, null, null, it.SaleConfirmNo, null, null, null, null, OrderStatusCode.All).FirstOrDefault();
                    if (order != null && statusWeb != null)
                    {
                        order.OrderStatusCode = statusWeb;
                        da.UpdateOrder(order);
                        order.SaleOrder = it.SaleOrderNo;
                        orders.Add(order);

                        OrderHistory oh = new OrderHistory()
                        {
                            OrderNo = order.OrderNo,
                            OrderStatusCode = statusWeb,
                            OrderStatusText = statusWeb,
                            Reason = reason,
                            ChangeBy = "SYSTEM",
                            ChangeTime = DateTime.Now
                        };

                        da.CreateOrderHistory(oh);
                    }
                }

                ts.Complete();
            }

            foreach (var order in orders)
            {
                if (order.OrderStatusCode == OrderStatusCode.QuotationApprove)
                {
                    var lines = da.SelectOrderItemsByOrderNo(order.OrderNo);
                    SendEmailByCaseNo(6, null, order, lines, string.Empty, null, order.SaleOrder);
                }
            }
        }

        public Order UpdateOrder(Order order, List<OrderItem> orderItems)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                //order.ChangeTime = DateTime.Now;
                order = da.UpdateOrder(order);

                if (orderItems != null && orderItems.Count > 0)
                {
                    da.DeleteOrderItemByOrderNo(order.OrderNo);

                    foreach (var op in orderItems)
                    {
                        op.OrderNo = order.OrderNo;
                        da.CreateOrderItem(op);
                    }
                }

                ts.Complete();
                return order;
            }
        }
        public Order DeleteOrder(Order order)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                order = da.DeleteOrder(order);

                ts.Complete();
                return order;
            }
        }
        public Order SelectOrderByPK(string orderNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectOrderByPK(orderNo);
        }
        public List<Order> SelectOrdersForList(string customerName, string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo, string saleConfirmNo, string paymentTermCode,
            string productNo, string pONo, string orderNo, string orderStatusCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectOrdersForList(customerName, customerNoFrom, customerNoTo, orderDateFrom,
                    orderDateTo, saleConfirmNo, paymentTermCode, productNo, pONo, orderNo, orderStatusCode).ToList();
        }
        public List<OrderForHMC> SelectOrdersForHMC(string customerName,
            string customerNoFrom, string customerNoTo,
            DateTime? orderDateFrom, DateTime? orderDateTo,
            string saleConfirmNoFrom, string saleConfirmNoTo,
            string paymentTermCode, string productNo,
            string pONo, string orderNo, string orderStatusCode,
            string segManagerCodeFrom, string segManagerCodeTo)
        {
            GetFromToString(ref customerNoFrom, ref customerNoTo);
            GetFromToString(ref saleConfirmNoFrom, ref saleConfirmNoTo);
            GetFromToString(ref segManagerCodeFrom, ref segManagerCodeTo);
            GetFromToDate(ref orderDateFrom, ref orderDateTo);

            SmoDA da = new SmoDA();
            return da.SelectOrdersForHMC(customerName, customerNoFrom, customerNoTo,
                orderDateFrom, orderDateTo, saleConfirmNoFrom, saleConfirmNoTo,
                paymentTermCode, productNo, pONo, orderNo, orderStatusCode,
                segManagerCodeFrom, segManagerCodeTo).ToList();
        }

        private static void GetFromToString(ref string from, ref string to)
        {
            string outFrom, outTo;

            if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(to))
            {
                outFrom = null;
                outTo = null;
            }
            else if (string.IsNullOrEmpty(from))
            {
                outFrom = to;
                outTo = to;
            }
            else if (string.IsNullOrEmpty(to))
            {
                outFrom = from;
                outTo = from;
            }
            else
            {
                outFrom = from;
                outTo = to;
            }

            from = outFrom;
            to = outTo;
        }

        private static void GetFromToDate(ref DateTime? from, ref DateTime? to)
        {
            DateTime? outFrom, outTo;

            if (from == null && to == null)
            {
                outFrom = null;
                outTo = null;
            }
            else if (from == null)
            {
                outFrom = to;
                outTo = to;
            }
            else if (to == null)
            {
                outFrom = from;
                outTo = from;
            }
            else
            {
                outFrom = from;
                outTo = to;
            }

            from = outFrom;
            to = outTo;
        }

        #endregion

        #region OrderItems
        public OrderItem CreateOrderItem(OrderItem orderItem)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderItem = da.CreateOrderItem(orderItem);

                ts.Complete();
                return orderItem;
            }
        }
        public OrderItem UpdateOrderItem(OrderItem orderItem)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderItem = da.UpdateOrderItem(orderItem);

                ts.Complete();
                return orderItem;
            }
        }
        public OrderItem DeleteOrderItem(OrderItem orderItem)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderItem = da.DeleteOrderItem(orderItem);

                ts.Complete();
                return orderItem;
            }
        }
        public OrderItem SelectOrderItemByPK(string orderNo, int itemNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectOrderItemByPK(orderNo, itemNo);
        }
        public List<OrderItem> SelectOrderItemsByOrderNo(string orderNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectOrderItemsByOrderNo(orderNo);
        }
        #endregion

        #region OrderHistory
        public OrderHistory CreateOrderHistory(OrderHistory orderHistory)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderHistory = da.CreateOrderHistory(orderHistory);

                ts.Complete();
                return orderHistory;
            }
        }
        public List<OrderHistory> SelectOrderHistoryByOrderNo(string orderNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectOrderHistoryByOrderNo(orderNo);
        }

        #endregion

        #region OrderStatus
        public OrderStatus CreateOrderStatus(OrderStatus orderStatus)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderStatus = da.CreateOrderStatus(orderStatus);

                ts.Complete();
                return orderStatus;
            }
        }
        public OrderStatus UpdateOrderStatus(OrderStatus orderStatus)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderStatus = da.UpdateOrderStatus(orderStatus);

                ts.Complete();
                return orderStatus;
            }
        }
        public OrderStatus DeleteOrderStatus(OrderStatus orderStatus)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                orderStatus = da.DeleteOrderStatus(orderStatus);

                ts.Complete();
                return orderStatus;
            }
        }
        public OrderStatus SelectOrderStatusByPK(string orderStatusCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectOrderStatusByPK(orderStatusCode);
        }
        public List<OrderStatus> SelectOrderStatusForList()
        {
            SmoDA da = new SmoDA();
            return da.SelectOrderStatusForList();
        }

        #endregion

        #region Users
        public User CreateUser(User user)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                user = da.CreateUser(user);

                ts.Complete();
                return user;
            }
        }
        public User UpdateUser(User user)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                user = da.UpdateUser(user);

                ts.Complete();
                return user;
            }
        }
        //public User DeleteUser(User user)
        //{
        //    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
        //    {
        //        SmoDA da = new SmoDA();
        //        user = da.DeleteUser(user);

        //        ts.Complete();
        //        return user;
        //    }
        //}
        //public User SelectUserByPK(string customerNo, string username)
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectUserByPK(customerNo, username);
        //}
        public User UserAuthentication(string username, string password)
        {
            SmoDA da = new SmoDA();

            var adminUser = da.SelectConstantByPK("ADMIN_USER");
            var adminPass = da.SelectConstantByPK("ADMIN_PASS");

            var hash = PasswordHelper.EncodePasswordToBase64(adminPass.Value);

            bool isAdmin = adminUser.Value.ToUpper() == username.ToUpper();

            User user = null;
            if (isAdmin)
            {
                if (password == hash)
                {
                    user = GetAdminUser(adminUser);
                }
            }
            else
            {
                user = da.UserAuthentication(username, password);
            }

            return user;
        }
        public User SelectUserByPK(string customerNo, string username)
        {
            SmoDA da = new SmoDA();

            var adminUser = da.SelectConstantByPK("ADMIN_USER");
            var adminPass = da.SelectConstantByPK("ADMIN_PASS");

            bool isAdmin = adminUser.Value.ToUpper() == username.ToUpper();

            User user;
            if (isAdmin)
            {
                user = GetAdminUser(adminUser);
            }
            else
            {
                user = da.SelectUserByPK(username);
            }

            return user;
        }

        private User GetAdminUser(Constant adminUser)
        {
            return new User()
            {
                Username = adminUser.Value,
                FullName = "Administrator",
                CustomerNo = "902017",
                IsDeleted = false,
                ValidFrom = DateTime.MinValue,
                ValidTo = DateTime.MaxValue,
                IsAdmin = true
            };
        }
        public User SelectUserCreateByOrderNo(string orderNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectUserCreateByOrderNo(orderNo);
        }
        #endregion

        #region Customers

        public Customer CreateCustomer(Customer customer)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customer = da.CreateCustomer(customer);

                ts.Complete();
                return customer;
            }
        }

        public Customer UpdateCustomer(Customer customer)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customer = da.UpdateCustomer(customer);

                ts.Complete();
                return customer;
            }
        }
        public Customer DeleteCustomer(Customer customer)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customer = da.DeleteCustomer(customer);

                ts.Complete();
                return customer;
            }
        }
        public Customer SelectCustomerByPK(string customerNo, string saleOrg, string distCh, string division)
        {
            SmoDA da = new SmoDA();
            return da.SelectCustomerByPK(customerNo, saleOrg, distCh, division);
        }
        public List<Customer> SelectCustomersForList(string customerNo)
        {
            SmoDA da = new SmoDA();
            var temp = da.SelectCustomersForList(customerNo);
            var rs = temp.Where(o => !o.IsDeleted).ToList();
            return rs;

        }

        #endregion

        #region Materials
        public Material CreateMaterial(Material material)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string materialNo = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                material.MaterialNo = materialNo;
                material = da.CreateMaterial(material);

                ts.Complete();
                return material;
            }
        }
        public Material UpdateMaterial(Material material)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                material = da.UpdateMaterial(material);

                ts.Complete();
                return material;
            }
        }
        public Material DeleteMaterial(Material material)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                material = da.DeleteMaterial(material);

                ts.Complete();
                return material;
            }
        }
        public Material SelectMaterialByPK(string materialNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectMaterialByPK(materialNo);
        }
        public List<Material> SelectMaterialsProductNo(string productNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectMaterialsProductNo(productNo);
        }
        public Material SelectMaterialByByProductNoAndPackageSizeCode(string productNo, string packageSizeCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectMaterialByByProductNoAndPackageSizeCode(productNo, packageSizeCode);
        }

        #endregion

        #region ProductFavs
        public ProductFav CreateProductFav(ProductFav productFav)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                productFav = da.CreateProductFav(productFav);

                ts.Complete();
                return productFav;
            }
        }
        public ProductFav UpdateProductFav(ProductFav productFav)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                productFav = da.UpdateProductFav(productFav);

                ts.Complete();
                return productFav;
            }
        }
        public ProductFav DeleteProductFav(ProductFav productFav)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                productFav = da.DeleteProductFav(productFav);

                ts.Complete();
                return productFav;
            }
        }
        //public ProductFav SelectProductFavByPK(string customerNo, string productNo, string packageSizeCode, string materialNo)
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectProductFavByPK(customerNo, productNo, packageSizeCode, materialNo);
        //}

        #endregion

        #region ProductMaterialMaps

        public ProductMaterialMap CreateProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                productMaterialMap = da.CreateProductMaterialMap(productMaterialMap);

                ts.Complete();
                return productMaterialMap;
            }
        }

        public ProductMaterialMap UpdateProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                productMaterialMap = da.UpdateProductMaterialMap(productMaterialMap);

                ts.Complete();
                return productMaterialMap;
            }
        }
        public ProductMaterialMap DeleteProductMaterialMap(ProductMaterialMap productMaterialMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                productMaterialMap = da.DeleteProductMaterialMap(productMaterialMap);

                ts.Complete();
                return productMaterialMap;
            }
        }
        //public ProductMaterialMap SelectProductMaterialMapByPK(string productNo, string packageSizeCode)
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectProductMaterialMapByPK(productNo, packageSizeCode);
        //}

        #endregion

        #region Products
        public Product CreateProduct(Product product)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                product = da.CreateProduct(product);

                ts.Complete();
                return product;
            }
        }
        public Product UpdateProduct(Product product)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                product = da.UpdateProduct(product);

                ts.Complete();
                return product;
            }
        }
        public Product DeleteProduct(Product product)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                product = da.DeleteProduct(product);

                ts.Complete();
                return product;
            }
        }
        public Product SelectProductByPK(string productNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectProductByPK(productNo);
        }
        public List<Product> SelectProductByCustomerNo(string customerNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectProductByCustomerNo(customerNo);
        }
        public List<Product> SelectProductExceptCustomerNo(string customerNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectProductExceptCustomerNo(customerNo);
        }
        #endregion

        #region PackageSizes
        public PackageSize CreatePackageSize(PackageSize packageSize)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string packageSizeCode = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                packageSize.PackageSizeCode = packageSizeCode;
                packageSize = da.CreatePackageSize(packageSize);

                ts.Complete();
                return packageSize;
            }
        }
        public PackageSize UpdatePackageSize(PackageSize packageSize)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                packageSize = da.UpdatePackageSize(packageSize);

                ts.Complete();
                return packageSize;
            }
        }
        public PackageSize DeletePackageSize(PackageSize packageSize)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                packageSize = da.DeletePackageSize(packageSize);

                ts.Complete();
                return packageSize;
            }
        }
        public PackageSize SelectPackageSizeByPK(string packageSizeCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectPackageSizeByPK(packageSizeCode);
        }
        //public List<PackageSize> SelectPackageSizesForList()
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectPackageSizesForList();
        //}
        public List<PackageSize> SelectPackageSizesByProductNo(string productNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectPackageSizesByProductNo(productNo);
        }
        #endregion

        #region PaymentTerms
        public PaymentTerm CreatePaymentTerm(PaymentTerm paymentTerm)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string paymentTermCode = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                paymentTerm.PaymentTermCode = paymentTermCode;
                paymentTerm = da.CreatePaymentTerm(paymentTerm);

                ts.Complete();
                return paymentTerm;
            }
        }
        public PaymentTerm UpdatePaymentTerm(PaymentTerm paymentTerm)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                paymentTerm = da.UpdatePaymentTerm(paymentTerm);

                ts.Complete();
                return paymentTerm;
            }
        }
        public PaymentTerm DeletePaymentTerm(PaymentTerm paymentTerm)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                paymentTerm = da.DeletePaymentTerm(paymentTerm);

                ts.Complete();
                return paymentTerm;
            }
        }
        public PaymentTerm SelectPaymentTermByPK(string paymentTermCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectPaymentTermByPK(paymentTermCode);
        }
        public List<PaymentTerm> SelectPaymentTermByCustomerNo(string customerNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectPaymentTermByCustomerNo(customerNo);
        }
        #endregion

        #region CustomerShipToMaps

        public CustomerShipToMap CreateCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerShipToMap = da.CreateCustomerShipToMap(customerShipToMap);

                ts.Complete();
                return customerShipToMap;
            }
        }

        public CustomerShipToMap UpdateCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerShipToMap = da.UpdateCustomerShipToMap(customerShipToMap);

                ts.Complete();
                return customerShipToMap;
            }
        }
        public CustomerShipToMap DeleteCustomerShipToMap(CustomerShipToMap customerShipToMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerShipToMap = da.DeleteCustomerShipToMap(customerShipToMap);

                ts.Complete();
                return customerShipToMap;
            }
        }
        public CustomerShipToMap SelectCustomerShipToMapByPK(string customerNo, string saleOrg, string distCh, string division, string shipToCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectCustomerShipToMapByPK(customerNo, saleOrg, distCh, division, shipToCode);
        }
        public List<CustomerShipToMap> SelectCustomerShipToMapsByCustomerNo(string customerNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectCustomerShipToMapsByCustomerNo(customerNo);
        }

        #endregion

        #region CustomerGroups
        public CustomerGroup CreateCustomerGroup(CustomerGroup customerGroup)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerGroup = da.CreateCustomerGroup(customerGroup);

                ts.Complete();
                return customerGroup;
            }
        }
        public CustomerGroup UpdateCustomerGroup(CustomerGroup customerGroup)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerGroup = da.UpdateCustomerGroup(customerGroup);

                ts.Complete();
                return customerGroup;
            }
        }
        public CustomerGroup DeleteCustomerGroup(CustomerGroup customerGroup)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerGroup = da.DeleteCustomerGroup(customerGroup);

                ts.Complete();
                return customerGroup;
            }
        }
        public CustomerGroup SelectCustomerGroupByPK(string customerGroupCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectCustomerGroupByPK(customerGroupCode);
        }
        public List<CustomerGroup> SelectCustomerGroupsForList()
        {
            SmoDA da = new SmoDA();
            return da.SelectCustomerGroupsForList();
        }

        #endregion

        #region CustomerPaymentTermMaps
        public CustomerPaymentTermMap CreateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerPaymentTermMap = da.CreateCustomerPaymentTermMap(customerPaymentTermMap);

                ts.Complete();
                return customerPaymentTermMap;
            }
        }
        public CustomerPaymentTermMap UpdateCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerPaymentTermMap = da.UpdateCustomerPaymentTermMap(customerPaymentTermMap);

                ts.Complete();
                return customerPaymentTermMap;
            }
        }
        public CustomerPaymentTermMap DeleteCustomerPaymentTermMap(CustomerPaymentTermMap customerPaymentTermMap)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                customerPaymentTermMap = da.DeleteCustomerPaymentTermMap(customerPaymentTermMap);

                ts.Complete();
                return customerPaymentTermMap;
            }
        }
        //public CustomerPaymentTermMap SelectCustomerPaymentTermMapByPK(string customerNo, string paymentTermCode)
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectCustomerPaymentTermMapByPK(customerNo, paymentTermCode);
        //}
        public List<CustomerPaymentTermMap> SelectCustomerPaymentTermMapsForList(string customerNo, string paymentTermCode)
        {
            SmoDA da = new SmoDA();
            return da.SelectCustomerPaymentTermMapsForList(customerNo, paymentTermCode);
        }

        #endregion

        #region SegmentManagers
        public SegmentManager CreateSegmentManager(SegmentManager segmentManager)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                segmentManager = da.CreateSegmentManager(segmentManager);

                ts.Complete();
                return segmentManager;
            }
        }
        public SegmentManager UpdateSegmentManager(SegmentManager segmentManager)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                segmentManager = da.UpdateSegmentManager(segmentManager);

                ts.Complete();
                return segmentManager;
            }
        }
        public SegmentManager DeleteSegmentManager(SegmentManager segmentManager)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                segmentManager = da.DeleteSegmentManager(segmentManager);

                ts.Complete();
                return segmentManager;
            }
        }
        //public SegmentManager SelectSegmentManagerByPK(string segmentManagerCode)
        //{
        //    SmoDA da = new SmoDA();
        //    return da.SelectSegmentManagerByPK(segmentManagerCode);
        //}
        public List<SegmentManager> SelectSegmentManagersForList()
        {
            SmoDA da = new SmoDA();
            return da.SelectSegmentManagersForList();
        }

        #endregion


        #region SendEmail

        string formatTable = @"<table style=""border: 1px solid black;border-collapse: collapse;font-family:Tahoma;font-size: 10pt;"">{0}</table>";

        public void SendEmailForInvoice(Customer c, List<Invoice> items, string fileName)
        {
            var formats = new List<HtmlTableRowFormat>();
            formats.Add(new HtmlTableRowFormat(0, Align.Center, "Delivery order no."));
            formats.Add(new HtmlTableRowFormat(1, Align.Left, "Product/Grade"));
            formats.Add(new HtmlTableRowFormat(2, Align.Right, "Quantity/kgs"));
            formats.Add(new HtmlTableRowFormat(3, Align.Center, "Invoice No"));

            string dataTable = GetFormatRowHd(formats);
            string formatRowIt = GetFormatRowIt(formats);

            if (items != null)
            {
                foreach (var item in items)
                {
                    string dataRow = string.Format(formatRowIt,
                        item.DeliveryOrder,
                        string.Format("{0} {1}", item.ProductName, item.PackSizeName),
                        item.Quantity.ToString(DataFormat.Integer),
                        item.InvoiceNo);
                    dataTable += dataRow;
                }
            }

            string lineItems = string.Format(formatTable, dataTable);
            var it = items.FirstOrDefault();
            bool isCancel = it.IsCancel == "X";

            var caseNo = isCancel ? 191 : 19;

            var fs = fileName.Split('-');

            var order = new SmoDA().SelectOrdersForList(null, null, null, null, null, items.First().QuotationNo, null, null, null, null, OrderStatusCode.All).FirstOrDefault();
            var orderNo = order == null ? string.Empty : order.OrderNo;

            if (isCancel)
            {
                //delete by invoice
                var da = new SmoDA();
                var exist = da.SelectMailPDFByPK(fileName);
                da.DeleteMailPDF(exist);
            }

            SendEmailForV2(caseNo, c.MailInvAddress, c.MailInvCc, c.MailInvBcc, "", null, it.PoNo, it.ShipToName, it.Address, it.DeliveryDate, it.ShipmentNo, it.SoldToName, it.SaleOrder, it.QuotationNo, it.InvoiceNo, lineItems, fileName, orderNo);
        }

        public void SendEmailForReportShipmentDetail(Customer c, List<ReportShipmentDetail> items, GroupByShipmentSoldTo group)
        {
            if (items == null | items.Count == 0)
                throw new Exception("SendEmailForReportShipmentDetail -> items == null | items.Count == 0");

            var formats = new List<HtmlTableRowFormat>();
            formats.Add(new HtmlTableRowFormat(0, Align.Left, "Delivery Order No."));
            formats.Add(new HtmlTableRowFormat(1, Align.Left, "Product / Grade"));
            formats.Add(new HtmlTableRowFormat(2, Align.Left, "Package Size"));
            formats.Add(new HtmlTableRowFormat(3, Align.Right, "Quantity/KG"));

            string dataTable = GetFormatRowHd(formats);
            string formatRowIt = GetFormatRowIt(formats);

            foreach (var item in items)
            {
                string dataRow = string.Format(formatRowIt,
                    item.DeliveryOrder,
                    item.ProductName,
                    item.PackageSizeName,
                   (item.Quantity.HasValue ? item.Quantity.Value.ToString(DataFormat.Integer) : ""));
                dataTable += dataRow;
            }

            string lineItems = string.Format(formatTable, dataTable);
            var it = items.FirstOrDefault();
            if (it == null)
                throw new Exception("SendEmailForReportShipmentDetail -> it == null");

            var caseNo = group.Action == ActionEnum.NonDelete ? 16 : 161;
            SendEmailForV2(caseNo, c.MailShpAddress, c.MailShpCc, c.MailShpBcc, "", null, it.PoNo, it.ShipToName, it.Address, it.DeliveryDate, it.ShipmentNo, it.SoldToName, it.SaleOrder, it.QuotationNo, null, lineItems, "", group.OrderNo);
        }

        public void SendEmailForV2(int caseNo, string to, string cc, string bcc, string redirectUrl, byte[] pdf, string poNo, string shipToName, string address, DateTime? deliveryDate, string shipmentNo, string customerName, string saleOrder, string quotationNo, string invoiceNo, string lineItems, string pfdFileName, string docNo)
        {
            string templateHd = string.Empty;
            string templateBody = string.Empty;
            List<string> tos = new List<string>();
            List<string> ccs = new List<string>();
            List<string> bccs = new List<string>();
            string hd, body;

            templateHd = GetTemplate("EMAIL_SMO" + caseNo.ToString() + "_HD");
            templateBody = GetTemplate("EMAIL_SMO" + caseNo.ToString() + "_BODY");

            tos.Add(to);
            ccs.Add(cc);
            bccs.Add(bcc);

            string footerTh = GetTemplate("EMAIL_BODY_FOOTER_TH");
            string footerEn = GetTemplate("EMAIL_BODY_FOOTER_EN");
            string headerTh = GetTemplate("EMAIL_BODY_HEADER_TH");
            string headerEn = GetTemplate("EMAIL_BODY_HEADER_EN");

            EmailHelper.GetMailContentV2(templateHd, GetTemplate("EMAIL_BODY_TEMPLATE"), templateBody, "All",
                docNo, redirectUrl, "soldto", quotationNo, out hd, out body, "actor",
                lineItems, customerName, "reason", headerTh, headerEn, footerTh, footerEn,
                poNo, saleOrder, shipToName, address, deliveryDate, shipmentNo, invoiceNo
                );

            SendEmail(hd, body, tos.ToArray(), ccs.ToArray(), pfdFileName, bccs.ToArray());
        }


        public void SendEmail(string subject, string body, string[] sentTo, string[] ccs, string module, string[] bccs)
        {
            string to = string.Empty;
            if (sentTo != null && sentTo.Length > 0)
            {
                foreach (var item in sentTo)
                {
                    if (!string.IsNullOrEmpty(to))
                        to += "|";
                    to += item;
                }
            }

            string cc = string.Empty;
            if (ccs != null && ccs.Length > 0)
            {
                foreach (var item in ccs)
                {
                    if (!string.IsNullOrEmpty(cc))
                        cc += "|";
                    cc += item;
                }
            }

            string bcc = string.Empty;
            if (bccs != null && bccs.Length > 0)
            {
                foreach (var item in bccs)
                {
                    if (!string.IsNullOrEmpty(bcc))
                        bcc += "|";
                    bcc += item;
                }
            }

            SendMail m = new SendMail()
            {
                MailContent = body,
                MailSubject = subject,
                Module = module,
                SendTo = to,
            };

            if (!string.IsNullOrEmpty(cc)) m.SendCc = cc;
            if (!string.IsNullOrEmpty(bcc)) m.SendBcc = bcc;

            if (!string.IsNullOrEmpty(to))
                CreateSendMail(m);
        }
        public void SendEmailByCaseNo(int caseNo, User currentUser, Order order, List<OrderItem> items, string redirectUrl, byte[] pdf)
        {
            SendEmailByCaseNo(caseNo, currentUser, order, items, redirectUrl, pdf, "");
        }
        public void SendEmailByCaseNo(int caseNo, User currentUser, Order order, List<OrderItem> items, string redirectUrl, byte[] pdf, string saleOrderNo)
        {
            string templateHd = string.Empty;
            string templateBody = string.Empty;
            List<string> tos = new List<string>();
            List<string> ccs = new List<string>();
            User userRequester, userApprover;
            string hd, body;

            string lineItems = GetTableOfLineItem(items);

            //string saleConfirmNo = string.Empty;
            bool isSendPDF = false;

            string reason = string.Empty;

            var orderHistorys = new SmoDA().SelectOrderHistoryByOrderNo(order.OrderNo);

            if (orderHistorys != null && orderHistorys.Count > 0)
            {
                var orderHistory = orderHistorys.OrderByDescending(d => d.ChangeTime).FirstOrDefault();
                if (orderHistory != null && !string.IsNullOrEmpty(orderHistory.Reason))
                    reason = GetTableOfReason(orderHistory.Reason);
            }

            GetUserRequesterAndApprover(order.OrderNo, out userRequester, out userApprover);

            if (caseNo == 3)
            {
                ///3	Requestor	Submit	Req  APV	APV	Req
                templateHd = GetTemplate("EMAIL_SM_APV_HD");
                templateBody = GetTemplate("EMAIL_SM_APV_BODY");

                tos.Add(userApprover.Email);
                ccs.Add(currentUser.Email);
            }
            else if (caseNo == 4)
            {
                ///4	Approver	Submit	Req  APV  HMC	                    Sales Co.   Req, APV
                templateHd = GetTemplate("EMAIL_SM_HMC_HD");
                templateBody = GetTemplate("EMAIL_SM_HMC_BODY");

                tos.AddRange(GetHmcEmail());

                ccs.Add(userRequester.Email);
                ccs.Add(currentUser.Email);
            }
            else if (caseNo == 5 || caseNo == 18)
            {
                templateHd = GetTemplate("EMAIL_SM_SAP_HD");
                templateBody = GetTemplate("EMAIL_SM_SAP_BODY");

                tos.AddRange(GetHmcEmail());
            }
            else if (caseNo == 6 || caseNo == 19)
            {
                templateHd = GetTemplate("EMAIL_SM_SMO_HD");
                templateBody = GetTemplate("EMAIL_SM_SMO_BODY");

                //saleConfirmNo = order.SaleConfirmNo;
                isSendPDF = true;

                if (userApprover != null)
                {
                    ///6	Segment     Approve	Req  APV  HMC SAP   SMO	        Req, APV	Sales Co.
                    tos.Add(userRequester.Email);
                    tos.Add(userApprover.Email);

                    ccs.AddRange(GetHmcEmail());
                }
                else
                {
                    //19	Segment 	Approve	Req  HMC  SAP   SMO	Req	        Sales Co. 
                    tos.Add(userRequester.Email);
                    ccs.AddRange(GetHmcEmail());
                }
            }
            else if (caseNo == 7)
            {
                ///7	Approver	Reject	Req  APV  Req	                    Req	        APV
                templateHd = GetTemplate("EMAIL_RJ_APV_REQ_HD");
                templateBody = GetTemplate("EMAIL_RJ_APV_REQ_BODY");

                tos.Add(userRequester.Email);
                ccs.Add(userApprover.Email);
            }
            else if (caseNo == 8)
            {
                ///8	Requestor	Cancel	Req  APV  Req  End	            APV	        Req
                templateHd = GetTemplate("EMAIL_CC_REQ_HD");
                templateBody = GetTemplate("EMAIL_CC_REQ_BODY");

                tos.Add(userApprover.Email);
                ccs.Add(userRequester.Email);
            }
            else if (caseNo == 9)
            {
                ///9	Approver	Cancel	Req  APV  End	                    Req	        APV
                templateHd = GetTemplate("EMAIL_CC_APV_HD");
                templateBody = GetTemplate("EMAIL_CC_APV_BODY");

                tos.Add(userRequester.Email);
                ccs.Add(userApprover.Email);
            }
            else if (caseNo == 10 || caseNo == 15)
            {
                ///10	Sales Co.	Reject	Req  APV  HMC  Req	            Req	        APV, Sales Co.
                templateHd = GetTemplate("EMAIL_RJ_HMC_REQ_HD");
                templateBody = GetTemplate("EMAIL_RJ_HMC_REQ_BODY");

                tos.Add(userRequester.Email);

                if (userApprover != null)
                    ccs.Add(userApprover.Email);

                ccs.AddRange(GetHmcEmail());
            }
            else if (caseNo == 11 || caseNo == 16)
            {
                templateHd = GetTemplate("EMAIL_CC_REQ_HD");
                templateBody = GetTemplate("EMAIL_CC_REQ_BODY");

                if (userApprover != null)
                {
                    ///11	Requestor	Cancel	Req  APV  HMC  Req  End	        APV	        Req, Sales Co.
                    tos.Add(userApprover.Email);

                    ccs.Add(userRequester.Email);
                    ccs.AddRange(GetHmcEmail());
                }
                else
                {
                    //16	Requestor	Cancel	Req  HMC  Req  End	Sales Co. 	Req
                    tos.AddRange(GetHmcEmail());
                    ccs.Add(userRequester.Email);
                }
            }
            else if (caseNo == 12)
            {
                ///12	Approver	Cancel	Req  APV  HMC  Req  APV  End	Req	        APV, Sales Co.
                templateHd = GetTemplate("EMAIL_CC_APV_HD");
                templateBody = GetTemplate("EMAIL_CC_APV_BODY");

                tos.Add(userRequester.Email);
                ccs.Add(userApprover.Email);
                ccs.AddRange(GetHmcEmail());
            }
            else if (caseNo == 13 || caseNo == 17)
            {
                ///13	Sales Co.	Cancel	Req  APV  HMC  End	            Req,APV	        Sales Co.
                templateHd = GetTemplate("EMAIL_CC_HMC_HD");
                templateBody = GetTemplate("EMAIL_CC_HMC_BODY");

                tos.Add(userRequester.Email);
                if (userApprover != null)
                    tos.Add(userApprover.Email);

                ccs.AddRange(GetHmcEmail());
            }
            else if (caseNo == 14)
            {
                //14	Requestor	Submit	Req  HMC	            Sales Co. 	Req
                templateHd = GetTemplate("EMAIL_SM_HMC_HD");
                templateBody = GetTemplate("EMAIL_SM_HMC_BODY");

                tos.AddRange(GetHmcEmail());
                ccs.Add(userRequester.Email);
            }
            //else if (caseNo == 18)
            //{
            //    //18	HMC	        Submit	Req  HMC  SAP	        Sales Co. 	-
            //    templateHd = GetTemplate("EMAIL_SM_SAP_HD");
            //    templateBody = GetTemplate("EMAIL_SM_SAP_BODY");

            //    tos.AddRange(GetHmcEmail());
            //}
            else if (caseNo == 20)
            {
                templateHd = GetTemplate("EMAIL_CC_SYS_HD");
                templateBody = GetTemplate("EMAIL_CC_SYS_BODY");
                tos.Add(userRequester.Email);
                tos.Add(userApprover.Email);
                ccs.AddRange(GetHmcEmail());
            }
            else if (caseNo == 24)
            {
                templateHd = GetTemplate("EMAIL_CC_SYS_HD");
                templateBody = GetTemplate("EMAIL_CC_SYS_BODY");
                tos.Add(userRequester.Email);
                ccs.AddRange(GetHmcEmail());
            }
            string footerTh = GetTemplate("EMAIL_BODY_FOOTER_TH");
            string footerEn = GetTemplate("EMAIL_BODY_FOOTER_EN");
            string headerTh = GetTemplate("EMAIL_BODY_HEADER_TH");
            string headerEn = GetTemplate("EMAIL_BODY_HEADER_EN");

            EmailHelper.GetMailContent(templateHd, GetTemplate("EMAIL_BODY_TEMPLATE"), templateBody, "All", order.OrderNo,
                redirectUrl, order.ShipToText, order.SaleConfirmNo, out hd, out body, string.Empty,
                lineItems, order.CustomerName, reason, headerTh, headerEn, footerTh, footerEn, saleOrderNo);
            var module = isSendPDF ? order.SaleConfirmNo : string.Empty;
            SendEmail(hd, body, tos.ToArray(), ccs.ToArray(), module, null);

            if (pdf != null)
            {
                var mailPdf = new MailPDF()
                {
                    SaleConfirmNo = order.SaleConfirmNo,
                    PDFContent = pdf,
                };

                new SmoDA().CreateMailPDF(mailPdf);
            }
        }

        public void SendMail()
        {
            try
            {
                SmtpClient client = new SmtpClient();

                SmoDA da = new SmoDA();
                var EM_HOST = da.SelectConstantByPK("EM_HOST").Value;
                var EM_PORT = da.SelectConstantByPK("EM_PORT").Value;
                var EM_FR_EMAIL = da.SelectConstantByPK("EM_FR_EMAIL").Value;
                var EM_FR_NAME = da.SelectConstantByPK("EM_FR_NAME").Value;

                client.Host = EM_HOST;
                client.Port = int.Parse(EM_PORT);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;

                var mails = new SmoDA().SelectSendMailsForList(false);

                foreach (var mm in mails)
                {
                    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
                    {
                        var m = da.SelectSendMailByPK(mm.SendMailId);
                        if (!m.IsSent)
                        {
                            m.IsSent = true;
                            m.SendTime = DateTime.Now;
                            da.UpdateSendMail(m);

                            MailMessage mail = new MailMessage();
                            mail.From = new MailAddress(EM_FR_EMAIL, EM_FR_NAME);
                            mail.Subject = m.MailSubject;
                            mail.SubjectEncoding = System.Text.Encoding.UTF8;
                            mail.IsBodyHtml = true;
                            mail.Body = m.MailContent;
                            mail.BodyEncoding = System.Text.Encoding.UTF8;

                            // new
                            //string apPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                            //var filePath = Path.Combine(apPath, "hmc_logo.png");
                            //LinkedResource res = new LinkedResource(filePath);
                            //res.ContentId = Guid.NewGuid().ToString();
                            //AlternateView alternateView = AlternateView.CreateAlternateViewFromString(m.MailContent, null, MediaTypeNames.Text.Html);
                            //alternateView.LinkedResources.Add(res);
                            //mail.AlternateViews.Add(alternateView);

                            var sentTo = m.SendTo.Split('|');
                            if (sentTo != null && sentTo.Length > 0)
                            {
                                foreach (var to in sentTo)
                                {
                                    if (!string.IsNullOrEmpty(to))
                                    {
                                        if (CheckEmail(to))
                                        {
                                            mail.To.Add(new MailAddress(to));
                                        }
                                    }
                                }
                            }
                            if (!string.IsNullOrEmpty(m.SendCc))
                            {
                                foreach (var address in m.SendCc.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    mail.CC.Add(address);
                                }
                            }
                            if (!string.IsNullOrEmpty(m.SendBcc))
                            {
                                foreach (var address in m.SendBcc.Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    mail.Bcc.Add(address);
                                }
                            }

                            var mailPdf = da.SelectMailPDFByPK(m.Module);

                            if (mailPdf != null)
                            {
                                string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                                string tempPath = Path.Combine(path, "Temp");
                                string fileName = mailPdf.SaleConfirmNo + ".pdf";
                                string fullPath = Path.Combine(tempPath, fileName);

                                System.IO.File.WriteAllBytes(fullPath, mailPdf.PDFContent);
                                mail.Attachments.Add(new System.Net.Mail.Attachment(fullPath));
                            }

                            client.Send(mail);
                        }

                        ts.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private AlternateView GetEmbeddedImage(String filePath)
        {
            LinkedResource res = new LinkedResource(filePath);
            res.ContentId = Guid.NewGuid().ToString();
            string htmlBody = @"<img src='cid:logo'/>";
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(res);
            return alternateView;
        }

        private bool CheckEmail(string email)
        {
            return Regex.IsMatch(email,
              @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$",
              RegexOptions.IgnoreCase);
        }

        private List<string> GetHmcEmail()
        {
            var c = new SmoDA().SelectConstantByPK("HMC_EMAIL_CENTER");
            return c.Value.Split('|').ToList();
        }

        public void _Test()
        {
            //var da = new SmoDA();
            //var user = da.SelectUserByPK("ttt");

            //var its = da.SelectReportShipmentDetailsForList(new SearchShipmentCriteria());
            //SendEmailForReportShipmentDetail(user, its);

            //SendEmailForInvoice(user, items);
        }
        private string GetTemplate(string name)
        {
            string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            string fullPath = System.IO.Path.Combine(path, "Template", name + ".txt");
            string text = System.IO.File.ReadAllText(fullPath);
            return text;
        }
        private void GetUserRequesterAndApprover(string orderNo, out User userRequester, out User userApprover)
        {
            var da = new SmoDA();
            userRequester = null;
            userApprover = null;

            //var Order = da.SelectOrderByPK(orderNo);

            if (orderNo != null)
            {
                userRequester = da.SelectUserCreateByOrderNo(orderNo);

                if (!string.IsNullOrEmpty(userRequester.ApproverBy))
                    userApprover = da.SelectUserByPK(userRequester.ApproverBy);
            }
        }

        private static string GetTableOfReason(string his)
        {
            try
            {

                var xx = @"<table style=""font-family:Tahoma;font-size: 10pt;border-style:solid; border-width:1px; border-color:#000000; padding: 5px;""><tr><td>{0}</td></tr></table>";

                int i = his.IndexOf(',');
                int ii = his.Length - i - 1;

                var text1 = his.Substring(0, i);
                var text2 = his.Substring(i + 1, ii);

                string rs = "<b>Reason :</b> " + text1;

                if (!string.IsNullOrEmpty(text2))
                    rs += "<br><b>Text :</b> " + text2;
                return string.Format(xx, rs);

            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        private static string GetTableOfLineItem(List<OrderItem> items)
        {
            string formatTable = @"<table style=""border: 1px solid black;border-collapse: collapse;font-family:Tahoma;font-size: 10pt;"">{0}</table>";
            string formatCellHd = @"<th style=""border: 1px solid black; border-collapse: collapse; padding: 5px 10px;"">";
            string formatCellIt = @"<td style=""border: 1px solid black; border-collapse: collapse; padding: 5px 10px;"">";
            string formatCellHdMinWidth = @"<th style=""border: 1px solid black; border-collapse: collapse; padding: 5px 40px 5px 5px;"">";
            string formatCellItMinWidth = @"<td style=""border: 1px solid black; border-collapse: collapse; padding: 5px 40px 5px 5px;"">";
            string formatCellItRight = @"<td style=""border: 1px solid black; border-collapse: collapse; padding: 5px 10px; text-align: right;"">";
            string formatCellItCenter = @"<td style=""border: 1px solid black; border-collapse: collapse; padding: 5px 10px; text-align: center;"">";
            string formatRowHd = @"<tr>" + formatCellHdMinWidth + "{0}{1}</th>" + formatCellHd + "{2}</th>" + formatCellHd + "{3}</th>" + formatCellHd + "{4}</th></tr>";
            string formatRowIt = @"<tr>" + formatCellItMinWidth + "{0}) {1}</td>" + formatCellItCenter + "{2}</td>" + formatCellItRight + "{3}</td>" + formatCellItRight + "{4}</td></th>";

            string dataTable = string.Empty;
            string dataHd = string.Format(formatRowHd, string.Empty, "Product/Grade", "Package Size", "Quantity/KG", "Unit Price");
            dataTable += dataHd;

            if (items != null)
            {
                foreach (var item in items)
                {
                    string dataRow = string.Format(formatRowIt,
                        item.ItemNo,
                        item.ProductText,
                        item.PackageSizeText,
                        item.Quantity.ToString(DataFormat.Integer),
                        item.UnitPrice.ToString(DataFormat.Decimal));
                    dataTable += dataRow;
                }
            }

            string fullData = string.Format(formatTable, dataTable);

            return fullData;
        }

        //private static string GetTableOfLineItemForSMO16(List<ReportShipmentDetail> items, List<HtmlTableRowFormat> formats)
        //{

        //}

        private static string GetFormatRowHd(List<HtmlTableRowFormat> formats)
        {
            string formatCellHdFormat = @"<th style=""border: 1px solid black; border-collapse: collapse; padding: 5px;"">";
            //string formatRowHd = @"<tr>" + formatCellHd + "{0}{1}</th>" + formatCellHd + "{2}</th>" + formatCellHd + "{3}</th>" + formatCellHd + "{4}</th></tr>";
            //string dataHd = string.Format(formatRowHd, string.Empty, "Product/Grade", "Package Size", "Quantity/KG", "Unit Price");


            string formatRowHdNew = "<tr>";

            foreach (var item in formats)
            {
                var formatCellHd = formatCellHdFormat + item.HeaderText + "</th>";
                formatRowHdNew += formatCellHd;
            }

            formatRowHdNew += "</tr>";

            return formatRowHdNew;
        }

        private static string GetFormatRowIt(List<HtmlTableRowFormat> formats)
        {
            string formatCellItFmt = @"<td style=""border: 1px solid black; border-collapse: collapse; padding: 5px 10px; text-align: {0};"">{1}</td>";
            string formatRowItNew = "<tr>";

            foreach (var item in formats)
            {
                var formatCellIt = string.Format(formatCellItFmt, item.AlignText, item.ColText);
                formatRowItNew += formatCellIt;
            }

            formatRowItNew += "</tr>";

            return formatRowItNew;
        }


        //public void SendEmailOneTo(string subject, string body, string sentTo)
        //{
        //    var to = new string[] { sentTo };
        //    SendEmail(subject, body, to, null);
        //}
        //public void SendEmailOneToOneCc(string subject, string body, string sentTo, string cc)
        //{
        //    var to = new string[] { sentTo };
        //    var ccs = new string[] { cc };
        //    SendEmail(subject, body, to, ccs);
        //}
        //public void SendEmailOneToMultiCc(string subject, string body, string sentTo, string[] ccs)
        //{
        //    var to = new string[] { sentTo };
        //    SendEmail(subject, body, to, ccs);
        //}

        #endregion


        #region CreateOrModify
        public void CreateOrModifyPaymentTerms(List<PaymentTerm> paymentTerms)
        {
            
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in paymentTerms)
                {
                    
                    var obj = da.SelectPaymentTermByPK(c.PaymentTermCode);

                    if (obj == null)
                        da.CreatePaymentTerm(c);
                    else
                        da.UpdatePaymentTerm(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyPackageSizes(List<PackageSize> packageSizes)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in packageSizes)
                {
                    var obj = da.SelectPackageSizeByPK(c.PackageSizeCode);

                    if (obj == null)
                        da.CreatePackageSize(c);
                    else
                        da.UpdatePackageSize(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyCustomerGroups(List<CustomerGroup> customerGroups)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in customerGroups)
                {
                    var obj = da.SelectCustomerGroupByPK(c.CustomerGroupCode);

                    if (obj == null)
                        da.CreateCustomerGroup(c);
                    else
                        da.UpdateCustomerGroup(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifySegmentManagers(List<SegmentManager> segmentManagers)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in segmentManagers)
                {
                    var obj = da.SelectSegmentManagerByPK(c.SegmentManagerCode);

                    if (obj == null)
                        da.CreateSegmentManager(c);
                    else
                        da.UpdateSegmentManager(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyCustomers(List<Customer> customers)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in customers)
                {
                    c.MailInvAddress = c.MailInvAddress.Replace(';', '|');
                    c.MailInvBcc = c.MailInvBcc.Replace(';', '|');
                    c.MailInvCc = c.MailInvCc.Replace(';', '|');

                    c.MailShpAddress = c.MailShpAddress.Replace(';', '|');
                    c.MailShpBcc = c.MailShpBcc.Replace(';', '|');
                    c.MailShpCc = c.MailShpCc.Replace(';', '|');

                    var obj = da.SelectCustomerByPK(c.CustomerNo, c.SaleOrg, c.DistCh, c.Division);

                    if (obj == null)
                        da.CreateCustomer(c);
                    else
                        da.UpdateCustomer(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyCustomerShipToMaps(List<CustomerShipToMap> customerShipToMaps)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in customerShipToMaps)
                {
                    var obj = da.SelectCustomerShipToMapByPK(c.CustomerNo, c.SaleOrg, c.DistCh, c.Division, c.ShipToCode);

                    if (obj == null)
                        da.CreateCustomerShipToMap(c);
                    else
                        da.UpdateCustomerShipToMap(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyCustomerPaymentTermMaps(List<CustomerPaymentTermMap> customerPaymentTermMaps)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in customerPaymentTermMaps)
                {
                    var obj = da.SelectCustomerPaymentTermMapByPK(c.CustomerNo, c.PaymentTermCode);

                    if (obj == null)
                        da.CreateCustomerPaymentTermMap(c);
                    else
                        da.UpdateCustomerPaymentTermMap(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyMaterials(List<Material> materials)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in materials)
                {
                    var obj = da.SelectMaterialByPK(c.MaterialNo);

                    if (obj == null)
                    {
                        da.CreateMaterial(c);
                    }
                    else
                    {
                        da.UpdateMaterial(c);
                    }
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyProducts(List<Product> products)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in products)
                {
                    var obj = da.SelectProductByPK(c.ProductNo);
                    if (obj == null)
                        da.CreateProduct(c);
                    else
                        da.UpdateProduct(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyProductMaterialMaps(List<ProductMaterialMap> productMaterialMaps)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in productMaterialMaps)
                {
                    var obj = da.SelectProductMaterialMapByPK(c.ProductNo, c.PackageSizeCode);
                    if (obj == null)
                        da.CreateProductMaterialMap(c);
                    else
                        da.UpdateProductMaterialMap(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyProductFavs(List<ProductFav> productFavs)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in productFavs)
                {
                    var obj = da.SelectProductFavByPK(c.CustomerNo, c.ProductNo);
                    if (obj == null)
                        da.CreateProductFav(c);
                    else
                        da.UpdateProductFav(c);
                }

                ts.Complete();
            }
        }
        public void CreateOrModifyUsers(List<User> users)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                foreach (var c in users)
                {
                    var obj = da.SelectUserByPK(c.Username);
                    if (obj == null)
                        da.CreateUser(c);
                    else
                        da.UpdateUser(c);
                }

                ts.Complete();
            }
        }
        public enum ActionEnum { NonDelete, Delete }
        public class GroupByShipmentSoldTo
        {
            public string ShipmentNo { get; set; }
            public string SoldTo { get; set; }
            public string OrderNo { get; set; }
            public ActionEnum Action { get; set; }
        }
        public string CreateOrModifyReportShipmentDetails(List<ReportShipmentDetail> reportShipmentDetails, bool? isSendMail)
        {
            if (reportShipmentDetails.Count <= 0)
                return "reportShipmentDetails == 0";

            var groupByShipments = reportShipmentDetails.GroupBy(x => new { x.ShipmentNo, x.SoldTo, x.SMONo }
            , (key, group) => new GroupByShipmentSoldTo
            {
                ShipmentNo = key.ShipmentNo,
                SoldTo = key.SoldTo,
                OrderNo = key.SMONo
            }).ToList();
            SmoDA da = new SmoDA();

            var itemsForPrintInEmail = new List<ReportShipmentDetail>();

            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                foreach (var it in groupByShipments)
                {
                    var s = new SearchShipmentCriteria() { ShipmentNo = it.ShipmentNo, SoldTo = it.SoldTo, SMONo = it.OrderNo };
                    var itemsFromDB = da.SelectReportShipmentDetailsForList(s);

                    if (string.IsNullOrEmpty(it.SoldTo) || string.IsNullOrEmpty(it.OrderNo))
                    {
                        //if no sold to in textfile, means delete shipment
                        if (string.IsNullOrEmpty(it.SoldTo))
                        {
                            it.Action = ActionEnum.Delete;
                        }

                        //get SmoNo & soldto
                        if (itemsFromDB != null && itemsFromDB.Count > 0)
                        {
                            var item1 = itemsFromDB.First();

                            it.SoldTo = item1.SoldTo;

                            if (string.IsNullOrEmpty(item1.QuotationNo))
                                continue;

                            var qNo = int.Parse(item1.QuotationNo).ToString();
                            var o = da.SelectOrdersForList(null, null, null, null, null, qNo, null, null, null, null, OrderStatusCode.All).FirstOrDefault();
                            if (o != null)
                                it.OrderNo = o.OrderNo;
                        }
                    }

                    // always delete shipment in all cases
                    if (itemsFromDB != null)
                    {
                        if (it.Action == ActionEnum.Delete)
                        {
                            //keep delete items for show in email before delete
                            //itemsForPrintInEmail.AddRange(itemsFromDB);

                            foreach (var item in itemsFromDB)
                            {
                                var oo = da.SelectReportShipmentDetailByPK(item.ReportShipmentDetailID);
                                itemsForPrintInEmail.Add(oo);
                            }
                        }

                        foreach (var itDel in itemsFromDB)
                        {
                            var oo = da.SelectReportShipmentDetailByPK(itDel.ReportShipmentDetailID);
                            da.DeleteReportShipmentDetail(oo);
                        }
                    }

                    var its = reportShipmentDetails.Where(o => o.ShipmentNo == it.ShipmentNo && o.SoldTo == it.SoldTo && o.SMONo == it.OrderNo).ToList();
                    foreach (var it2 in its)
                    {
                        if (!string.IsNullOrEmpty(it2.DeliveryOrder))
                        {
                            it2.ReportShipmentDetailID = Guid.NewGuid().ToString();
                            da.CreateReportShipmentDetail(it2);

                            itemsForPrintInEmail.Add(it2);
                        }
                    }
                }
                ts.Complete();
            }

            //send mail
            foreach (var it in groupByShipments)
            {
                if (it.Action == ActionEnum.Delete && string.IsNullOrEmpty(it.OrderNo))
                    continue;

                string cCode = it.SoldTo;
                Customer c = null;

                c = da.SelectCustomerByPK(cCode, null, null, null);

                if (c == null)
                    return string.Format("Customer {0} not found", cCode);

                if (isSendMail.Value && c.MailShpSend)
                {
                    var reportShipmentDetailsByShipmentNo = itemsForPrintInEmail.Where(o => o.ShipmentNo == it.ShipmentNo && o.SoldTo == it.SoldTo && o.SMONo == it.OrderNo).ToList();
                    SendEmailForReportShipmentDetail(c, reportShipmentDetailsByShipmentNo, it);
                }
            }

            return null;
        }

        public void CreateOrModifyReportShipmentBalances(List<ReportShipmentBalance> reportShipmentBalances)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                var q = reportShipmentBalances.First().QuotationNo;
                var s = new SearchShipmentCriteria();
                s.QuotationNo = q;
                var dellst = da.SelectReportShipmentBalancesForList(s);
                foreach (var item in dellst)
                {
                    da.DeleteReportShipmentBalance(item);
                }
                foreach (var c in reportShipmentBalances)
                {
                    var obj = da.SelectUserByPK(c.ReportShipmentBalanceID);
                    if (obj == null)
                    {
                        c.ReportShipmentBalanceID = Guid.NewGuid().ToString();
                        da.CreateReportShipmentBalance(c);
                    }
                    else
                        da.UpdateReportShipmentBalance(c);
                }

                ts.Complete();
            }
        }

        public void CreateOrModifyMailPDF(MailPDF mailPDF)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                var exist = da.SelectMailPDFByPK(mailPDF.SaleConfirmNo);
                if (exist != null)
                    da.DeleteMailPDF(exist);

                da.CreateMailPDF(mailPDF);
                ts.Complete();
            }
        }

        #endregion

        #region SendMails
        public SendMail CreateSendMail(SendMail sendMail)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string sendMailId = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                sendMail.SendMailId = sendMailId;
                sendMail = da.CreateSendMail(sendMail);

                ts.Complete();
                return sendMail;
            }
        }
        public SendMail UpdateSendMail(SendMail sendMail)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                sendMail = da.UpdateSendMail(sendMail);

                ts.Complete();
                return sendMail;
            }
        }
        public SendMail DeleteSendMail(SendMail sendMail)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                sendMail = da.DeleteSendMail(sendMail);

                ts.Complete();
                return sendMail;
            }
        }
        public SendMail SelectSendMailByPK(string sendMailId)
        {
            SmoDA da = new SmoDA();
            return da.SelectSendMailByPK(sendMailId);
        }
        public List<SendMail> SelectSendMailsForList(bool isSent)
        {
            SmoDA da = new SmoDA();
            return da.SelectSendMailsForList(isSent);
        }


        #endregion

        public decimal SelectTaxPercentByTaxClass(string taxClass)
        {
            SmoDA da = new SmoDA();
            return da.SelectTaxPercentByTaxClass(taxClass);
        }

        public RS_ZIDOC_INPUT_QUOTATION BAPI_ZIDOC_INPUT_QUOTATION(RS_ZIDOC_INPUT_QUOTATION val)
        {
            string hdText = string.Empty;
            string value = string.Empty;

            value = GetLeadingSpace(4, val.Customer.QuotType);
            hdText += value;
            value = GetLeadingSpace(4, val.Customer.SaleOrg);
            hdText += value;
            value = GetLeadingSpace(2, val.Customer.DistCh);
            hdText += value;
            value = GetLeadingSpace(2, val.Customer.Division);
            hdText += value;
            value = GetLeadingSpace(4, val.Customer.SaleOffice);
            hdText += value;
            value = GetLeadingSpace(16, val.Customer.CustomerNo);
            hdText += value;
            value = GetLeadingSpace(16, val.Order.ShipToCode);
            hdText += value;
            value = GetLeadingSpace(20, val.Order.PONo);
            hdText += value;
            value = GetLeadingSpace(8, val.Order.PODate.HasValue ? val.Order.PODate.Value.ToString("yyyyMMdd") : string.Empty);
            hdText += value;
            value = GetLeadingSpace(8, val.Order.ValidityDate.Value.ToString("yyyyMMdd"));
            hdText += value;
            value = GetLeadingSpace(8, val.Order.PricingDate.Value.ToString("yyyyMMdd"));
            hdText += value;
            value = GetLeadingSpace(4, val.Order.PaymentTermCode);
            hdText += value;
            value = GetLeadingSpace(5, "THB");
            hdText += value;
            value = GetLeadingSpace(12, val.Order.OrderNo);
            hdText += value;
            value = GetLeadingSpace(3, "SMO");
            hdText += value;
            value = GetLeadingSpace(3, val.Order.CustomerGroupCode);
            hdText += value;
            value = GetLeadingSpace(35, val.User.FullName);
            hdText += value;

            SapConnection bapi = new SapConnection("ZIDOC_INPUT_QUOTATION");

            var tblData = bapi.Function.GetTable("PT_IDOC_DATA_RECORDS_40");
            tblData.Append();
            tblData.SetValue("SDATA", hdText);
            val.TEXT_HD = hdText;

            List<string> text_it = new List<string>();

            foreach (var v in val.OrderItems)
            {
                string itText = string.Empty;
                value = string.Empty;

                value = GetLeadingSpace(18, v.MaterialNo);
                itText += value;
                value = GetLeadingSpace(13, v.Quantity.ToString());
                itText += value;
                value = GetLeadingSpace(13, "KG");
                itText += value;
                value = GetLeadingSpace(13, v.UnitPrice.ToString());
                itText += value;
                value = GetLeadingSpace(4, v.ItemNo.ToString());
                itText += value;

                if (v.DeliveryDate.HasValue)
                {
                    value = GetLeadingSpace(8, v.DeliveryDate.Value.ToString("yyyyMMdd"));
                    itText += value;
                }

                tblData.Append();
                tblData.SetValue("SDATA", itText);

                text_it.Add(itText);
            }
            val.TEXT_IT = text_it;

            bapi.ExcFunction();

            var xxx = bapi.Function.GetString("IDOC_NO");
            val.IDOC_QUOTATION = bapi.Function.GetString("IDOC_QUOTATION");

            if (string.IsNullOrEmpty(val.IDOC_QUOTATION))
                val.IDOC_MESSAGE = bapi.Function.GetString("IDOC_MESSAGE");

            return val;
        }

        private static string GetLeadingSpace(int lengthField, string value)
        {
            string valueWithSpace = value;
            int spaceCount = lengthField - value.Length;

            for (int i = 0; i < spaceCount; i++)
            {
                valueWithSpace += " ";
            }
            return valueWithSpace;
        }

        #region MailPDFs
        public MailPDF CreateMailPDF(MailPDF mailPDF)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {

                SmoDA da = new SmoDA();
                mailPDF = da.CreateMailPDF(mailPDF);

                ts.Complete();
                return mailPDF;
            }
        }
        public MailPDF DeleteMailPDF(MailPDF mailPDF)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                mailPDF = da.DeleteMailPDF(mailPDF);

                ts.Complete();
                return mailPDF;
            }
        }
        public MailPDF SelectMailPDFByPK(string saleConfirmNo)
        {
            SmoDA da = new SmoDA();
            return da.SelectMailPDFByPK(saleConfirmNo);
        }
        public List<MailPDFSelectForList> SelectMailPDFsSelectForList(string invoice)
        {
            return new SmoDA().SelectMailPDFsSelectForList(invoice);
        }
        #endregion

        #region Attachments
        public Attachment CreateAttachment(Attachment attachment)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string attachmentId = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                attachment.AttachmentId = attachmentId;
                attachment = da.CreateAttachment(attachment);

                ts.Complete();
                return attachment;
            }
        }
        public Attachment UpdateAttachment(Attachment attachment)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                attachment = da.UpdateAttachment(attachment);

                ts.Complete();
                return attachment;
            }
        }
        public Attachment DeleteAttachment(Attachment attachment)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                attachment = da.DeleteAttachment(attachment);

                ts.Complete();
                return attachment;
            }
        }
        public Attachment DeleteAttachmentByPK(string attachmentId)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();

                Attachment attachment = da.SelectAttachmentByPK(attachmentId);
                attachment = da.DeleteAttachment(attachment);

                ts.Complete();
                return attachment;
            }
        }
        public Attachment SelectAttachmentByPK(string attachmentId)
        {
            SmoDA da = new SmoDA();
            return da.SelectAttachmentByPK(attachmentId);
        }
        public List<AttachmentForList> SelectAttachmentsForList(string orderNo, string note)
        {
            return new SmoDA().SelectAttachmentsForList(orderNo, note);
        }

        #endregion

        #region ReportShipmentDetails
        public ReportShipmentDetail CreateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                string reportShipmentDetailID = Guid.NewGuid().ToString();

                SmoDA da = new SmoDA();
                reportShipmentDetail.ReportShipmentDetailID = reportShipmentDetailID;
                reportShipmentDetail = da.CreateReportShipmentDetail(reportShipmentDetail);

                ts.Complete();
                return reportShipmentDetail;
            }
        }
        public ReportShipmentDetail UpdateReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                reportShipmentDetail = da.UpdateReportShipmentDetail(reportShipmentDetail);

                ts.Complete();
                return reportShipmentDetail;
            }
        }
        public ReportShipmentDetail DeleteReportShipmentDetail(ReportShipmentDetail reportShipmentDetail)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                SmoDA da = new SmoDA();
                reportShipmentDetail = da.DeleteReportShipmentDetail(reportShipmentDetail);

                ts.Complete();
                return reportShipmentDetail;
            }
        }
        public ReportShipmentDetail SelectReportShipmentDetailByPK(string reportShipmentDetailID)
        {
            return new SmoDA().SelectReportShipmentDetailByPK(reportShipmentDetailID);
        }
        public List<ReportShipmentDetailForList> SelectReportShipmentDetailsForList(SearchShipmentCriteria s)
        {
            return new SmoDA().SelectReportShipmentDetailsForList(s);
        }

        internal WebConfig GetWebConfig()
        {
            var da = new SmoDA();
            var cf = new WebConfig();

            cf.WEB_QTY_MIN = int.Parse(da.SelectConstantByPK("WEB_QTY_MIN").Value);//25;
            cf.WEB_ITEM_MAX = int.Parse(da.SelectConstantByPK("WEB_ITEM_MAX").Value);//10;
            cf.WEB_QTY_MAX = int.Parse(da.SelectConstantByPK("WEB_QTY_MAX").Value);//2000000;
            cf.WEB_APP_TYPE = da.SelectConstantByPK("WEB_APP_TYPE").Value;//"PRD";
            cf.WEB_UP_EXTS = da.SelectConstantByPK("WEB_UP_EXTS").Value;//"xls, xlsx, doc, docx, txt, pdf, jpg, jpeg, png";
            cf.WEB_UP_MAX_FILE = int.Parse(da.SelectConstantByPK("WEB_UP_MAX_FILE").Value);//5;
            cf.WEB_UP_MAX_SIZE = int.Parse(da.SelectConstantByPK("WEB_UP_MAX_SIZE").Value);//1;

            return cf;
        }

        #endregion

    }
}