//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hmc.Smo.Wcf
{
    using System;
    using System.Collections.Generic;
    
    public partial class MailPDF
    {
        public string SaleConfirmNo { get; set; }
        public byte[] PDFContent { get; set; }
        public System.DateTime CreatedTime { get; set; }
        public string QuotationNo { get; set; }
        public string DeliveryOrder { get; set; }
        public string InvoiceNo { get; set; }
        public string SMONo { get; set; }
        public string InvoiceType { get; set; }
    }
}
