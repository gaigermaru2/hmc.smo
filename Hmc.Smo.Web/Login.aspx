﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Hmc.Smo.Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <title>Welcome to SmartOrder System by HMC</title>
    <script type="text/javascript" src="Scripts/JScript.js"></script>
    <link type="text/css" href="Styles/panel.css" rel="stylesheet" />
    <link type="text/css" href="Styles/w3.css" rel="stylesheet" />

    <style>
        .mySlides {
            display: none
        }

        .w3-left, .w3-right, .w3-badge {
            cursor: pointer
        }

        .w3-badge {
            height: 13px;
            width: 13px;
            padding: 0
        }
    </style>
</head>
<body style="background-color: #fff">
    <div class="w3-display-container" style="height: 100px">
        <div class="w3-display-left w3-margin-left">
            <img alt="" src="Images/hmc_logo_226x71.png" style="width: 200px;" />
        </div>
        <div class="w3-display-right w3-margin-right">
            <img alt="" src="Images/hmc_smo_logo.png" style="width: 250px;" />
        </div>
    </div>
    <div class="w3-display-container">
        <div class="mySlides w3-animate-opacity" style="height: 380px; max-width: 100%; background: url('Images/login1.jpg') center center;"></div>
        <div class="mySlides w3-animate-opacity" style="height: 380px; max-width: 100%; background: url('Images/login2.jpg') center center;"></div>
        <div class="mySlides w3-animate-opacity" style="height: 380px; max-width: 100%; background: url('Images/login3.jpg') center center;"></div>

        <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
        <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
    </div>
    <script type="text/javascript">
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) { myIndex = 1 }
            x[myIndex - 1].style.display = "block";
            setTimeout(carousel, 5000);
        }

        function plusDivs(n) {
            showDivs(myIndex += n);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            if (n > x.length) { myIndex = 1 }
            if (n < 1) { myIndex = x.length }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            x[myIndex - 1].style.display = "block";
        }

        function currentDiv(n) {
            showDivs(myIndex = n);
        }

    </script>
    <%-- <script>
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
            showDivs(slideIndex += n);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            if (n > x.length) { slideIndex = 1 }
            if (n < 1) { slideIndex = x.length }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            x[slideIndex - 1].style.display = "block";
        }
    </script>--%>
    <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="w3-content">

                    <asp:Panel ID="pnlSignIn" runat="server" DefaultButton="btnSignIn">
                        <div class="w3-row-padding w3-margin">
                            <div class="w3-half">
                                <label>USERNAME</label>
                                <asp:TextBox ID="txtUsername" runat="server" CssClass="w3-input w3-border" MaxLength="40"
                                    placeholder="enter your username/email" />
                                <asp:RequiredFieldValidator ID="vrqUsername" ControlToValidate="txtUsername" runat="server"
                                    CssClass="label-r" ValidationGroup="login" SetFocusOnError="true" Display="Dynamic" />
                            </div>
                            <div class="w3-half">
                                <label>PASSWORD</label>
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="w3-input w3-border" MaxLength="20"
                                    TextMode="Password" placeholder="enter your password" />
                                <asp:RequiredFieldValidator ID="vrqPass" ControlToValidate="txtPassword" runat="server"
                                    CssClass="label-r" ValidationGroup="login" SetFocusOnError="true" Display="Dynamic" />
                            </div>
                        </div>
                        <div class="w3-row-padding w3-margin">
                            <div class="w3-half">
                                <asp:Button ID="btnSignIn" Text="SIGN IN" runat="server" CssClass="w3-button w3-black w3-round-large"
                                    ValidationGroup="login" OnClick="btnSignIn_Click" />
                                <asp:Label ID="txtMsg" runat="server" Style="color: red" />
                            </div>
                            <div class="w3-half">
                                <asp:LinkButton ID="lnkResetPassword" Text="Reset your password?" PostBackUrl="~/ResetPassword.aspx"
                                    runat="server" CssClass="label" ForeColor="Gray" Font-Size="10pt" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="w3-center w3-padding-16 w3-margin-top w3-gray">
                    <a href="https://www.hmcpolymers.com/applications" target="_blank">Application</a> |
                    <a href="https://www.hmcpolymers.com/product-table" target="_blank">Product Table</a> |
                    <a href="https://www.hmcpolymers.com/uploads/files/resources/hmc-product-selection-guide.pdf">Product Quick Reference Guide</a> |
                    <a href="https://www.hmcpolymers.com/latest-news" target="_blank">Latest News</a>
                </div>
                <asp:Panel ID="pnlChangePass" runat="server" CssClass="panel-msg" DefaultButton="btnChangePass">
                    <div class="w3-container w3-blue">
                        <h2>Change password</h2>
                    </div>
                    <div class="w3-container">
                        <p>
                            <label>
                                New Password <span style="color: red;">*</span>
                            </label>
                            <asp:TextBox ID="txtNewPass" runat="server" CssClass="w3-input w3-border" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="rfvNewPass" ControlToValidate="txtNewPass" runat="server"
                                CssClass="label-r" ValidationGroup="changepass" SetFocusOnError="true" Display="Dynamic" />
                        </p>

                        <p>
                            <label>
                                Confirm New Password <span style="color: red;">*</span>
                            </label>
                            <asp:TextBox ID="txtNewPassConfirm" runat="server" CssClass="w3-input w3-border" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="rfvNewPassConfirm" ControlToValidate="txtNewPassConfirm"
                                runat="server" CssClass="label-r" ValidationGroup="changepass" SetFocusOnError="true"
                                Display="Dynamic" />
                        </p>
                        <p>
                            <asp:CompareValidator ID="cpvNewPass" ErrorMessage="New Password must be same" ControlToValidate="txtNewPassConfirm"
                                ControlToCompare="txtNewPass" ValidationGroup="changepass" Display="Dynamic"
                                SetFocusOnError="true" CssClass="w3-red" runat="server" />
                        </p>
                        <p>
                            <asp:Button ID="btnChangePass" Text="Change Password" runat="server" CssClass="w3-btn w3-blue-grey"
                                ValidationGroup="changepass" OnClick="btnChangePass_Click" />
                        </p>
                    </div>
                </asp:Panel>
                <asp:Button ID="btnChangePassHidden" runat="server" CssClass="hide" />
                <atk:ModalPopupExtender ID="mpeChangePass" BackgroundCssClass="panel-bg" PopupControlID="pnlChangePass"
                    TargetControlID="btnChangePassHidden" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
    <div class="w3-container">
    </div>
</body>
</html>
