﻿<%@ Page Title="Orders : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Hmc.Smo.Web.Dashboard"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageName" runat="server">
    <div class="panel-heading" style="font-size:16px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e;cursor:pointer">☰</a>  Dashboard<div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>
    </div>
      <link type="text/css" href="assets/css/argon.css?v=1.0.0" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 0px">       
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphCriteria" runat="server">
 <style>
     @media (min-width: 992px) {
    .barchartx {
       height:10vh; width:20vw
    }
}
@media (min-width: 1200px) {
  .barchartx {
    /*width: 1170px;*/
    
    height:20vh; width:40vw
  }
}

.panel-headingx {
    color: #fff;
    background-color: #FF6600;
    border-color: #337ab7;
}
.panel-primary.panel-heading {
    color: #fff;
    background-color: #FF6600;
    border-color: #337ab7;
}


 </style>
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="row" >
         <div class="col-lg-4" >        
         <div class="card card-stats mb-4 mb-xl-0" style="" >
               <div class="panel panel-primary" style="border-color: #FF6600;">
                    <div class="panel-heading" style="font-size:16px;font-weight:bold;background-color:#FF6600;border-color: #FF6600;">
                        Sales Order: <asp:Label ID="txtPreViousMonthName" runat="server" Text=""></asp:Label>'<asp:Label ID="Label1" runat="server" Text=""></asp:Label> Qty (TONS)  
                       <%-- <div class="icon icon-shape bg-info text-white rounded-circle shadow" style="font-size:12px;font-weight:bold;float:right;vertical-align:top">
                        TON
                      </div>--%>
                    </div>
                   
                      
                    
                <div class="card-body" style="height:145px;">
                  <div class="row" >
                    <div class="col"  style="height:120px;">
                      <%--<h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>--%>
                      <span class="h1 font-weight-bold" style="font-size:58px">
                         
                          <asp:Label ID="txtPreViousMonth" runat="server" Text="0.0"></asp:Label>
                      </span>
                    </div>
                    
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <%--<span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>--%>
                    <%--<span class="text-nowrap">Last update Yesterday</span>--%>
                  </p>
                </div>
              </div>
              </div>
        </div>
         <div class="col-lg-4" >
        
          <div class="card card-stats mb-4 mb-xl-0">
               <div class="panel panel-green">
                    <div class="panel-heading" style="font-size:16px;font-weight:bold;">
                       Sales Order: <asp:Label ID="txtCurrentMonthName" runat="server" Text=""></asp:Label>'<asp:Label ID="Label2" runat="server" Text=""></asp:Label> Qty (TONS)
                       <%-- <div class="icon icon-shape bg-info text-white rounded-circle shadow" style="font-size:12px;font-weight:bold;float:right">
                        TON
                      </div>--%>
                    </div>
                <div class="card-body"  style="height:145px;">
                  <div class="row">
                    <div class="col">
                      
                      <span class="h1 font-weight-bold" style="font-size:58px"> 
                          <asp:Label ID="txtCurrentMonth" runat="server" Text="0.0"></asp:Label>

                      </span>
                    </div>
                     
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    
                   <%-- <span class="text-nowrap">Last update Yesterday</span>--%>
                  </p>
                </div>
              </div>
              </div>
        </div>
         <div class="col-lg-4" >
        
          <div class="card card-stats mb-4 mb-xl-0">
               <div class="panel panel-primary">
                   <div class="panel-heading" style="font-size:16px;font-weight:bold;">
                      Sales Order: <asp:Label ID="txtNextName" runat="server" Text=""></asp:Label>'<asp:Label ID="Label3" runat="server" Text=""></asp:Label> Qty (TONS) 
                      <%-- <div class="icon icon-shape bg-info text-white rounded-circle shadow" style="font-size:12px;font-weight:bold;float:right">
                        TON
                      </div>--%>
                    </div>
                <div class="card-body"  style="height:145px;">
                  <div class="row">
                    <div class="col">
                     
                      <span class="h1 font-weight-bold" style="font-size:58px"><asp:Label ID="txtNext" runat="server" Text="0.0"></asp:Label></span>
                    </div>
                     
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    
                    
                  </p>
                </div>
              </div>
              </div>
        </div>
    </div>
    <div class="row">
     <div class="col-lg-6">
        
          <div class="card card-stats mb-6 mb-xl-0">
               <div class="panel panel-green">
                    <div class="panel-heading" style="font-size:16px;font-weight:bold;">
                       Announcement
                    </div>
                <div class="card-body"  style="height:200px;">
                  <div class="row">
                    <div class="col">
                     
                      <span class="h2 font-weight-bold mb-0">
                          <asp:Label ID="txtAnnounce" runat="server" Text=""></asp:Label>
                      
                      </span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                  <%--  <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>--%>
                  </p>
                </div>
              </div>
              </div>
        </div>
         <div class="col-lg-6">
        
          <div class="card card-stats mb-6 mb-xl-0">
               <div class="panel panel-primary">
                   <div class="panel-heading" style="font-size:16px;font-weight:bold;">
                       Delivery History
                    </div>
                <div class="card shadow" style="height:200px;">
           
            <div class="card-body">
              <!-- Chart -->
              <div class="chart" >
                  <canvas id="mmchart" class="chart-canvas" style="height:220px;width:auto;"></canvas>
                <%--<canvas id="mmchart" class="barchartx"  ></canvas>--%>
              </div>
            </div>
          </div>
              </div>
              </div>
        </div>
    </div>

        <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
 <%-- <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>--%>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.0.0"></script>
    <style>
          element.style {
    height: 200px;
    display: block;
    width: 328px;
}
    </style>
    <script type="text/javascript">
$(function () {
    LoadChart();
    
});
function LoadChart() {
    var chartType = 1;
    $.ajax({
        type: "POST",
        url: "Dashboard.aspx/GetChart",
        data: "{val: 'test'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (chData) {
            var obj = JSON.parse(chData.d);
            var aLabels =  new Array(6);
            var aDatasets1 =  new Array(6);
                var i;
                for (i = 0; i < obj.length; i++) { 
                aLabels[i] = obj[i].MonthName;
                aDatasets1[i]=obj[i].TotalQty
                }

            
                //For Bar Chart
                var ctx = document.getElementById("mmchart").getContext('2d');
                ctx.height = 900;
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    height: "230px",
                    width: "450px",
                    autoSkip: true,
                    responsive: true,
                    animation: true,
                    showDatapoints: true,
                    legend: { position: 'bottom' },
                    data: {
                        //labels: ["M", "T", "W", "T", "F", "S", "S"],
                        labels: aLabels,
                        datasets: [{
                            label: aLabels,
                            data: aDatasets1,
                            borderColor: "#10a1a3",
                            fill: true,
                            backgroundColor: "#10a1a3" }]
                         
                    }, options: {
                        events: ['click'],
                        scaleShowValues: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                                , scaleLabel: {
                                display: true,
                                labelString: 'Qty (TONS)'
                              }
                            }],
                            xAxes: [{
                                ticks: {
                                    autoSkip: false
                                }
                            }]
                        }
                    }
                });
        },
        failure: function (response) {
            alert('There was an error.');
        }
    });
}
</script>
</asp:Content>
