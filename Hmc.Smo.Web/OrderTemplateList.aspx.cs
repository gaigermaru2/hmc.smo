﻿using System;
using Hmc.Smo.Web.DBService;
using System.Data;
using System.Collections.Generic;
using AjaxControlToolkit;
using System.Linq;
using System.IO;

namespace Hmc.Smo.Web
{
    public partial class OrderTemplateList : BasePage
    {
        #region property

        public List<OrderTemplateForList> OrderTemplates
        {
            get { return Session["OrderTemplates"] as List<OrderTemplateForList>; }
            set { Session["OrderTemplates"] = value; }
        }

        public List<Customer> Customers
        {
            get { return Session["Customers"] as List<Customer>; }
            set { Session["Customers"] = value; }
        }

        private string OrderTemplateID
        {
            get { return ViewState["OrderTemplateID"] as string; }
            set { ViewState["OrderTemplateID"] = value; }
        }

        protected OrderTemplate OrderTemplate
        {
            get { return ViewState["OrderTemplate"] as OrderTemplate; }
            set { ViewState["OrderTemplate"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                LoadOrderTemplates();
                LoadOrderTemplate(null);
                LoadCustomers(null);
                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadCustomers(string orderTemplateID)
        {
            using (var sv = new ServiceClient())
            {
                //Customers = sv.SelectCustomersForList(null);
                //return;
                var cs = sv.SelectCustomersForList(null);

                if (orderTemplateID != null)
                {
                    //Customers = cs;
                    Customers = cs.Where(o => o.CustomerNo == OrderTemplate.CustomerNo).ToList();
                }
                else
                {
                    Customers = new List<Customer>();
                    foreach (var item in cs)
                    {
                        var oo = OrderTemplates.Where(o => o.CustomerNo == item.CustomerNo).FirstOrDefault();
                        if (oo == null)
                        {
                            Customers.Add(item);
                        }
                    }
                }
            }
        }

        private void LoadOrderTemplates()
        {
            using (var sv = new ServiceClient())
            {
                OrderTemplates = sv.SelectOrderTemplatesForList(null, null);
            }
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        protected void btnTemplateOK_Click(object sender, EventArgs e)
        {
            try
            {
                var FileName = ful.FileName;
                byte[] bytes = null;

                bool isOkExtension = Path.GetExtension(FileName).ToUpper() == ".XLSX";
                //lblErrorUpload.Text = string.Empty;
                if (isOkExtension)
                {
                    bytes = ReadFully(ful.FileContent);
                }

                if (OrderTemplateID == null)
                {
                    if (isOkExtension)
                    {

                        OrderTemplate.TemplateContent = bytes;
                        OrderTemplate.CustomerNo = ddlCustomer.SelectedItem.Value;
                        OrderTemplate.CustomerName = Customers.FirstOrDefault(o => o.CustomerNo == ddlCustomer.SelectedItem.Value).CustomerName;
                        OrderTemplate.AllowDownload = chkAllow.Checked;
                        OrderTemplate.CreatedTime = DateTime.Now;
                        OrderTemplate.LastUploadtedTime = DateTime.Now;
                    }
                    else
                    {
                        //show error;
                        mpeTemplate.Show();
                        return;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(FileName))
                    {
                        if (isOkExtension)
                        {
                            OrderTemplate.TemplateContent = bytes;
                        }
                        else
                        {
                            //show error;
                            mpeTemplate.Show();
                            return;
                        }
                    }

                    OrderTemplate.AllowDownload = chkAllow.Checked;
                    OrderTemplate.LastUploadtedTime = DateTime.Now;
                }

                using (var sv = new ServiceClient())
                {
                    if (OrderTemplateID == null)
                        OrderTemplate = sv.CreateOrderTemplate(OrderTemplate);
                    else
                        OrderTemplate = sv.UpdateOrderTemplate(OrderTemplate);
                }

                FileName = null;
                LoadOrderTemplates();
                //LoadCustomers();
                DataBind();

            }
            catch (Exception ex)
            {
                //ShowMessage(ex.Message);
            }
        }

        protected void grd_RowCommand(object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e)
        {
            string id = (string)e.CommandArgument;
            LoadOrderTemplate(id);
            LoadCustomers(id);

            DataBind();

            ddlCustomer.SelectedIndex = ddlCustomer.Items.IndexOf(ddlCustomer.Items.FindByValue(OrderTemplate.CustomerNo));
            mpeTemplate.Show();
        }

        private void LoadOrderTemplate(string orderTemplateID)
        {
            if (orderTemplateID != null)
            {
                using (var sv = new ServiceClient())
                {
                    OrderTemplate = sv.SelectOrderTemplateByPK(orderTemplateID, null);
                }
            }
            else
            {
                OrderTemplate = new OrderTemplate();
            }

            OrderTemplateID = orderTemplateID;

            reqUpload.Enabled = orderTemplateID == null;
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                LoadOrderTemplate(null);
                LoadCustomers(null);
                DataBind();

                mpeTemplate.Show();

            }
            catch (Exception ex)
            {
                //ShowMessage(ex.Message);
            }
        }

        //protected void btnTemplateOK_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        bool isOkExtension = Path.GetExtension(FileName).ToUpper() == ".XLSX" ;
        //        string extText;
        //        //lblErrorUpload.Text = string.Empty;

        //        if (isOkExtension)
        //        {
        //            string errMsg;
        //            var bytes = GetByteArrayFromFileName(FileName, out errMsg);
        //            if (!string.IsNullOrEmpty(errMsg))
        //            {
        //                //ShowMessage(errMsg);
        //            }
        //            else
        //            {
        //                var file = Path.GetFileName(FileName);

        //                var a = new OrderTemplate();
        //                a.TemplateContent = bytes;
        //                a.CustomerNo = ddlCustomer.SelectedItem.Value;
        //                a.CustomerName = ddlCustomer.SelectedItem.Text;
        //                a.CreatedTime = DateTime.Now;
        //                a.LastUploadtedTime = DateTime.Now;

        //                using (var sv = new ServiceClient())
        //                {
        //                    a = sv.CreateOrderTemplate(a);
        //                }

        //                FileName = null;
        //                LoadOrderTemplates();
        //                mpeTemplate.Show();
        //            }
        //        }
        //        else
        //        {
        //            //lblErrorUpload.Text = extText;
        //            mpeTemplate.Show();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowMessage(ex.Message);
        //    }
        //}


        //protected void fulTemplate_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        //{
        //    try
        //    {
        //        //lblErrorUpload.Text = string.Empty;
        //        var fulTemplate = (AsyncFileUpload)sender;
        //        var fileName = fulTemplate.FileName;
        //        string msg = null;

        //        FileName = null;

        //        if (string.IsNullOrEmpty(fileName))
        //        {
        //            msg = "AlertUploadFileNameEmp.Text";
        //            //lblErrorUpload.Text = msg;

        //            //mpeTemplate.Show();
        //            return;
        //        }

        //        var fullPath = Server.MapPath("Temp/" + fileName);
        //        var info = new System.IO.FileInfo(fullPath);
        //        if (info.Exists)
        //            info.Delete();

        //        if (!string.IsNullOrEmpty(fileName.ToString()))
        //        {
        //            fulTemplate.SaveAs(fullPath);
        //            FileName = fullPath;

        //            btnTemplateOK.Enabled = true;
        //        }
        //        else
        //        {
        //            btnTemplateOK.Enabled = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //ShowMessage(ex.Message);
        //    }
        //}
    }
}