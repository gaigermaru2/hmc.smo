﻿<%@ Page Title="Shipment Balance : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="ShipmentBalanceList.aspx.cs" Inherits="Hmc.Smo.Web.ShipmentBalanceList"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageName" runat="server">
    
    <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e">☰</a>  Shipment Balance<div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>
    </div>  
             <style>a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */ 
 th {
     text-align: center;
}
             </style>
</asp:Content>

<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
             <div class="panel panel-primary">
            <div class="panel-heading"  style="height:34px;vertical-align:top">
               <a data-toggle="collapse" href="#resdiv" aria-expanded="true" aria-controls="resdiv"  style="color:white" >Criteria</a>
            </div>           
        <!-- /.panel-heading -->
            <div id="resdiv" class="panel-collapse collapse in"  style="overflow-x: auto;">
            <div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16" style="margin-top: 0px">
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="w3-row">
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sold-to
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <asp:DropDownList ID="ddlSoldTo" runat="server" CssClass="w3-input" DataTextField="CustomerName" DataValueField="CustomerNo" AutoPostBack="true" OnSelectedIndexChanged="ddlSoldTo_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Ship-to
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <asp:DropDownList ID="ddlShipTo" runat="server" CssClass="w3-input" DataTextField="Name" DataValueField="Code">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sales Confirmation
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtQuotNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                    </div>
                    <div class="w3-row">
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Product/Grade
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtProductName" runat="server" CssClass="w3-input" MaxLength="8" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Customer P/O
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtPONo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>


                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Document No.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDocNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>

                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Shipment Status
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <asp:DropDownList ID="ddlShipmentStatus" runat="server" CssClass="w3-input">
                                        <asp:ListItem Text="All" Value="0" />
                                        <asp:ListItem Text="Complete" Value="1" />
                                        <asp:ListItem Text="Not Complete" Value="2" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>
                        <asp:Button Text="Search" runat="server" CssClass="button-img-s img-search" ID="btnSearch"
                            OnClick="btnSearch_Click" />
                    </p>
                </asp:Panel>
            </div>
                </div>
                 </div>
              <div class="panel panel-primary">
            <div class="panel-heading"  style="height:34px;vertical-align:text-top">
               <a data-toggle="collapse" href="#prddiv"  style="color:white">Results</a>
            </div>
         <div id="prddiv" class="panel-collapse collapse in" style="overflow-x: auto;">
            <div class="panel-body">
                    <asp:GridView ID="grd" runat="server" AutoGenerateColumns="false" GridLines="None"
                        ShowHeaderWhenEmpty="true" AllowSorting="true" OnSorting="grd_Sorting" AllowPaging="True"
                        PageSize="10" OnPageIndexChanging="grd_PageIndexChanging"
                       CssClass="table table-striped"   PagerStyle-CssClass="pager"
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header" Width="100%">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="No">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="SMONo" HeaderText="Doc no." SortExpression="DocDate" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"  ControlStyle-Width="120" />
                            <asp:BoundField DataField="PoNo" HeaderText="Customer P/O" SortExpression="PoNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="QuotationNo" HeaderText="Sales Conf." SortExpression="QuotationNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="QuotationItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="ShipToName" HeaderText="Ship-to" SortExpression="ShipToName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="ProductName" HeaderText="Product/Grade" SortExpression="ProductName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="PackSizeName" HeaderText="Package" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                <HeaderTemplate>
                                    Order 
                                <br />
                                    (Qty)
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# FormatHelper.NoPoint(Eval("QtySaleConfirm")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                <HeaderTemplate>
                                    Delivered
                                <br />
                                    (Qty)
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# FormatHelper.NoPoint(Eval("QtyDelivery")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                <HeaderTemplate>
                                    Delivered  
                                    <br />
                                    schedule    
                                    <br />
                                    (Qty)
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# FormatHelper.NoPoint(Eval("QtyBackOrder")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                                <HeaderTemplate>
                                    Waiting for 
                                <br />
                                    delivery schedule
                                 <br />
                                    (Qty)
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%# FormatHelper.NoPoint(Eval("QtyAfterSaleOrder")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <script type="text/javascript" src="Scripts/jquery.min.js"></script>
                    <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
                    <script type="text/javascript" src="Scripts/gridviewScroll.min.js"></script>
                    <script type="text/javascript"> 
                        function pageLoad() {
                            $('#<%=grd.ClientID%>').gridviewScroll({
                                width: 1500,
                                height: 700,
                                arrowsize: 30,
                                varrowtopimg: "Images/arrowvt.png",
                                varrowbottomimg: "Images/arrowvb.png",
                                harrowleftimg: "Images/arrowhl.png",
                                harrowrightimg: "Images/arrowhr.png"
                            });
                        }
                    </script>
                
            </div>
             </div>
                 </div>

        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>
