﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Hmc.Smo.Web.DBService;
using Hmc.Smo.Common;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;

namespace Hmc.Smo.Web
{
    public partial class OrderUploadExcel : BasePage
    {
        #region property
        public int? Step
        {
            get { return ViewState["Step"] as int?; }
            set { ViewState["Step"] = value; }
        }

        public List<Order> Orders
        {
            get { return ViewState["Orders"] as List<Order>; }
            set { ViewState["Orders"] = value; }
        }

        public List<OrderItemExcel> ValidateItems
        {
            get { return ViewState["ValidateItems"] as List<OrderItemExcel>; }
            set { ViewState["ValidateItems"] = value; }
        }
        public List<CustomerShipToMap> ShipTos
        {
            get { return ViewState["ShipTos"] as List<CustomerShipToMap>; }
            set { ViewState["ShipTos"] = value; }
        }
        public List<Product> Products
        {
            get { return ViewState["Products"] as List<Product>; }
            set { ViewState["Products"] = value; }
        }
        public OrderTemplate OrderTemplate
        {
            get { return ViewState["OrderTemplate"] as OrderTemplate; }
            set { ViewState["OrderTemplate"] = value; }
        }


        public bool CanValidateOrder
        {
            get
            {
                var rs = Orders == null || Orders.Count() == 0;
                return rs;
            }
        }
        public bool CanCreateOrders
        {
            get
            {
                var rs = ValidateItems != null && ValidateItems.Where(o => !string.IsNullOrEmpty(o.ErrorMessage)).Count() == 0
                    && (Orders == null || Orders.Count() == 0);
                return rs;
            }
        }

        public bool CanShowValidateItem
        {
            get
            {
                var rs = ValidateItems != null && ValidateItems.Count > 0;
                return rs;
            }
        }

        public bool CanShowOrder
        {
            get
            {
                var rs = Orders != null && Orders.Count() > 0;
                return rs;
            }
        }


        protected bool CanDownloadTemplate
        {
            get { return OrderTemplate != null && OrderTemplate.AllowDownload; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                Step = 1;

                using (var sv = new ServiceClient())
                {
                    OrderTemplate = sv.SelectOrderTemplateByPK(null, CurrentCustomer.CustomerNo);
                }
                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void OnValidateExcel(object sender, EventArgs e)
        {
            try
            {
                string fullPath, fileName;
                fileName = ful.FileName;

                if (string.IsNullOrEmpty(fileName))
                    return;

                fullPath = Server.MapPath("Temp/" + fileName);
                FileInfo info = new FileInfo(fullPath);
                if (info.Exists)
                    info.Delete();

                ful.SaveAs(fullPath);

                List<OrderItemExcel> itemsTemp;

                string err = ExcelHelper.GetPoItemsFromExcel(fullPath, out itemsTemp);

                List<OrderItemExcel> items = itemsTemp.Where(o => !string.IsNullOrEmpty(o.PONo) && !string.IsNullOrEmpty(o.ShipTo) && !string.IsNullOrEmpty(o.PaymentTermCode)).ToList();

                var headers = items.GroupBy(o => new { o.PONo, o.ShipTo }).ToList();

                if (items.Count == 0)
                    err += "No data in excel file";

                lblErrMsg.Text = err;

                if (!string.IsNullOrEmpty(err))
                    return;

                using (var sv = new ServiceClient())
                {
                    ShipTos = sv.SelectCustomerShipToMapsByCustomerNo(CurrentCustomer.CustomerNo);
                    Products = sv.SelectProductByCustomerNo(CurrentCustomer.CustomerNo);
                    var ps_temp = sv.SelectProductExceptCustomerNo(CurrentCustomer.CustomerNo);
                    Products.AddRange(ps_temp);

                    var paymentTerms = sv.SelectPaymentTermByCustomerNo(CurrentCustomer.CustomerNo);
                    foreach (var item in items)
                    {
                        var errs = new List<string>();
                        var paymentTerm = paymentTerms.Where(o => o.PaymentTermCode == item.PaymentTermCode).FirstOrDefault();
                        var shipTo = ShipTos.Where(o => o.ShipToCode == item.ShipTo).FirstOrDefault();

                        if (string.IsNullOrEmpty(item.ShipTo))
                            errs.Add("Require Ship to");
                        if (string.IsNullOrEmpty(item.PONo))
                            errs.Add("Require PO No");
                        if (string.IsNullOrEmpty(item.PaymentTermCode))
                            errs.Add("Require Payment term");

                        if (shipTo == null)
                            errs.Add("Ship to not found");
                        else
                            item.ShipToText = shipTo.ShipToName;

                        if (paymentTerm == null)
                            errs.Add("Payment term not found");
                        else
                            item.PaymentTermText = paymentTerm.PaymentTermDesc;

                        if (errs.Count != 0)
                        {
                            item.ErrorMessage = string.Join(",", errs);
                        }
                        else
                        {
                            var packageSizes = sv.SelectPackageSizesByProductNo(item.ProductNo);

                            string errItem;
                            OrderItem orderItem;
                            var isOk = CheckProduct(item.ProductNo, item.MaterialNo, item.UnitPrice, item.PackageSizeCode, item.Quantity, item.DeliveryDate, Products, packageSizes, out orderItem, out errItem, null, null, item);
                        }
                    }
                }

                ValidateItems = items;

                Step = 2;
                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        public static decimal GetTax(string shipToCode, List<CustomerShipToMap> shipTos)
        {
            using (var sv = new ServiceClient())
            {
                var shipTo = shipTos.Where(x => x.ShipToCode == shipToCode).FirstOrDefault();
                decimal taxClass = shipTo == null ? 0 : sv.SelectTaxPercentByTaxClass(shipTo.TaxClass);
                return taxClass;
            }
        }

        protected void OnCreateOrder(object sender, EventArgs e)
        {
            try
            {
                var hds = ValidateItems.GroupBy(o => new { o.PONo, o.PaymentTermCode, o.ShipTo, o.PODate, o.PaymentTermText, o.ShipToText }, (key, group) => new { Key = key, Items = group.ToList() }).ToList();

                Orders = new List<Order>();
                string statusDraftText;
                using (var sv = new ServiceClient())
                {
                    statusDraftText = sv.SelectOrderStatusByPK("1").OrderStatusText;
                }
                foreach (var hd in hds)
                {
                    var order = new Order()
                    {
                        CustomerGroupCode = CurrentCustomer.CustomerGroupCode,
                        CustomerName = CurrentCustomer.CustomerName,
                        CustomerNo = CurrentCustomer.CustomerNo,
                        OrderDate = DateTime.Now,
                        PaymentTermCode = hd.Key.PaymentTermCode,
                        PaymentTermText = hd.Key.PaymentTermText,
                        PODate = DateTime.ParseExact(hd.Key.PODate, DataFormat.Date, null),
                        PONo = hd.Key.PONo,
                        ShipToCode = hd.Key.ShipTo,
                        ShipToText = hd.Key.ShipToText,
                        OrderStatusCode = "1",
                        OrderStatusText = statusDraftText,
                    };

                    var orderItems = new List<OrderItem>();

                    var itemNo = 1;
                    foreach (var item in hd.Items)
                    {
                        var p = Products.Where(x => x.ProductNo == item.ProductNo).FirstOrDefault();

                        var oIt = new OrderItem();
                        if (!string.IsNullOrEmpty(item.DeliveryDate))
                            oIt.DeliveryDate = DateTime.ParseExact(item.DeliveryDate, DataFormat.Date, null);
                        oIt.ProductNo = p.ProductNo;
                        oIt.ProductText = p.ProductText;
                        oIt.MaterialNo = item.MaterialNo;
                        oIt.MaterialText = item.MaterialNo;
                        oIt.UnitPrice = decimal.Parse(item.UnitPrice);
                        oIt.PackageSizeCode = item.PackageSizeCode;
                        oIt.PackageSizeText = item.PackageSize;
                        oIt.Quantity = int.Parse(item.Quantity);
                        oIt.ItemNo = itemNo;
                        orderItems.Add(oIt);

                        itemNo++;
                    }
                    var tax = GetTax(hd.Key.ShipTo, ShipTos);
                    RecalculateOrder(order, orderItems, tax);
                    order = SaveData(order, orderItems);
                    Orders.Add(order);
                }
                Step = 3;

                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }


        private Order SaveData(Order order, List<OrderItem> orderItems)
        {
            List<string> errs = new List<string>();
            if (order.TotalAmount >= 9999999999 || order.TotalAmount <= -9999999999)
            {
                errs.Add("Total amount must be between -9,999,999,999 and 9,999,999,999");
            }
            else
            {
                order.CustomerNo = CurrentCustomer.CustomerNo;
                order.CustomerName = CurrentCustomer.CustomerName;

                using (var sv = new ServiceClient())
                {
                    order = sv.CreateOrder(order, orderItems, null);
                }
            }

            return order;
        }

        protected void OnUploadNewFile(object sender, EventArgs e)
        {
            try
            {
                ValidateItems = null;
                Orders = null;
                Step = 1;

                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void btnDownloadTemplate_Click(object sender, EventArgs e)
        {
            var fileName = "template_" + OrderTemplate.CustomerNo + ".xlsx";
            var fullPath = Server.MapPath("Temp/" + fileName);

            File.WriteAllBytes(fullPath, OrderTemplate.TemplateContent);
            Thread.Sleep(2000);

            DownloadFileToClient(fileName, fullPath);
        }

        private static void DownloadFileToClient(string fileName, string fullPath)
        {
            System.Web.HttpRequest request = System.Web.HttpContext.Current.Request;
            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/plain";
            response.AddHeader("Content-Disposition",
                               "attachment; filename=" + fileName + ";");
            response.TransmitFile(fullPath);
            response.Flush();
            response.End();
        }

        protected void OnSubmit(object sender, EventArgs e)
        {
            string statusText;
            var statusCode = CurrentUser.HasApprover ? OrderStatusCode.WaitingForApproval : OrderStatusCode.SubmittedToHMC;
            var caseNo = CurrentUser.HasApprover ? 3 : 14;

            using (var sv = new ServiceClient())
            {
                statusText = sv.SelectOrderStatusByPK(statusCode).OrderStatusText;
            }
            foreach (var order in Orders)
            {
                using (var sv = new ServiceClient())
                {
                    sv.ChangeOrderStatus(order.OrderNo, CurrentUser.Username, OrderStatusCode.Draft, string.Empty);
                    sv.ChangeOrderStatus(order.OrderNo, CurrentUser.Username, statusCode, string.Empty);
                    var orderItems = sv.SelectOrderItemsByOrderNo(order.OrderNo);
                    sv.SendEmailByCaseNo(caseNo, CurrentUser, order, orderItems, string.Empty, null);
                }
                order.OrderStatusText = statusText;
                //if (CurrentUser.HasApprover)
                //{
                //    using (var sv = new ServiceClient())
                //    {
                //        sv.ChangeOrderStatus(order.OrderNo, CurrentUser.Username, OrderStatusCode.WaitingForApproval, "");
                //        var orderItems = sv.SelectOrderItemsByOrderNo(order.OrderNo);
                //        sv.SendEmailByCaseNo(3, CurrentUser, order, orderItems, string.Empty, null);
                //    }
                //}
                //else
                //{
                //    using (var sv = new ServiceClient())
                //    {
                //        sv.ChangeOrderStatus(order.OrderNo, CurrentUser.Username, OrderStatusCode.SubmittedToHMC, "");
                //        var orderItems = sv.SelectOrderItemsByOrderNo(order.OrderNo);
                //        sv.SendEmailByCaseNo(14, CurrentUser, order, orderItems, string.Empty, null);
                //    }
                //}
            }


            if (CurrentUser.HasApprover)
            {
                DateTime today = DateTime.Today;
                string msg = string.Empty;

                DateTime dueDate = today;

                if (today.DayOfWeek != DayOfWeek.Sunday)
                {
                    int offset = today.DayOfWeek - DayOfWeek.Monday;

                    DateTime lastMonday = today.AddDays(-offset);
                    DateTime nextSunday = lastMonday.AddDays(6);

                    dueDate = nextSunday;
                }

                msg = string.Format(Msg.MSG_ORDER_VALID_TH, dueDate.ToString(DataFormat.Date))
                   + Environment.NewLine
                   + string.Format(Msg.MSG_ORDER_VALID_EN, dueDate.ToString(DataFormat.Date));
            }
            Step++;

            DataBind();
        }
    }
}