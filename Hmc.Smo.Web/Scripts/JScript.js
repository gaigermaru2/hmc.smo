﻿function fnOnUpdateValidators() {
    for (var i = 0; i < Page_Validators.length; i++) {
        var val = Page_Validators[i];
        var ctrl = document.getElementById(val.controltovalidate);
        if (ctrl != null && ctrl.style != null) {
            if (!val.isvalid)
                ctrl.style.background = '#FFAAAA';
            else
                ctrl.style.backgroundColor = '';
        }
    }
}
function cancelBack() {
    if ((event.keyCode == 8 ||
           (event.keyCode == 37 && event.altKey) ||
           (event.keyCode == 39 && event.altKey))
            &&
           (event.srcElement.form == null || event.srcElement.isTextEdit == false)
          ) {
        return false;
    }
}

