﻿<%@ Page Title="Shipment Detail : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="ShipmentDetailList.aspx.cs" Inherits="Hmc.Smo.Web.ShipmentDetailList"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ContentPlaceHolderID="cphPageName" runat="server">
    <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e">☰</a>  Shipment Detail<div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>
    </div>  
             <style>a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */
 th {
     text-align: center;
}
             </style>
    <script>
        function UserAction() {
    var xhttp = new XmlHttpRequest();
    xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) {
             alert(this.responseText);
         }
    };
    xhttp.open("POST", "https://digitalqas.hmcpolymers.com/api/authentication/login", true);
            xhttp.setRequestHeader("Content-type", "application/json");
            xmlhttp.send(JSON.stringify({ "username": "hello@user.com", "password": "Redrotrams","deviceID":"-" }));
    
}

    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
                         <div class="panel panel-primary">
            <div class="panel-heading"  style="height:36px;vertical-align:top">
               <a data-toggle="collapse" href="#resdiv" aria-expanded="true" aria-controls="resdiv"  style="color:white">Criteria</a>
            </div>           
        <!-- /.panel-heading -->
            <div id="resdiv" class="panel-collapse collapse in"  style="overflow-x: auto;">
            <div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16" style="margin-top: 0px">
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                
                    <div class="w3-row">
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sold-to
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <asp:DropDownList ID="ddlSoldTo" runat="server" CssClass="w3-input" DataTextField="CustomerName" DataValueField="CustomerNo" AutoPostBack="true" OnSelectedIndexChanged="ddlSoldTo_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Ship-to
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <asp:DropDownList ID="ddlShipTo" runat="server" CssClass="w3-input" DataTextField="Name" DataValueField="Code">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sales Confirmation
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtQuotNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sales Order no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtSaleOrder" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>

                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Delivery no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDeliveryNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Delivery date
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDlvDate" runat="server" CssClass="w3-input" MaxLength="10" />
                                <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="txtDlvDate" runat="server" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Shipment no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtShipmentNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Invoice no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                         <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Product/Grade
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtProductName" runat="server" CssClass="w3-input" MaxLength="8" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Customer P/O
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtPoNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                       
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Document No.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDocNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Inv.Type
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <asp:DropDownList ID="ddlInvoiceType" runat="server" CssClass="w3-input" DataTextField="Name" DataValueField="Code" AutoPostBack="true" OnSelectedIndexChanged="ddlInvoiceType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>
                        <asp:Button Text="Search" runat="server" CssClass="button-img-s img-search" ID="btnSearch"
                            OnClick="btnSearch_Click" /> 
                        <%--<input type="button"  onclick="UserAction();"></input>--%>
                        <%--<asp:Button Text="DO" runat="server" CssClass="button-img-s img-search" ID="Button1"   OnClick="btnDO_Click" 
                             />
                       --%>
                         <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    </p>
                </asp:Panel>
            </div>
                </div>
                             </div>
           <div class="panel panel-primary">
            <div class="panel-heading"  style="height:36px;vertical-align:text-top">
               <a data-toggle="collapse" href="#prddiv"  style="color:white">Results</a>
            </div>
         <div id="prddiv" class="panel-collapse collapse in" style="overflow-x: auto;">
            <div class="panel-body">
                    <asp:GridView ID="grd" runat="server" AutoGenerateColumns="false" GridLines="None"
                        ShowHeaderWhenEmpty="true" AllowSorting="true" OnSorting="grd_Sorting" AllowPaging="True"
                        PageSize="10" OnPageIndexChanging="grd_PageIndexChanging"
                        DataSource='<%# ShipmentDetailDT %>'
                        CssClass="table table-striped"   PagerStyle-CssClass="pager"
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header" Width="100%"
                        OnRowCommand="grd_RowCommand">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="No">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="OrderNo" HeaderText="Doc no." SortExpression="OrderNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="PoNo" HeaderText="Customer P/O" SortExpression="PoNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="QuotationNo" HeaderText="Sales Confirm" SortExpression="QuotationNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="QuotationItem" HeaderText="Item" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="SaleOrder" HeaderText="Sales Order" SortExpression="SaleOrder" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />

                            <asp:BoundField DataField="SoType" HeaderText="Sales Order Type" SortExpression="SoType" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />

                            <%--<asp:BoundField DataField="DeliveryOrder" HeaderText="Delivery" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />--%>

                            <asp:TemplateField HeaderText="DeliveryOrder" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("DeliveryOrder") %>' runat="server" CommandName="ORDER_DOWNLOAD" CommandArgument='<%# Eval("DeliveryOrder")  %>' />
                                </ItemTemplate>
                            </asp:TemplateField>




                            <asp:BoundField DataField="ShipmentNo" HeaderText="Shipment" SortExpression="ShipmentNo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Invoice" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("InvoiceNo") %>' runat="server" CommandName="INV_DOWNLOAD" CommandArgument='<%# Eval("MailPDFId")  %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:BoundField DataField="invoicetype" HeaderText="Inv.Type" SortExpression="invoicetype" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            
                            <asp:BoundField DataField="ShipToName" HeaderText="Ship-to" SortExpression="ShipToName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText="Delivery Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# FormatHelper.DateToStr(Eval("DeliveryDate")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ProductName" HeaderText="Product/Grade" SortExpression="ProductName" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="Quantity" HeaderText="Quantity/KG" SortExpression="Quantity" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0}" />
                        </Columns>
                    </asp:GridView>
                </div>
             </div>
                    <script type="text/javascript" src="Scripts/jquery.min.js"></script>
                    <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
                    <script type="text/javascript" src="Scripts/gridviewScroll.min.js"></script>
                    <script type="text/javascript"> 
                        function pageLoad() {
                            $('#<%=grd.ClientID%>').gridviewScroll({
                                width: 1900,
                                height: 450,
                                arrowsize: 30,
                                varrowtopimg: "Images/arrowvt.png",
                                varrowbottomimg: "Images/arrowvb.png",
                                harrowleftimg: "Images/arrowhl.png",
                                harrowrightimg: "Images/arrowhr.png"
                            });
                        }
               
        //$(window).bind("load", function() {
        //    var ds = document.getElementById("maindiv");
        //    ds.style.display = 'none';
        //});
                    </script>
                
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
