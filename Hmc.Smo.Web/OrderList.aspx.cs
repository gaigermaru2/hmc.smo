﻿using Hmc.Smo.Common;
using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class OrderList : BasePage
    {
        private const string CSS_SORT = "img-sort";
        private const string CTRL_chkSelect = "chkSelect";
        private const string FORMAT_FILENAME = "SmartOrder_{0}.xlsx";
        private const string FORMAT_FILENAME_DATETIME = "yyyyMMddHHmm";
        #region property

        public List<string> SearchCriterias
        {
            get { return Session["SearchCriterias"] as List<string>; }
            set { Session["SearchCriterias"] = value; }
        }
        public List<OrderForHMC> Orders
        {
            get { return Session["Orders"] as List<OrderForHMC>; }
            set { Session["Orders"] = value; }
        }
        public DataTable OrdersDT
        {
            get { return Session["OrdersDT"] as DataTable; }
            set { Session["OrdersDT"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                SetTextBoxSelectAllOnFocus(txtCustCodeFrom);
                SetTextBoxSelectAllOnFocus(txtCustName);
                SetTextBoxSelectAllOnFocus(txtOrderNo);
                SetTextBoxSelectAllOnFocus(txtPONo);
                SetTextBoxSelectAllOnFocus(txtProduct);
                SetTextBoxSelectAllOnFocus(txtSaleConfirmNoFrom);
                SetTextBoxSelectAllOnFocus(txtSaleConfirmNoTo);

                IsShowCriteria = true;
                SortImage = new Image();
                SortImage.CssClass = CSS_SORT;

                List<OrderStatus> status = OrderStatuses.Where(o => o.OrderStatusCode != OrderStatusCode.New).ToList();

                ddlStatus.DataSource = status;
                ddlStatus.DataBind();

                LoadSearchCriteria();

                using (var sv = new ServiceClient())
                {
                    var sgms = sv.SelectSegmentManagersForList();
                    sgms.Insert(0, new SegmentManager() { SegmentManagerCode = string.Empty, SegmentManagerName = string.Empty });
                    ddlSaleSegMgrFrom.DataSource = sgms;
                    ddlSaleSegMgrTo.DataSource = sgms;
                    ddlSaleSegMgrFrom.DataBind();
                    ddlSaleSegMgrTo.DataBind();
                }

                LoadOrders();

                btnNewOrder.DataBind();
                btnApproveMulti.DataBind();
                btnUploadExcel.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadSearchCriteria()
        {
            if (SearchCriterias == null)
            {
                string val = IsUser ? OrderStatusCode.DraftAndWaitingForApproveAndRejected
                    : IsManager ? OrderStatusCode.WaitingForApproval
                    : IsHMC ? OrderStatusCode.SubmittedToHMC
                    : OrderStatusCode.All;

                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(val));
            }
            else
            {
                txtCustName.Text = SearchCriterias[0];
                txtCustCodeFrom.Text = SearchCriterias[1];
                txtCDateFrom.Text = SearchCriterias[2];
                txtCDateTo.Text = SearchCriterias[3];
                txtSaleConfirmNoFrom.Text = SearchCriterias[4];
                txtSaleConfirmNoTo.Text = SearchCriterias[5];
                txtProduct.Text = SearchCriterias[6];
                txtPONo.Text = SearchCriterias[7];
                txtOrderNo.Text = SearchCriterias[8];
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(SearchCriterias[9]));
                ddlSaleSegMgrFrom.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(SearchCriterias[10]));
                ddlSaleSegMgrTo.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(SearchCriterias[11]));
            }
        }

        private void SaveSearchCriteria()
        {
            SearchCriterias = new List<string>();

            SearchCriterias.Add(txtCustName.Text);
            SearchCriterias.Add(txtCustCodeFrom.Text);
            SearchCriterias.Add(txtCDateFrom.Text);
            SearchCriterias.Add(txtCDateTo.Text);
            SearchCriterias.Add(txtSaleConfirmNoFrom.Text);
            SearchCriterias.Add(txtSaleConfirmNoTo.Text);
            SearchCriterias.Add(txtProduct.Text);
            SearchCriterias.Add(txtPONo.Text);
            SearchCriterias.Add(txtOrderNo.Text);
            SearchCriterias.Add(ddlStatus.SelectedValue);
            SearchCriterias.Add(ddlSaleSegMgrFrom.SelectedValue);
            SearchCriterias.Add(ddlSaleSegMgrTo.SelectedValue);
        }

        private void LoadOrders()
        {
            string status = ddlStatus.SelectedValue;

            string statusConcat = string.Empty;
            statusConcat = status == OrderStatusCode.DraftAndWaitingForApproveAndRejected
               ? statusConcat = string.Format("{0},{1},{2}", OrderStatusCode.Draft,
                    OrderStatusCode.WaitingForApproval, OrderStatusCode.Rejected)
               : statusConcat = status;

            string custFrom, custTo;

            if (CurrentUser.Role == Role.HMCSaleCo)
            {
                custFrom = txtCustCodeFrom.Text.Trim();
                custTo = txtCustCodeFrom.Text.Trim();
            }
            else
            {
                custFrom = CurrentUser.CustomerNo;
                custTo = CurrentUser.CustomerNo;
            }
            using (var sv = new ServiceClient())
            {
                var dateFrom = ConvertEmptyToNullDateTime(txtCDateFrom.Text);
                var dateTo = ConvertEmptyToNullDateTime(txtCDateTo.Text);

                /*null=paymentterm*/
                Orders = sv.SelectOrdersForHMC(txtCustName.Text.Trim(),
                    custFrom, custTo,
                    dateFrom, dateTo,
                    txtSaleConfirmNoFrom.Text.Trim(),
                    txtSaleConfirmNoTo.Text.Trim(),
                    null,
                    txtProduct.Text.Trim(),
                    txtPONo.Text.Trim(),
                    txtOrderNo.Text.Trim(),
                    statusConcat,
                    ddlSaleSegMgrFrom.SelectedValue,
                    ddlSaleSegMgrTo.SelectedValue);

                OrdersDT = ConvertToDataTable<OrderForHMC>(Orders);

                grdOrder.DataSource = OrdersDT;
                grdOrder.DataBind();

            }

            SaveSearchCriteria();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                LoadOrders();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grdOrder_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                OrdersDT = ConvertToDataTable<OrderForHMC>(Orders);
                SetSortDirection(CurrentSortDirection);
                if (OrdersDT != null)
                {
                    //Sort the data.
                    SortExpression = e.SortExpression;

                    OrdersDT.DefaultView.Sort = e.SortExpression + " " + NewSortDirection;
                    grdOrder.DataSource = OrdersDT;
                    grdOrder.DataBind();
                    CurrentSortDirection = NewSortDirection;
                }

                int columnIndex = 0;
                foreach (DataControlFieldHeaderCell headerCell in grdOrder.HeaderRow.Cells)
                {
                    if (headerCell.ContainingField.SortExpression == e.SortExpression)
                    {
                        columnIndex = grdOrder.HeaderRow.Cells.GetCellIndex(headerCell);
                    }
                }
                grdOrder.HeaderRow.Cells[columnIndex].Controls.Add(SortImage);
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grdOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header ||
                    e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[0].Visible = IsManager;//checkbox
                    e.Row.Cells[4].Visible = IsHMC;
                    
                    e.Row.Cells[5].Visible = IsHMC;
                }
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadOrders();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grdOrder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdOrder.PageIndex = e.NewPageIndex;
                LoadOrders();
                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            string fileName = string.Format(FORMAT_FILENAME, DateTime.Now.ToString(FORMAT_FILENAME_DATETIME));

            var dt = new DataTable("Orders");
            dt.Columns.Add("Document No");

            if (IsHMC)
            {
                dt.Columns.Add("Customer Code");
                dt.Columns.Add("Customer Name");
            }

            dt.Columns.Add("Product");
            dt.Columns.Add("Package Size");
            dt.Columns.Add("Quantity", System.Type.GetType("System.Int32"));
            dt.Columns.Add("Unit Price", System.Type.GetType("System.Decimal"));
            dt.Columns.Add("Created Date");
            dt.Columns.Add("Status");
            dt.Columns.Add("PO No");
            dt.Columns.Add("Sale Confirm No");

            foreach (DataRowView o in OrdersDT.DefaultView)
            {
                var dr = dt.NewRow();
                dr["Document No"] = o["OrderNo"];

                if (IsHMC)
                {
                    dr["Customer Code"] = o["CustomerNo"];
                    dr["Customer Name"] = o["CustomerName"];
                }

                string dateStr = DateTime.Parse(o["OrderDate"].ToString()).ToString("dd/MM/yyyy HH:mm");

                dr["Product"] = o["ProductText"];
                dr["Package Size"] = o["PackageSizeText"];
                dr["Quantity"] = o["Quantity"];
                dr["Unit Price"] = o["UnitPrice"];
                dr["Created Date"] = dateStr;
                dr["Status"] = o["OrderStatusText"];
                dr["PO No"] = o["PONo"];
                dr["Sale Confirm No"] = o["SaleConfirmNo"];
                dt.Rows.Add(dr);
            }

            string col = string.Format("A1:{0}1", IsHMC ? "K" : "I");

            var ba = ExcelHelper.DumpExcel(dt, col, "Orders");
            DownloadFile(fileName, ba);
        }

        protected void TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string id = ((TextBox)sender).ID;

                var ctrl = pnlSearch.FindControl(id);
                if (ctrl != null)
                    ctrl.Focus();

            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void btnApproveMulti_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> orderNos = GetOrderNoSelected();
                using (var sv = new ServiceClient())
                {
                    orderNos = orderNos.GroupBy(o => o).Select(o => o.Key).OrderBy(o => o).ToList();

                    foreach (var orderNo in orderNos)
                    {
                        ChangeOrderStatusAndRefreshPage(orderNo, OrderStatusCode.SubmittedToHMC);
                    }
                }

                LoadOrders();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private List<string> GetOrderNoSelected()
        {
            var rs = new List<string>();
            foreach (GridViewRow row in grdOrder.Rows)
            {
                var chkSelect = (CheckBox)row.FindControl(CTRL_chkSelect);
                if (chkSelect.Checked)
                {
                    rs.Add(chkSelect.ToolTip);
                }
            }
            return rs;
        }

        public string ProcessMyDataItem(object myValue,int v)
        {
            if (myValue == null)
            {
                return "";
            }
            else
            {
                int len = myValue.ToString().Length;
                if (len < v)
                    return myValue.ToString();
                else
                    return myValue.ToString().Substring(0, v);
            }

        }

    }
}