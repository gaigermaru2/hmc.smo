﻿using Hmc.Smo.Common;
using System;

namespace Hmc.Smo.Web
{
    public class FormatHelper
    {
        public string TwoPoint(object obj)
        {
            string fmt = "#,##0.00";
            string rs = string.Empty;

            if (obj != null && obj is decimal)
            {
                var d = (decimal)obj;

                rs = d.ToString(fmt);
            }
            else
                throw new Exception("This function accept only decimal");

            return rs;
        }

        public string NoPoint(object obj)
        {
            string fmt = "#,##0";
            string rs = string.Empty;

            if (obj != null && obj is decimal)
            {
                var d = (decimal)obj;
                rs = d.ToString(fmt);
            }
            else if (obj != null && obj is int)
            {
                var d = (int)obj;
                rs = d.ToString(fmt);
            }
            else
                throw new Exception("This function accept only decimal/int");

            return rs;
        }

        public string DateToStr(object obj)
        {
            string rs;

            if (obj == null || obj.ToString() == "")
            {
                rs = string.Empty;
            }
            else if (obj != null && obj is DateTime)
            {
                var d = (DateTime)obj;

                rs = d.ToString(DataFormat.Date);
            }
            else if (obj != null && obj is DateTime?)
            {
                var d = (DateTime?)obj;

                rs = d.HasValue ? d.Value.ToString(DataFormat.Date) : string.Empty;
            }
            else
                throw new Exception("This function accept only DateTime/DateTime?");

            return rs;
        }

        public DateTime? StrToDate(string date)
        {
            if (!string.IsNullOrEmpty(date))
                return DateTime.ParseExact(date, DataFormat.Date, null);
            return null;
        }
    }
}