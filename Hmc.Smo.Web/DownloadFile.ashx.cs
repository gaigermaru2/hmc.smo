﻿using System.IO;
using System.Web;

namespace Hmc.Smo.Web
{
    public class DownloadFile : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = HttpContext.Current.Request;
            string path = Path.Combine(HttpContext.Current.Server.MapPath("~/."), "Temp");
            string fileName = request.QueryString["file"];
            string v = request.QueryString["v"];
            string fullPath = Path.Combine(path, fileName);

            bool isPdf = Path.GetExtension(fileName).ToUpper() == ".PDF";
            bool isView = v == "1";
            string contentDisposition = isView ? "inline" : "attachment";

            HttpResponse response = HttpContext.Current.Response;
            response.ClearContent();
            response.Clear();
            response.ContentType = isPdf ? "application/pdf" : "text/plain";
            response.AddHeader("Content-Disposition", string.Format("{0}; filename={1};", contentDisposition, fileName));
            response.TransmitFile(fullPath);
            response.Flush();
            response.End();

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}