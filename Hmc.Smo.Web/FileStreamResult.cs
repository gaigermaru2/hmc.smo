﻿using System.IO;

namespace Hmc.Smo.Web
{
    internal class FileStreamResult
    {
        private Stream fileStream;
        private object fileContentType;

        public FileStreamResult(Stream fileStream, object fileContentType)
        {
            this.fileStream = fileStream;
            this.fileContentType = fileContentType;
        }
    }
}