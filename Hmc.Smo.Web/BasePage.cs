﻿using AjaxControlToolkit;
using Hmc.Smo.Common;
using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public class DropDownValue
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class BasePage : Page
    {
        public WebConfig Config
        {
            get
            {
                var config = Application["Config"] as WebConfig;
                if (config == null)
                {
                    using (var sv = new ServiceClient())
                    {
                        config = sv.GetWebConfig();
                        Application["Config"] = config;
                    }
                }
                return config;
            }
        }

        #region Format

        public FormatHelper FormatHelper
        {
            get
            {
                FormatHelper f = Session["FormatHelper"] as FormatHelper;
                if (f == null)
                {
                    f = new FormatHelper();
                    Session["FormatHelper"] = f;
                }

                return f;
            }
        }

        //protected static string FormatDateToString(DateTime? date)
        //{
        //    if (date.HasValue)
        //        return date.Value.ToString(DataFormat.Date);
        //    return string.Empty;
        //}

        //protected static DateTime? FormatStringToDate(string date)
        //{
        //    if (!string.IsNullOrEmpty(date))
        //        return DateTime.ParseExact(date, DataFormat.Date, null);
        //    return null;
        //}

        public static string DateTimeFormat(object oDt)
        {
            if (oDt != null)
            {
                var dt = Convert.ToDateTime(oDt);
                return dt.ToString(DataFormat.DateTime);
            }
            return string.Empty;
        }

        #endregion


        protected void ChangeOrderStatusAndRefreshPage(string orderNo, string newStatus)
        {
            string reason = string.Empty;

            //if (Arg != null && (Arg.Contains("Reject") || Arg.Contains("Cancel")))
            //{
            //    reason = string.Format("{0}, {1}", ddlReason.SelectedItem.Text, txtReason.Text);
            //}

            using (var sv = new ServiceClient())
            {
                var order = sv.ChangeOrderStatus(orderNo, CurrentUser.Username, newStatus, reason);
                var items = sv.SelectOrderItemsByOrderNo(orderNo);
                sv.SendEmailByCaseNo(4, CurrentUser, order, items, null, null);
            }
        }



        protected static byte[] GetByteArrayFromFileName(string fileName, out string sbErr)
        {
            byte[] bytes = null;
            sbErr = string.Empty;

            try
            {
                bytes = System.IO.File.ReadAllBytes(fileName);

                Thread.Sleep(2000);
            }
            catch (Exception ex)
            {
                sbErr = ex.Message;
            }
            return bytes;
        }

        protected static void RecalculateOrder(Order Order, List<OrderItem> OrderItems, decimal Tax)
        {
            foreach (var it in OrderItems)
            {
                it.Amount = it.Quantity * it.UnitPrice;
                it.TaxAmount = (it.Amount * Tax) / 100;
            }

            var totalQty = OrderItems.Sum(o => o.Quantity);
            decimal sumAmount = OrderItems.Sum(o => o.Amount);
            decimal vat = OrderItems.Sum(o => o.TaxAmount);
            decimal totalAmount = sumAmount + vat;

            Order.TotalQty = totalQty;
            Order.Amount = sumAmount;
            Order.TotalTaxAmount = vat;
            Order.TotalAmount = totalAmount;
        }

        protected static bool CheckProduct(string productNo, string materialNo, string unitPrice, string packageSizeCode, string qty, string deliveryDate,
            List<Product> products, List<PackageSize> packageSizes,
            out OrderItem item, out string err,
            TextBox unitPriceTextbox = null, TextBox qtyTextbox = null, OrderItemExcel orderItemExcel = null)
        {
            List<string> errs = new List<string>();
            bool isOkPrice = true;
            bool isOkQty = true;
            bool isOkProduct = true;
            bool isOkPackage = true;

            item = null;

            err = string.Empty;

            //check price
            decimal price;
            isOkPrice = decimal.TryParse(unitPrice, out price);

            if (isOkPrice)
            {
                isOkPrice = price >= 1 && price <= 9999999999;
                if (isOkPrice)
                {
                    int i = unitPrice.IndexOf('.');

                    if (i > -1)
                    {
                        string beforeDot = unitPrice.Substring(0, i);
                        string afterDot = unitPrice.Substring(i);

                        if (afterDot.Length > 3)
                        {
                            unitPrice = beforeDot + afterDot.Substring(0, 3);
                        }
                    }
                }
                else
                {
                    errs.Add(Msg.PriceMustBiggerZero);
                }
            }
            else
            {
                errs.Add(Msg.PriceMustBeDecimal);
            }

            if (unitPriceTextbox != null)
            {
                var color = isOkPrice ? Color.NORMAL_COLOR : Color.ERROR_COLOR;
                unitPriceTextbox.BackColor = System.Drawing.ColorTranslator.FromHtml(color);
            }

            //check product
            var product = products.Where(p => p.ProductNo == productNo).FirstOrDefault();
            if (product == null)
            {
                errs.Add("Product not found");
                isOkProduct = false;
            }

            int qtyInt = int.Parse(qty);
            isOkQty = qtyInt >= 1 && qtyInt <= 9999999;

            //check pack size with qty
            var pck = packageSizes.Where(p => p.PackageSizeCode == packageSizeCode).FirstOrDefault();
            if (pck == null)
            {
                errs.Add("Package Size not found");
                isOkPackage = false;
            }
            else
            {
                if (isOkQty)
                {
                    int mod = qtyInt % pck.Weight;
                    isOkQty = mod == 0;
                }
            }

            if (!isOkQty)
                errs.Add(Msg.QtyPackageSizeDoesNotMatch);

            if (qtyTextbox != null)
            {
                var color = isOkPrice ? Color.NORMAL_COLOR : Color.ERROR_COLOR;
                qtyTextbox.BackColor = System.Drawing.ColorTranslator.FromHtml(color);
            }

            DateTime? dlvDate = null;
            bool isOkDlvDate = true;

            if (!string.IsNullOrEmpty(deliveryDate) && deliveryDate != "__/__/____")
            {
                DateTime dlvDateTemp;
                isOkDlvDate = DateTime.TryParseExact(deliveryDate, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dlvDateTemp);
                if (isOkDlvDate)
                    dlvDate = dlvDateTemp;
                else
                    errs.Add("Delivery date format is incorrect (dd/MM/yyyy)");
            }

            bool isOk = isOkPrice && isOkQty && isOkDlvDate & isOkProduct & isOkPackage;

            if (isOk)
            {
                item = new OrderItem()
                {
                    UnitPrice = price,
                    Quantity = qtyInt,
                    ProductNo = productNo,
                    MaterialNo = materialNo,
                    PackageSizeCode = pck.PackageSizeCode,
                    PackageSizeText = pck.PackageSizeText,
                    DeliveryDate = dlvDate,
                };

                if (orderItemExcel != null)
                {
                    orderItemExcel.PackageSize = pck.PackageSizeText;
                    orderItemExcel.ProductText = product.ProductText;
                }
            }
            else
            {
                err = string.Join(",", errs);
                if (orderItemExcel != null)
                {
                    orderItemExcel.PackageSize = packageSizeCode;
                    orderItemExcel.ProductText = productNo;
                    orderItemExcel.ErrorMessage = err;
                }
            }

            return isOk;
        }


        #region property

        public bool IsHMC
        {
            get { return CurrentUser.Role == Role.HMCSaleCo; }
        }
        public bool IsManager
        {
            get { return CurrentUser.Role == Role.CustomerManager; }
        }
        public bool IsUser
        {
            get { return CurrentUser.Role == Role.CustomerUser; }
        }

        public bool IsShowCriteria
        {
            set { Session["IsShowCriteria"] = value; }
        }
        public string BPUrlRedirect
        {
            get { return Session["BPUrlRedirect"] as string; }
            set { Session["BPUrlRedirect"] = value; }
        }

        public List<DBService.OrderStatus> OrderStatuses
        {
            get
            {
                var os = Session["OrderStatuses"] as List<DBService.OrderStatus>;

                if (os == null || os.Count == 0)
                {
                    using (var sv = new ServiceClient())
                    {
                        os = sv.SelectOrderStatusForList();
                        Session["OrderStatuses"] = os;
                    }
                }

                return os;
            }
            set { Session["OrderStatuses"] = value; }
        }

        public User CurrentUser
        {
            get { return Session["CurrentUser"] as User; }
            set { Session["CurrentUser"] = value; }
        }
        public Customer CurrentCustomer
        {
            get { return Session["CurrentCustomer"] as Customer; }
            set { Session["CurrentCustomer"] = value; }
        }
        public bool HasSession
        {
            get
            {
                bool has = CurrentUser != null;
                if (!has)
                {
                    Response.Redirect("Logon.aspx?ecode=E0001&url=" + RedirectUrl, false);
                }

                return has;
            }
        }
        public string RedirectUrl
        {
            get
            {
                var vs = ViewState["RedirectUrl"] as string;

                if (string.IsNullOrEmpty(vs))
                {
                    vs = System.IO.Path.GetFileName(Request.PhysicalPath);
                    ViewState["RedirectUrl"] = vs;
                }
                return vs;
            }
            set { ViewState["RedirectUrl"] = value; }
        }

        #endregion

        protected void OpenNewWindow(string url)
        {
            JScript(string.Format("window.open('{0}');", url));
        }

        protected void JScript(string code)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "js", "javascript: " + code, true);
        }

        public string DictionaryValue(object obj)
        {
            string sCode = (string)obj;
            var text = OrderStatuses.Where(o => o.OrderStatusCode == sCode).FirstOrDefault();
            return text.OrderStatusText;
        }

        public string FormatString(object oText, object oL)
        {
            if (oText.GetType().Name == "DBNull")
                return string.Empty;

            string text = (string)oText;
            int l = (int)oL;

            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            else if (text.Length > l)
            {
                return text.Substring(0, l - 1) + "..";
            }
            return text;
        }

        public static System.Data.DataTable ConvertToDataTable<T>(IList<T> data)
        {
            System.ComponentModel.PropertyDescriptorCollection properties =
               System.ComponentModel.TypeDescriptor.GetProperties(typeof(T));
            System.Data.DataTable table = new System.Data.DataTable();
            foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                System.Data.DataRow row = table.NewRow();
                foreach (System.ComponentModel.PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public void ShowMessageBase(string text)
        {
            var pnlMsg = this.Master.FindControl("pnlMsg") as Panel;
            if (pnlMsg != null)
            {
                var txtMsg = pnlMsg.FindControl("txtMsg") as TextBox;
                if (txtMsg != null)
                    txtMsg.Text = text;
            }

            var mpeMsg = this.Master.FindControl("mpeMsg") as ModalPopupExtender;
            if (mpeMsg != null)
                mpeMsg.Show();
        }

        public DateTime? ConvertEmptyToNullDateTime(string str)
        {
            if (string.IsNullOrEmpty(str))
                return null;
            else if (str == "__/__/____")
                return null;
            else
            {
                var x = DateTime.ParseExact(str, DataFormat.Date, null);
                return x;
            }
        }

        public void GetLengthDateTime(DateTime? inFrom, DateTime? inTo, out DateTime? outFrom, out DateTime? outTo)
        {
            outFrom = null;
            outTo = null;

            if (inFrom != null && inTo != null)
            {
                outFrom = inFrom;
                outTo = inTo;
            }
            else if (inFrom != null && inTo == null)
            {
                outFrom = inFrom;
                outTo = inFrom;
            }
            else if (inFrom == null && inTo != null)
            {
                outFrom = inTo;
                outTo = inTo;
            }
        }

        protected void DownloadFile(string fileName, byte[] data)
        {
            //Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;  filename=" + fileName);
            Response.BinaryWrite(data);
        }

        protected void SetSortDirection(string currentSortDirection)
        {
            if (currentSortDirection == "ASC")
            {
                NewSortDirection = "DESC";
                SortImage.ImageUrl = "~/Images/view_sort_ascending.png";
            }
            else
            {
                NewSortDirection = "ASC";
                SortImage.ImageUrl = "~/Images/view_sort_descending.png";
            }
        }

        public string CurrentSortDirection
        {
            get { return ViewState["CurrentSortDirection"] as string; }
            set { ViewState["CurrentSortDirection"] = value; }
        }
        public string NewSortDirection
        {
            get { return ViewState["NewSortDirection"] as string; }
            set { ViewState["NewSortDirection"] = value; }
        }
        public Image SortImage
        {
            get { return Session["SortImage"] as Image; }
            set { Session["SortImage"] = value; }
        }
        public string SortExpression
        {
            get { return ViewState["SortExpression"] as string; }
            set { ViewState["SortExpression"] = value; }
        }

        protected void GridSortData(string currentSortDirection, string sortExpression, DataTable dt, GridView grd)
        {
            if (currentSortDirection == "ASC")
            {
                NewSortDirection = "DESC";
                SortImage.ImageUrl = "~/Images/view_sort_ascending.png";
            }
            else
            {
                NewSortDirection = "ASC";
                SortImage.ImageUrl = "~/Images/view_sort_descending.png";
            }

            if (dt != null)
            {
                //Sort the data.
                SortExpression = sortExpression;

                dt.DefaultView.Sort = sortExpression + " " + NewSortDirection;
                grd.DataSource = dt;
                grd.DataBind();
                CurrentSortDirection = NewSortDirection;
            }

            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in grd.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == sortExpression)
                {
                    columnIndex = grd.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }
            grd.HeaderRow.Cells[columnIndex].Controls.Add(SortImage);
        }

        protected void SetTextBoxSelectAllOnFocus(TextBox textbox)
        {
            textbox.Attributes["onfocus"] = "javascript:this.select();";
        }
    }
}