﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hmc.Smo.Web.DBService;
using Hmc.Smo.Common;
using System.Data;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using AjaxControlToolkit;

namespace Hmc.Smo.Web
{
    public partial class ShipmentDetailList : BasePage
    {
        #region property

        public SearchShipmentCriteria Search
        {
            get { return Session["SearchShipmentCriteria"] as SearchShipmentCriteria; }
            set { Session["SearchShipmentCriteria"] = value; }
        }
        public DataTable ShipmentDetailDT
        {
            get { return Session["ShipmentDetailDT"] as DataTable; }
            set { Session["ShipmentDetailDT"] = value; }
        }


        public List<ReportShipmentDetailForList> ShipmentDetails
        {
            get { return Session["ShipmentDetails"] as List<ReportShipmentDetailForList>; }
            set { Session["ShipmentDetails"] = value; }
        }

        public List<Customer> SoldTos
        {
            get { return Session["SoldTos"] as List<Customer>; }
            set { Session["SoldTos"] = value; }
        }
        public List<DropDownValue> InvoiceTypeList
        {
            get { return Session["InvoiceTypeList"] as List<DropDownValue>; }
            set { Session["InvoiceTypeList"] = value; }
        }
        public List<DropDownValue> ShipTos
        {
            get { return Session["ShipTos"] as List<DropDownValue>; }
            set { Session["ShipTos"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                SetTextBoxSelectAllOnFocus(txtQuotNo);

                IsShowCriteria = true;
                SortImage = new Image();
                SortImage.CssClass = "img-sort";

                using (var sv = new ServiceClient())
                {
                    if (IsUser || IsManager)
                    {
                        SoldTos = sv.SelectCustomersForList(CurrentCustomer.CustomerNo);
                    }
                    else
                    {
                        SoldTos = sv.SelectCustomersForList(null);
                    }

                    SoldTos.ForEach(o => o.CustomerName = string.Format("{0} : {1}", o.CustomerNo, o.CustomerName));

                    if (IsHMC)
                    {
                        SoldTos.Insert(0, new Customer() { CustomerNo = "", CustomerName = "-----Select Sold To-----" });
                    }
               //     ShipTos.Insert(0, new DropDownValue() { Code = "", Name = "All" });
                    ddlSoldTo.DataSource = SoldTos;
                    ddlSoldTo.DataBind();
                    InvoiceTypeList = new List<DropDownValue>();
                    InvoiceTypeList.Insert(0, new DropDownValue() { Code = "", Name = "-----All-----" });
                    InvoiceTypeList.Insert(1, new DropDownValue() { Code = "F2", Name = "F2:Invoice" });
                    InvoiceTypeList.Insert(2, new DropDownValue() { Code = "RE", Name = "RE:Return Credit" });
                   // InvoiceTypeList.Insert(3, new DropDownValue() { Code = "FD", Name = "FD:Replacement" });
                    InvoiceTypeList.Insert(3, new DropDownValue() { Code = "G2", Name = "G2:Credit" });
                    InvoiceTypeList.Insert(4, new DropDownValue() { Code = "L2", Name = "L2:Debit" });
                    InvoiceTypeList.Insert(5, new DropDownValue() { Code = "CN", Name = "CN:All CN Type" });
                    ddlInvoiceType.DataSource = InvoiceTypeList;
                    ddlInvoiceType.DataBind();
                    
                }

                LoadSearchCriteria();
                if (Request["InvoiceType"] != null)
                {
                    if (Request["InvoiceType"].Equals("F2"))
                    {
                        ddlInvoiceType.SelectedIndex = 1;
                    }
                    else if (Request["InvoiceType"].Equals("RE"))
                    {
                        ddlInvoiceType.SelectedIndex = 2;
                    }
                    else if (Request["InvoiceType"].Equals("FD"))
                    {
                        ddlInvoiceType.SelectedIndex = 3;
                    }
                    else if (Request["InvoiceType"].Equals("G2"))
                    {
                        ddlInvoiceType.SelectedIndex = 3;
                    }
                    else if (Request["InvoiceType"].Equals("L2"))
                    {
                        ddlInvoiceType.SelectedIndex = 4;
                    }
                    else if (Request["InvoiceType"].Equals("CN"))
                    {
                        ddlInvoiceType.SelectedIndex = 5;
                    }
                }

                if (string.IsNullOrEmpty(Search.SoldTo))
                {
                    ShipmentDetails = new List<ReportShipmentDetailForList>();
                  
                            LoadReportShipmentDetails();
                    grd.DataSource = ShipmentDetailDT;
                    grd.DataBind();
                }
                else
                {
                    LoadReportShipmentDetails();
                }

                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadSearchCriteria()
        {
            if (Search == null)
            {
                Search = new SearchShipmentCriteria();

                if (IsUser || IsManager)
                {
                    Search.SoldTo = CurrentCustomer.CustomerNo;
                    if (Request["OrderNo"] != null)
                    {
                        Search.DocNo = Request["OrderNo"].ToString();
                        txtDocNo.Text = Request["OrderNo"].ToString();
                    }
                    else
                    {
                       // txtDocNo.Text = Search.DocNo;
                    }
                }
                else
                {
                    if (Request["OrderNo"] != null)
                    {
                        Search.DocNo = Request["OrderNo"].ToString();
                        txtDocNo.Text = Request["OrderNo"].ToString();
                    }
                    else
                    {
                        txtDocNo.Text = Search.DocNo;
                    }
                }
            }
            else
            {
                txtQuotNo.Text = Search.QuotationNo;
                txtDlvDate.Text = FormatHelper.DateToStr(Search.DeliveryDate);
                ddlSoldTo.SelectedIndex = ddlSoldTo.Items.IndexOf(ddlSoldTo.Items.FindByValue(Search.SoldTo));
                ddlShipTo.SelectedIndex = ddlShipTo.Items.IndexOf(ddlShipTo.Items.FindByValue(Search.SoldTo));
                txtProductName.Text = Search.ProductName;
                if (Request["OrderNo"] != null)
                {
                    Search = new SearchShipmentCriteria();
                    Search.DocNo = Request["OrderNo"].ToString();
                    txtDocNo.Text = Request["OrderNo"].ToString();
                }
                else
                {
                    txtDocNo.Text = Search.DocNo;
                }
                txtDocNo.Text = Search.DocNo;
                txtShipmentNo.Text = Search.ShipmentNo;
                txtSaleOrder.Text = Search.SaleOrder;
                txtDeliveryNo.Text = Search.DeliveryNo;
                txtInvoiceNo.Text = Search.InvoiceNo;

                txtPoNo.Text = Search.CustRef;
                ddlInvoiceType.SelectedIndex = ddlInvoiceType.Items.IndexOf(ddlInvoiceType.Items.FindByValue(Search.InvoiceType));
            }
        }

        private void SaveSearchCriteria()
        {
            Search = new SearchShipmentCriteria();
            Search.QuotationNo = txtQuotNo.Text.Trim();

            var str = txtDlvDate.Text.Trim();
            if (!string.IsNullOrEmpty(str))
                Search.DeliveryDate = FormatHelper.StrToDate(str);

            Search.SoldTo = string.IsNullOrEmpty(ddlSoldTo.SelectedValue) ? null : ddlSoldTo.SelectedValue;
            if (string.IsNullOrEmpty(Search.SoldTo))
                Search.ShipTo = null;
            else
                Search.ShipTo = string.IsNullOrEmpty(ddlShipTo.SelectedValue) ? null : ddlShipTo.SelectedValue;
            Search.ProductName = txtProductName.Text.Trim();

            Search.DocNo = txtDocNo.Text.Trim();
            Search.ShipmentNo = txtShipmentNo.Text.Trim();
            Search.SaleOrder = txtSaleOrder.Text.Trim();
            Search.DeliveryNo = txtDeliveryNo.Text.Trim();
            Search.InvoiceNo = txtInvoiceNo.Text.Trim();
            Search.CustRef = txtPoNo.Text.Trim();
            Search.InvoiceType = string.IsNullOrEmpty(ddlInvoiceType.SelectedValue) ? null : ddlInvoiceType.SelectedValue;
            // = string.IsNullOrEmpty(ddlSoldTo.SelectedValue) ? null : ddlSoldTo.SelectedValue;
        }

        private void LoadReportShipmentDetails(bool bind = true)
        {
            using (var sv = new ServiceClient())
            {
                ShipmentDetails = sv.SelectReportShipmentDetailsForList(Search);

                ShipmentDetailDT = ConvertToDataTable<ReportShipmentDetailForList>(ShipmentDetails);
                ShipmentDetailDT.DefaultView.Sort = "QuotationNo desc, ShipmentNo desc, DeliveryOrder desc";
                if (bind)
                {
                    grd.DataSource = ShipmentDetailDT;
                    grd.DataBind();
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadReportShipmentDetails();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }
        //private async Task<string> HttpPost(string relativeUri, string json)
        //{

        //    var cts = new CancellationTokenSource();
        //    //var jsonResponse = "";
        //    //cts.CancelAfter(5000);
        //    LoginParam login = new LoginParam();
        //    login.username = "smartorder@hmcpolymers.com";
        //    login.password = "Redrotrams";
        //    login.deviceID = "-";
        //    try
        //    {
        //        HttpClient client = new HttpClient();

        //        Uri uri = new Uri($"https://digitaldo.hmcpolymers.com/api/authentication/login");
        //        var httpContent = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
        //        Task<HttpResponseMessage> response = client.PostAsync(uri, httpContent);//.AsTask(cts.Token);
        //       // HttpResponseMessage response = await client.PostAsync(uri, httpContent).AsTask(cts.Token);

        //       // dynamic response = await client.PostAsync(uri, httpContent);

        //        if (!response.IsCompleted)
        //        {
        //            return string.Empty;
        //        }

        //       // string jsonResponse = await response

        //        System.Diagnostics.Debug.WriteLine("");

        //        return "";
        //    }
        //    catch (Exception)
        //    {
        //        return string.Empty;
        //    }
        //}
        //static async Task<HttpResponseMessage> GetURI(Uri u)
        //{
        //    var response = string.Empty;
        //    LoginParam login = new LoginParam();
        //    login.username = "smartorder@hmcpolymers.com";
        //    login.password = "Redrotrams";
        //    login.deviceID = "-";
        //    using (var client = new HttpClient())
        //    {
        //        HttpResponseMessage result = await client.PostAsync(string.Format("{0}authentication/login", "https://digitaldo.hmcpolymers.com/api/"), new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json"));
        //        if (result.IsSuccessStatusCode)
        //        {
        //            response = await result.Content.ReadAsStringAsync();
        //        }
        //    }
        //    return response;
        //}
         async Task<string> PostURI(Uri u, HttpContent c,string ordernumber)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                
                HttpClient cc = new HttpClient();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                var response = string.Empty;
                LoginParam login = new LoginParam();
                login.username = "smartorder@hmcpolymers.com";
                login.password = "Hmc@2019";
                login.deviceID = "-";
                string api = "";
                //  HttpResponseMessage result = await cc.PostAsync(u, c);
                var to = cc.PostAsync(string.Format("{0}authentication/login", "https://digitaldo.hmcpolymers.com/api/"), new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;
                var resultxx = JsonConvert.DeserializeObject<LoginResult>(to);
                //var to = c.PostAsync(string.Format("{0}authentication/login", this.txtAPI.Text), new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;
                //var result = JsonConvert.DeserializeObject<LoginResult>(to);
                //if (result.success)
                //{
                //    this.lblToken.Text = result.access_token;
                //    this.lblStatus.Text = "Connected";
                //    this.btnLogin.Text = "Disconnect";
                //    this.btnGetPDF.Enabled = true;
                //}
               // var x2x = JsonConvert.DeserializeObject<LoginResult>(result.ToJSON());
                if (resultxx.success)
                {
                    api = resultxx.access_token;
                    sb.Append("x2x.access_token====" + resultxx.access_token + "<br>");
                  
                }
                else
                {
                    api = "";
                    sb.Append("x2x.error===="+ resultxx.error + "<br>");
                    
                }


                if (resultxx.success)
                {
                 //   response = result.StatusCode.ToString();
                 //   sb.Append(response + "<br>");
                 //   var xx = result.ToJSON();
                //    sb.Append(xx  +"<br>");
                   // Label1.Text ="yyyy>>> "+ response +" xxxx>>"+ xx;
                    try
                    {
                      //  sb.Append("DefaultRequestHeaders" + "<br>");
                        cc.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", api);
                      //  sb.Append("cc2.DefaultRequestHeaders.Authorization" + "<br>");
                       
                        sb.Append(string.Format("{0}do/getid/{1}", "https://digitaldo.hmcpolymers.com/api/", ordernumber) + "<br>");
                        var result11 = await cc.GetAsync(string.Format("{0}do/getid/{1}", "https://digitaldo.hmcpolymers.com/api/", ordernumber));
                        sb.Append("HttpResponseMessage" + "<br>");
                        var ccon = result11.Content.ReadAsStringAsync().Result;
                        sb.Append("DO ID>>>>>>>"+ ccon + "<<<<<<<\n" + "<br>");
                        if (result11.IsSuccessStatusCode)                       {

                            //sb.Append(string.Format("{0}do/transport-order/{1}?doc=DO,COA&view=DOWNLOAD", "https://digitaldo.hmcpolymers.com/api/", "response"));
                            var filecon =  cc.GetAsync(string.Format("{0}do/transport-order/{1}?doc=DO,COA&view=DOWNLOAD", "https://digitaldo.hmcpolymers.com/api/", ccon));
                            sb.Append("11<br>");
                            var bytes = filecon.Result.Content.ReadAsByteArrayAsync().Result;
                            sb.Append("112<br>");
                            if (filecon.Result.IsSuccessStatusCode)
                            {
                                sb.Append("113<br>");
                                if (bytes.Length > 0)
                                {
                                    sb.Append("114<br>");
                                    File.WriteAllBytes(Server.MapPath("~/temp/") + ordernumber+".pdf", bytes);

                                    string url = "DownloadFile.ashx?file=" + ordernumber + ".pdf";
                                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "downloadFileExport", "javascript:window.location ='" + ResolveUrl(url) + "';", true);
                                }
                                else

                                {
                                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "alert('Delivery Order Does not exsit!');", true);
                                 //   ShowMessgeBox("Delivery Does not exsit!");
                                 //   sb.Append("115<br>");
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "alert('Delivery Order Does not exsit!');", true);

                                //     sb.Append("115<br>");
                                // MessageBox.Show(filecon.Result.ReasonPhrase);
                            }
                        }
                     //   Label1.Text = sb.ToString();
                        sb.Append("!!!!!end!!!!!");
                      //  Label1.Text = sb.ToString();
                        return "Delivery Does not exsit!";
                    }
                    catch (Exception e)
                    {
                        sb.Append("error" + e.Message);
                       // Label1.Text = e.Message;
                   //     Label1.Text = sb.ToString();
                        return "";
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "alert('Delivery Order Does not exsit!');", true);

                }
                // }
                return response;
            }catch(Exception e)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "popup", "alert('Delivery Order Does not exsit!');", true);

                sb.Append("error2" + e.Message);
              //  Label1.Text = sb.ToString();
                return "";
            }
        }
        public void ShowMessgeBox(string text)
        {
            var pnlMsg = this.FindControl("pnlMsg") as Panel;
            if (pnlMsg != null)
            {
                var txtMsg = pnlMsg.FindControl("txtMsg") as TextBox;
                if (txtMsg != null)
                    txtMsg.Text = text;
            }

            var mpeMsg = this.FindControl("mpeMsg") as ModalPopupExtender;
            if (mpeMsg != null)
                mpeMsg.Show();
        }

        protected void btnDO_Click(object sender, EventArgs e)
        {

            // print initial status
            Console.WriteLine("Runtime: " + System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(int).Assembly.Location).ProductVersion);
            Console.WriteLine("Enabled protocols:   " + ServicePointManager.SecurityProtocol);
            Console.WriteLine("Available protocols: ");
            Boolean platformSupportsTls12 = false;
            foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
            {
                Console.WriteLine(protocol.GetHashCode());
                if (protocol.GetHashCode() == 3072)
                {
                    platformSupportsTls12 = true;
                }
            }
            Console.WriteLine("Is Tls12 enabled: " + ServicePointManager.SecurityProtocol.HasFlag((SecurityProtocolType)3072));


            // enable Tls12, if possible
            if (!ServicePointManager.SecurityProtocol.HasFlag((SecurityProtocolType)3072))
            {
                if (platformSupportsTls12)
                {
                    Console.WriteLine("Platform supports Tls12, but it is not enabled. Enabling it now.");
                    ServicePointManager.SecurityProtocol |= (SecurityProtocolType)3072;
                }
                else
                {
                    Console.WriteLine("Platform does not supports Tls12.");
                }
            }

            // disable ssl3
            if (ServicePointManager.SecurityProtocol.HasFlag(SecurityProtocolType.Ssl3))
            {
                Console.WriteLine("Ssl3SSL3 is enabled. Disabling it now.");
                // disable SSL3. Has no negative impact if SSL3 is already disabled. The enclosing "if" if just for illustration.
                System.Net.ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;
            }
            Console.WriteLine("Enabled protocols:   " + ServicePointManager.SecurityProtocol);


            
           // HttpPost("", "");
            //    this.btnGetPDF.Text = "Loading...";
            //   this.btnGetPDF.Enabled = false;

            //    HttpClient c = new HttpClient();


            LoginParam login = new LoginParam();
            login.username = "smartorder@hmcpolymers.com";
            login.password = "Hmc@2019";
            login.deviceID = "-";
            Uri u = new Uri("https://digitaldo.hmcpolymers.com/api/authentication/login");
            HttpContent hcc = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
            //var t = Task.Run(() => PostURI(u, hcc,e.));
            //t.Wait();

         
        }


        protected void grd_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                GridSortData(CurrentSortDirection, e.SortExpression, ShipmentDetailDT, grd);
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grd.PageIndex = e.NewPageIndex;
                grd.DataSource = ShipmentDetailDT;
                grd.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void ddlSoldTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadReportShipmentDetails();

                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }
        protected void ddlInvType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LoadShipTos()
        {
            if (string.IsNullOrEmpty(Search.SoldTo))
            {
                ShipTos = new List<DropDownValue>();
            }
            else
            {
                //ShipTos = ShipmentDetails.GroupBy(o => o.ShipTo, o => o.ShipToName, (id, text) => new DropDownValue() { Code = id, Name = text.First() }).ToList();
                ShipTos = ShipmentDetails.GroupBy(o => o.ShipTo, o => o.ShipToName, (id, text) => new DropDownValue() { Code = id, Name = string.Format("{0} : {1}", id, text.First()) }).ToList();
                ShipTos.Insert(0, new DropDownValue() { Code = "", Name = "All" });
            }
            ddlShipTo.DataSource = ShipTos;
            ddlShipTo.DataBind();
        }

        protected void grd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "INV_DOWNLOAD")
            {
                using (var sv = new ServiceClient())
                {
                    var mailPDF = sv.SelectMailPDFByPK((string)e.CommandArgument);
                    var fileName = mailPDF.SaleConfirmNo + ".pdf";
                    var fullPath = Server.MapPath("Temp/" + fileName);

                    File.WriteAllBytes(fullPath, mailPDF.PDFContent);

                    Thread.Sleep(2000);

                    string url = "DownloadFile.ashx?file=" + fileName;
                    //ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "downloadFileExport", "javascript:window.location ='" + ResolveUrl(url) + "';", true);
                    Response.Redirect(url);
                }
            }
            else if (e.CommandName == "ORDER_DOWNLOAD") 
            {
                // print initial status
                //Console.WriteLine("Runtime: " + System.Diagnostics.FileVersionInfo.GetVersionInfo(typeof(int).Assembly.Location).ProductVersion);
                //Console.WriteLine("Enabled protocols:   " + ServicePointManager.SecurityProtocol);
                //Console.WriteLine("Available protocols: ");
                Boolean platformSupportsTls12 = false;
                foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
                {
                   // Console.WriteLine(protocol.GetHashCode());
                    if (protocol.GetHashCode() == 3072)
                    {
                        platformSupportsTls12 = true;
                    }
                }
              //  Console.WriteLine("Is Tls12 enabled: " + ServicePointManager.SecurityProtocol.HasFlag((SecurityProtocolType)3072));


                // enable Tls12, if possible
                if (!ServicePointManager.SecurityProtocol.HasFlag((SecurityProtocolType)3072))
                {
                    if (platformSupportsTls12)
                    {
                    //    Console.WriteLine("Platform supports Tls12, but it is not enabled. Enabling it now.");
                        ServicePointManager.SecurityProtocol |= (SecurityProtocolType)3072;
                    }
                    else
                    {
                    //    Console.WriteLine("Platform does not supports Tls12.");
                    }
                }

                // disable ssl3
                if (ServicePointManager.SecurityProtocol.HasFlag(SecurityProtocolType.Ssl3))
                {
                  //  Console.WriteLine("Ssl3SSL3 is enabled. Disabling it now.");
                    // disable SSL3. Has no negative impact if SSL3 is already disabled. The enclosing "if" if just for illustration.
                    System.Net.ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;
                }
             //   Console.WriteLine("Enabled protocols:   " + ServicePointManager.SecurityProtocol);
                LoginParam login = new LoginParam();
                login.username = "smartorder@hmcpolymers.com";
                login.password = "Hmc@2019";
                login.deviceID = "-";
                Uri u = new Uri("https://digitaldo.hmcpolymers.com/api/authentication/login");
                HttpContent hcc = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
                var t = Task.Run(() => PostURI(u, hcc,e.CommandArgument.ToString()));
                t.Wait();
            }
        }

        protected void ddlInvType1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlInvoiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadReportShipmentDetails();

                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }
    }
    public class InvoiceType
    {
        public string InvoiceTypeName { get; set; }
        public string InvoiceTypeCode { get; set; }
        public InvoiceType()
        {

        }

    }
    public class LoginParam
    {
        public string username { get; set; }
        public string password { get; set; }
        public string deviceID { get; set; }
    }
    public class LoginResult
    {
        public string error { get; set; }
        public bool success { get; set; }
        public string access_token { get; set; }
    }
}