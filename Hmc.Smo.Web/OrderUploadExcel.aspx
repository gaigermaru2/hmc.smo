﻿<%@ Page Title="Upload Orders : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="OrderUploadExcel.aspx.cs" Inherits="Hmc.Smo.Web.OrderUploadExcel"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ContentPlaceHolderID="cphPageName" runat="server">
    Orders > Upload from Excel
</asp:Content>
<asp:Content ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .header-text {
            padding-top: 40px;
        }

        .row {
            padding-bottom: 10px;
        }

        .div-content-height {
            min-height: 600px;
        }


        input[type=file].fileupload {
            display: inline-block;
            background-color: #eee;
            border: 1px solid gray;
            font-size: 15px;
            padding: 2px;
            width: 60%;
        }

        ::-webkit-file-upload-button {
            -webkit-appearance: none;
            background-color: steelblue;
            border: 1px solid gray;
            font-size: 15px;
            padding: 8px;
            color: white
        }

        ::-ms-browse {
            background-color: steelblue;
            border: 1px solid gray;
            font-size: 15px;
            padding: 8px;
            color: white
        }

        input[type=file]::-ms-value {
            border: none;
        }
    </style>
    <link href="Styles/step.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 8px">
        <asp:Button ID="btnDownloadTemplate" Text="Download Template" runat="server"
            CssClass="button-img-blue img-download-gray" Visible='<%# CanDownloadTemplate %>' OnClick="btnDownloadTemplate_Click" />
    </div>
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="w3-panel w3-white w3-card-4 w3-section" style="margin-top: 0px; min-height: 450px">
        <div class="container">
            <asp:Panel runat="server" Visible='<%# Step.Value == 1 %>'>
                <ul class="step-header">
                    <li class="active">Select Excel</li>
                    <li>Validate file</li>
                    <li>Order Created as DRAFT</li>
                    <li>Submit order to Approval</li>
                </ul>
                <asp:Panel runat="server" Visible='<%# CanValidateOrder %>'>
                    <div class="header-text">
                        Select file excel and click VALIDATE button
                    </div>
                    <div class="row">
                        <asp:FileUpload runat="server" ID="ful" CssClass="fileupload" />
                        <asp:RequiredFieldValidator ErrorMessage="*" ControlToValidate="ful" runat="server" ValidationGroup="validate" />
                        <br />
                        <asp:Label ID="lblErrMsg" ForeColor="Red" runat="server" />
                    </div>
                    <div class="row">
                        <asp:Button ID="btnValidate" Text="Validate" runat="server" CssClass="w3-button w3-blue-gray"
                            OnClick="OnValidateExcel" ValidationGroup="validate" />
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" Visible='<%# Step.Value == 2 %>'>
                <ul class="step-header">
                    <li>Select Excel</li>
                    <li class="active">Validate file</li>
                    <li>Order Created as DRAFT</li>
                    <li>Submit order to Approval</li>
                </ul>

                <asp:Panel runat="server" Visible='<%# CanCreateOrders || CanShowValidateItem %>'>
                    <div class="header-text">
                        Take a look at the error column on each row and click CREATE ORDERS button for create order to system
                    </div>
                    <div class="row">
                        <asp:GridView runat="server" ID="grdOrder2" DataSource='<%# ValidateItems %>'
                            AutoGenerateColumns="false" CssClass="grid"
                            GridLines="None" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="item" AlternatingRowStyle-CssClass="item-alt" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="Line" HeaderText="Line" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PONo" HeaderText="Po no." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="PODate" HeaderText="Po date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PaymentTermText" HeaderText="Payment Term" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ProductText" HeaderText="Product" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="PackageSize" HeaderText="Package Size" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="Quantity" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="DeliveryDate" HeaderText="Delivery date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ErrorMessage" HeaderText="Error message" ItemStyle-ForeColor="Red" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="row">
                        <asp:Button Text="Select other file" OnClick="OnUploadNewFile" runat="server" CssClass="w3-button w3-gray" />
                        <asp:Button Text="Create Orders" OnClick="OnCreateOrder" runat="server" CssClass="w3-button w3-blue-gray" Enabled='<%# CanCreateOrders %>' />
                    </div>
                </asp:Panel>

            </asp:Panel>
            <asp:Panel runat="server" Visible='<%# Step.Value == 3 %>'>
                <ul class="step-header">
                    <li>Select Excel</li>
                    <li>Validate file</li>
                    <li class="active">Order Created as DRAFT</li>
                    <li>Submit order to Approval</li>
                </ul>
                <asp:Panel runat="server" Visible='<%# CanShowOrder %>'>
                    <div class="header-text">
                        Order has been created as 'DRAFT'
                    </div>
                    <div class="row">
                        <asp:GridView runat="server" ID="grdOrder3" DataSource='<%# Orders %>'
                            AutoGenerateColumns="false" CssClass="grid"
                            GridLines="None" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="item" AlternatingRowStyle-CssClass="item-alt" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="OrderNo" HeaderText="Order no." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PONo" HeaderText="Po no." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="PODate" HeaderText="Po date" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PaymentTermText" HeaderText="Payment Term" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ShipToText" HeaderText="Ship-to" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="TotalQty" HeaderText="Quantity" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TotalTaxAmount" HeaderText="Tax" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TotalAmount" HeaderText="Total amount" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="OrderStatusText" HeaderText="Status" ItemStyle-ForeColor="DarkBlue" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="row">
                        <asp:Button Text="Upload new file" OnClick="OnUploadNewFile" runat="server" CssClass="w3-button w3-white" />
                        <asp:Button Text="Submit to Approval" OnClick="OnSubmit" runat="server" CssClass="w3-button w3-blue-gray" />
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" Visible='<%# Step.Value == 4 %>'>
                <ul class="step-header">
                    <li>Select Excel</li>
                    <li>Validate file</li>
                    <li>Order Created as DRAFT</li>
                    <li class="active">Submit order to Approval</li>
                </ul>
                <asp:Panel runat="server" Visible='<%# CanShowOrder %>'>
                    <div class="header-text">
                        All orders have been submitted to Approval/HMC.
                    </div>
                    <div class="row">
                        <asp:GridView ID="grdOrder4" runat="server" DataSource='<%# Orders %>'
                            AutoGenerateColumns="false" CssClass="grid"
                            GridLines="None" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="item" AlternatingRowStyle-CssClass="item-alt" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="OrderNo" HeaderText="Order no." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PONo" HeaderText="Po no." HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="PODate" HeaderText="Po date" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="PaymentTermText" HeaderText="Payment term" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="ShipToText" HeaderText="Ship-to" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="TotalQty" HeaderText="Quantity" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Amount" HeaderText="Amount" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TotalTaxAmount" HeaderText="Tax" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="TotalAmount" HeaderText="Total amount" DataFormatString="{0:#,##0}" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="OrderStatusText" HeaderText="Status" ItemStyle-ForeColor="DarkBlue" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="row">
                        <asp:Button Text="Upload new file" OnClick="OnUploadNewFile" runat="server" CssClass="w3-button w3-white" />
                    </div>
                </asp:Panel>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
