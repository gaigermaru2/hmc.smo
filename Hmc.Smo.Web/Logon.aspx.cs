﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hmc.Smo.Web.DBService;
using Hmc.Smo.Common;

namespace Hmc.Smo.Web
{
    public partial class Logon : Page
    {
        #region property

        public User User
        {
            get { return Session["User"] as User; }
            set { Session["User"] = value; }
        }

        public User CurrentUser
        {
            get { return Session["CurrentUser"] as User; }
            set { Session["CurrentUser"] = value; }
        }
        public Customer CurrentCustomer
        {
            get { return Session["CurrentCustomer"] as Customer; }
            set { Session["CurrentCustomer"] = value; }
        }

        public WebConfig Config
        {
            get
            {
                var config = Session["Config"] as WebConfig;
                if (config == null)
                {
                    using (var sv = new ServiceClient())
                    {
                        config = sv.GetWebConfig();
                        Session["Config"] = config;
                    }
                }
                return config;
            }
        }

        #endregion
        public string anno = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "fnOnUpdateValidators();");
            if (IsPostBack) return;

            try
            {
                string host = Request.Url.Host.ToLower();
                using (var sv = new ServiceClient())
                {
                    var a = sv.SelectAnnounceForList(0);
                    anno = a[0].announce_text;

                }
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();

                if (Config.WEB_APP_TYPE == "DEV")
                {
                    txtPassword.TextMode = TextBoxMode.SingleLine;
                    txtPassword.Text = "ttt";
                }

                var usernameCookie = Request.Cookies["UsernameCookie"];

                if (usernameCookie != null)
                {
                    txtUsername.Text = usernameCookie.Value;
                    txtPassword.Focus();
                }
                else
                {
                    txtUsername.Focus();
                }

                //check error code for show message
                string ecode = Request.QueryString["ecode"];
                if (!string.IsNullOrEmpty(ecode))
                {
                    using (var sv = new ServiceClient())
                    {
                        
                        var msg = sv.SelectErrorMsgByPK(ecode);
                        ShowMessage(msg);
                    }
                }

                //DateTime expired = new DateTime(2016, 10, 18, 12, 20, 0);
                //var expired = new DateTime(2016, 10, 23, 22, 0, 0);
                //pnlMsgAdvertise.Visible = DateTime.Now <= expired;

                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            try
            {
                using (var sv = new ServiceClient())
                {

                    if (Config.WEB_APP_TYPE == "DEV")
                    {
                        User = sv.SelectUserByPK(null, txtUsername.Text.ToUpper());
                    }
                    else
                    {
                        string passEn = PasswordHelper.EncodePasswordToBase64(txtPassword.Text);
                        User = sv.UserAuthentication(txtUsername.Text.ToUpper(), passEn);
                    }
                }

                if (User != null)
                {
                    HttpCookie usernameCookie = new HttpCookie("UsernameCookie");
                    usernameCookie.Value = txtUsername.Text;
                    usernameCookie.Expires = DateTime.Now.AddDays(7);
                    Response.Cookies.Add(usernameCookie);

                    if (User.IsDeleted != null && !User.IsDeleted.Value)
                    {
                        if (DateTime.Today >= User.ValidFrom.Date && DateTime.Today <= User.ValidTo.Date)
                        {
                            if (User.IsChangePass)
                            {
                                Response.Redirect("ResetPassword.aspx", false);
                                //  txtNewPass.Focus();
                                //   mpeChangePass.Show();
                            }
                            else
                            {
                                SetCurrentUserAndCustomerAndRedirect();
                            }
                        }
                        else
                            ShowMessage(Msg.UserIsExpired);
                    }
                    else
                    {
                        ShowMessage(Msg.UserIsLocked);
                    }
                }
                else
                {
                    ShowMessage(Msg.IncorrectUserPassword);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        protected void btnChangePass_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    using (var sv = new ServiceClient())
            //    {
            //        string encode = PasswordHelper.EncodePasswordToBase64(txtNewPass.Text);
            //        User.Password = encode;
            //        User.IsChangePass = false;
            //        sv.UpdateUser(User);
            //    }

            //    SetCurrentUserAndCustomerAndRedirect();
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage(ex.Message);
            //}
        }

        public void ShowMessage(string msg)
        {
         //   txtMsg.Text = msg;
            //mpeMsg.Show();
            //btnMsgCancel.Focus();
        }

        private void SetCurrentUserAndCustomerAndRedirect()
        {
            using (var sv = new ServiceClient())
            {
                CurrentUser = User;
                CurrentCustomer = sv.SelectCustomerByPK(User.CustomerNo, null, null, null);

                string url = Request.QueryString["url"];

                if (string.IsNullOrEmpty(url))
                    url = "Dashboard.aspx";

                if (CurrentUser.IsAdmin)
                { url = "orderlist.aspx"; }else 
                 //   url = "OrderTemplateList.aspx";
                 //    url = "Dashboard.aspx";
                if (CurrentUser.Role.Equals( "2"))
                { url = "orderlist.aspx"; }
                else    if ( CurrentUser.Role.Equals("3"))
                { url = "orderlist.aspx"; }
                     //   url = "orderlist.aspx";
                Response.Redirect(url, false);
            }
        }


    }
}
