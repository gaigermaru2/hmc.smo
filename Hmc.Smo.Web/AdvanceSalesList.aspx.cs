﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hmc.Smo.Web.DBService;
using Hmc.Smo.Common;
using System.Data;
using System.IO;
using System.Threading;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;

namespace Hmc.Smo.Web
{
    public partial class AdvanceSalesList : BasePage
    {
        #region property

        public SearchAdvanceSalesCriteria Search
        {
            get { return Session["SearchAdvanceSalesCriteria"] as SearchAdvanceSalesCriteria; }
            set { Session["SearchAdvanceSalesCriteria"] = value; }
        }
        //public AdvanceSalesList Search
        //{
        //    get { return Session["SearchShipmentCriteria"] as SearchShipmentCriteria; }
        //    set { Session["SearchShipmentCriteria"] = value; }
        //}
        public DataTable AdvanceSalesDT
        {
            get { return Session["AdvanceSalesDT"] as DataTable; }
            set { Session["AdvanceSalesDT"] = value; }
        }


        public List<AdvanSale> AdvanSales
        {
            get { return Session["AdvanSales"] as List<AdvanSale>; }
            set { Session["AdvanSales"] = value; }
        }

        public List<Customer> SoldTos
        {
            get { return Session["SoldTos"] as List<Customer>; }
            set { Session["SoldTos"] = value; }
        }
        public List<DropDownValue> ShipTos
        {
            get { return Session["ShipTos"] as List<DropDownValue>; }
            set { Session["ShipTos"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
               // SetTextBoxSelectAllOnFocus(txtQuotNo);

                IsShowCriteria = true;
                SortImage = new Image();
                SortImage.CssClass = "img-sort";

                using (var sv = new ServiceClient())
                {
                    if (IsUser || IsManager)
                    {
                        SoldTos = sv.SelectCustomersForList(CurrentCustomer.CustomerNo);
                    }
                    else
                    {
                        SoldTos = sv.SelectCustomersForList(null);
                    }

                    SoldTos.ForEach(o => o.CustomerName = string.Format("{0} : {1}", o.CustomerNo, o.CustomerName));

                    if (IsHMC)
                    {
                        SoldTos.Insert(0, new Customer() { CustomerNo = "", CustomerName = "-----Select Sold To-----" });
                    }
                    //List<Customer> array = new List<Customer>();
                    //foreach (Customer value in SoldTos)
                    //{
                    //    array.Add(value.CustomerName);
                    //  //  Console.WriteLine("FOREACH ITEM: " + value);
                    //}
                  //  for (int i = 0; i < 100; i++) array.Add(i.ToString());
                    ddlSoldTo.AddItems(SoldTos);

                    AdvanSale s = new AdvanSale();
                    
                   // s.SOLDTO_Name;
                    //ddlSoldTo.DataSource = SoldTos;
                    ddlSoldTo.DataBind();
                }

                LoadSearchCriteria();

                if (string.IsNullOrEmpty(Search.SoldTo))
                {
                    AdvanSales = new List<AdvanSale>();

                    grd.DataSource = AdvanceSalesDT;
                    grd.DataBind();
                }
                else
                {
                    LoadAdvanceSalesList();
                }

                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadSearchCriteria()
        {
            if (Search == null)
            {
                Search = new SearchAdvanceSalesCriteria();

                if (IsUser || IsManager)
                {
                    Search.SoldTo = CurrentCustomer.CustomerNo;
                }
            }
            else
            {
                txtDeliveryNo.Text = Search.DeliveryOrder;
             //   txtDeliveryNoTo.Text = Search.DeliveryOrderTo;   
               // ddlSoldTo.SelectedIndex = ddlSoldTo.Items.IndexOf(ddlSoldTo.Items.FindByValue(Search.SoldTo));
               // ddlShipTo.SelectedIndex = ddlShipTo.Items.IndexOf(ddlShipTo.Items.FindByValue(Search.SoldTo));
             //   txtShipmentNo.Text = Search.ProductName;

             //   txtShipmentNoTo.Text = Search.ShipmentNo;
             //   txtShipmentNo.Text = Search.ShipmentNoTo;
             //   invDateTo.Text = FormatHelper.DateToStr(Search.InvoiceDate); 
             //   invDateFrom.Text = FormatHelper.DateToStr(Search.InvoiceDateTo); 
                
              //  txtInvoiceNo.Text = Search.InvoiceNo;

              //  txtPoNo.Text = Search.CustRef;
            }
        }

        private void SaveSearchCriteria()
        {
            Search = new SearchAdvanceSalesCriteria();
            string soltoStr = soh.Value;
            string shitoStr = shh.Value;
            var solSplit = soltoStr.Split(',');
            for (int i = 0; i < solSplit.Length; i++) solSplit[i] = "" + solSplit[i] + "";
            var shiSplit = shitoStr.Split(',');
            for (int i = 0; i < shiSplit.Length; i++) shiSplit[i] = "" + shiSplit[i] + "";

            var soldToValue = string.Join(",", solSplit);
            Search.SoldTo = soldToValue;
            // Search.SoldTo = "900726";
            var shipToValue = string.Join(",", shiSplit);
            Search.ShipTo = shipToValue;
            Search.SaleConfirmation = txtSaleConfirmno.Text;
            Search.SaleOrder = txtSaleOrderno.Text;
            var str2 = invDateFrom.Text.Trim();
            if (!string.IsNullOrEmpty(str2))
                Search.InvoiceDate = FormatHelper.StrToDate(str2);



            Search.InvoiceNo = txtInvoiceNo.Text;
            Search.DeliveryOrder = txtDeliveryNo.Text;
            var str3 = txtDelivertDate.Text.Trim();
            if (!string.IsNullOrEmpty(str3))
                Search.DeliveryDate = FormatHelper.StrToDate(str3);
            Search.ShipmentNo = txtShipmentNo.Text;
            var str4 = txtDelivertDate.Text.Trim();
            if (!string.IsNullOrEmpty(str4))
                Search.ShipmentDate = FormatHelper.StrToDate(str4);
            Search.ProductGrade = txtProductGrade.Text;
            Search.PO = txtPO.Text;
            Search.DocumentNo = txtDocno.Text;

           


        }

        private void LoadAdvanceSalesList(bool bind = true)
        {
            using (var sv = new ServiceClient())
            {
                AdvanSales = sv.SearchAdvanSaleForList(Search);
                AdvanSale ss = new AdvanSale();
                // ss.Doc_no
                AdvanceSalesDT = ConvertToDataTable<AdvanSale>(AdvanSales);
               // AdvanceSalesDT.DefaultView.Sort = "InvoiceDate desc, ShiptmentNo desc, Delivery desc";
                if (bind)
                {
                    grd.DataSource = AdvanceSalesDT;
                    grd.DataBind();
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadAdvanceSalesList();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grd_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                GridSortData(CurrentSortDirection, e.SortExpression, AdvanceSalesDT, grd);
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grd.PageIndex = e.NewPageIndex;
                grd.DataSource = AdvanceSalesDT;
                grd.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void ddlSoldTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadAdvanceSalesList();

                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadShipTos()
        {
            if (string.IsNullOrEmpty(Search.SoldTo))
            {
                ShipTos = new List<DropDownValue>();
            }
            else
            {
                //ShipTos = ShipmentDetails.GroupBy(o => o.ShipTo, o => o.ShipToName, (id, text) => new DropDownValue() { Code = id, Name = text.First() }).ToList();
                ShipTos = AdvanSales.GroupBy(o => o.Ship_to_party, o => o.Ship_to_party_name, (id, text) => new DropDownValue() { Code = id, Name = string.Format("{0} : {1}", id, text.First()) }).ToList();
                ShipTos.Insert(0, new DropDownValue() { Code = "", Name = "All" });
            }
            //ArrayList array = new ArrayList();
            //foreach (DropDownValue value in ShipTos)
            //{
            //    array.Add(value.Name);
            //    //  Console.WriteLine("FOREACH ITEM: " + value);
            //}
            //  for (int i = 0; i < 100; i++) array.Add(i.ToString());
            ddlShipTo.AddItems(ShipTos);



           // ddlShipTo.DataSource = ShipTos;
            ddlShipTo.DataBind();
        }

        async Task<string> PostURI(Uri u, HttpContent c, string ordernumber)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                HttpClient cc = new HttpClient();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12| SecurityProtocolType.Tls11;
                var response = string.Empty;
                LoginParam login = new LoginParam();
                login.username = "smartorder@hmcpolymers.com";
                login.password = "Hmc@2019";
                login.deviceID = "-";
                string api = "";
                var to = cc.PostAsync(string.Format("{0}authentication/login", "https://digitalqas.hmcpolymers.com/api/"), new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;
                var resultxx = JsonConvert.DeserializeObject<LoginResult>(to);

                if (resultxx.success)
                {
                    api = resultxx.access_token;
                    sb.Append("x2x.access_token====" + resultxx.access_token + "<br>");

                }
                else
                {
                    api = "";
                    sb.Append("x2x.error====" + resultxx.error + "<br>");

                }


                if (resultxx.success)
                {
                  
                    try
                    {
                        //  sb.Append("DefaultRequestHeaders" + "<br>");
                        cc.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", api);
                        //  sb.Append("cc2.DefaultRequestHeaders.Authorization" + "<br>");

                        sb.Append(string.Format("{0}do/getid/{1}", "https://digitalqas.hmcpolymers.com/api/", ordernumber) + "<br>");
                        var result11 = await cc.GetAsync(string.Format("{0}do/getid/{1}", "https://digitalqas.hmcpolymers.com/api/", ordernumber));
                        sb.Append("HttpResponseMessage" + "<br>");
                        var ccon = result11.Content.ReadAsStringAsync().Result;
                        sb.Append("DO ID>>>>>>>" + ccon + "<<<<<<<\n" + "<br>");
                        if (result11.IsSuccessStatusCode)
                        {

                            //sb.Append(string.Format("{0}do/transport-order/{1}?doc=DO,COA&view=DOWNLOAD", "https://digitalqas.hmcpolymers.com/api/", "response"));
                            var filecon = cc.GetAsync(string.Format("{0}do/transport-order/{1}?doc=DO,COA&view=DOWNLOAD", "https://digitalqas.hmcpolymers.com/api/", ccon));
                            sb.Append("11<br>");
                            var bytes = filecon.Result.Content.ReadAsByteArrayAsync().Result;
                            sb.Append("112<br>");
                            if (filecon.Result.IsSuccessStatusCode)
                            {
                                sb.Append("113<br>");
                                if (bytes.Length > 0)
                                {
                                    sb.Append("114<br>");
                                    File.WriteAllBytes(Server.MapPath("~/temp/") + ordernumber + ".pdf", bytes);

                                    string url = "DownloadFile.ashx?file=" + ordernumber + ".pdf";
                                    ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "downloadFileExport", "javascript:window.location ='" + ResolveUrl(url) + "';", true);
                                }
                                else
                                {
                                    sb.Append("115<br>");
                                }
                            }
                            else
                            {
                                sb.Append("115<br>");
                                // MessageBox.Show(filecon.Result.ReasonPhrase);
                            }
                        }
                        //   Label1.Text = sb.ToString();
                        sb.Append("!!!!!end!!!!!");
                        //  Label1.Text = sb.ToString();
                        return "";
                    }
                    catch (Exception e)
                    {
                        sb.Append("error" + e.Message);
                        // Label1.Text = e.Message;
                        //     Label1.Text = sb.ToString();
                        return "";
                    }
                }
                // }
                return response;
            }
            catch (Exception e)
            {
                sb.Append("error2" + e.Message);
                //  Label1.Text = sb.ToString();
                return "";
            }
        }
        protected void grd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ORDER_DOWNLOAD")
            {
                Boolean platformSupportsTls12 = false;
                foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
                {
                    // Console.WriteLine(protocol.GetHashCode());
                    if (protocol.GetHashCode() == 3072)
                    {
                        platformSupportsTls12 = true;
                    }
                }
               
                if (!ServicePointManager.SecurityProtocol.HasFlag((SecurityProtocolType)3072))
                {
                    if (platformSupportsTls12)
                    {
                        //    Console.WriteLine("Platform supports Tls12, but it is not enabled. Enabling it now.");
                        ServicePointManager.SecurityProtocol |= (SecurityProtocolType)3072;
                    }
                    else
                    {
                        //    Console.WriteLine("Platform does not supports Tls12.");
                    }
                }

                // disable ssl3
                if (ServicePointManager.SecurityProtocol.HasFlag(SecurityProtocolType.Ssl3))
                {
                    //  Console.WriteLine("Ssl3SSL3 is enabled. Disabling it now.");
                    // disable SSL3. Has no negative impact if SSL3 is already disabled. The enclosing "if" if just for illustration.
                    System.Net.ServicePointManager.SecurityProtocol &= ~SecurityProtocolType.Ssl3;
                }
                //   Console.WriteLine("Enabled protocols:   " + ServicePointManager.SecurityProtocol);
                LoginParam login = new LoginParam();
                login.username = "smartorder@hmcpolymers.com";
                login.password = "Hmc@2019";
                login.deviceID = "-";
                Uri u = new Uri("https://digitalqas.hmcpolymers.com/api/authentication/login");
                HttpContent hcc = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
                var t = Task.Run(() => PostURI(u, hcc, e.CommandArgument.ToString()));
                t.Wait();
            }
        }
    }
}