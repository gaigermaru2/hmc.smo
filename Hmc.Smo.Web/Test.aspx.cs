﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class Test : System.Web.UI.Page
    {
        public int? Step
        {
            get { return ViewState["Step"] as int?; }
            set { ViewState["Step"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            Step = 1;
            DataBind();
        }

        protected void Unnamed_Click1(object sender, EventArgs e)
        {
        }
    }
}