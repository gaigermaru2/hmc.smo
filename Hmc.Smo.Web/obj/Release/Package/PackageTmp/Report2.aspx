﻿<%@ Page Title="Orders : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Report2.aspx.cs" Inherits="Hmc.Smo.Web.Report2"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageName" runat="server">
    Dashboard
      <link type="text/css" href="assets/css/argon.css?v=1.0.0" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 7px">
       
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphCriteria" runat="server">
 
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="row" >
         <div class="col-lg-4">        
          <div class="card card-stats mb-4 mb-xl-0"  >
               <div class="panel panel-primary">
                    <div class="panel-heading">
                Sales Order/Month
                    </div>
                <div class="card-body" style="height:150px;">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
              </div>
        </div>
         <div class="col-lg-4">
        
          <div class="card card-stats mb-4 mb-xl-0">
               <div class="panel panel-red">
                    <div class="panel-heading">
                Sales Order/Month
                    </div>
                <div class="card-body"  style="height:150px;">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
              </div>
        </div>
         <div class="col-lg-4">
        
          <div class="card card-stats mb-4 mb-xl-0">
               <div class="panel panel-green">
                    <div class="panel-heading">
                <h5>Sales Order/Month</h5>
                    </div>
                <div class="card-body"  style="height:150px;">
                  <div class="row">
                    <div class="col">
                     
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
              </div>
        </div>
    </div>
    <div class="row">
     <div class="col-lg-6">
        
          <div class="card card-stats mb-6 mb-xl-0">
               <div class="panel panel-green">
                    <div class="panel-heading">
                <h5>Announcement</h5>
                    </div>
                <div class="card-body"  style="height:200px;">
                  <div class="row">
                    <div class="col">
                     
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
              </div>
        </div>
         <div class="col-lg-6">
        
          <div class="card card-stats mb-6 mb-xl-0">
               <div class="panel panel-primary">
                    <div class="panel-heading">
                <h5>Performance</h5>
                    </div>
                <div class="card shadow" style="height:200px;">
           
            <div class="card-body">
              <!-- Chart -->
              <div class="chart" >
                <canvas id="chart-orders" class="chart-canvas" style="height:190px;"></canvas>
              </div>
            </div>
          </div>
              </div>
              </div>
        </div>
    </div>

        <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.0.0"></script>
    <style>
          element.style {
    height: 200px;
    display: block;
    width: 328px;
}
    </style>
  
</asp:Content>
