﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logon.aspx.cs" Inherits="Hmc.Smo.Web.Logon" %>


<!------ Include the above in your HEAD tag ---------->


<!DOCTYPE html>
<html>
<head>
	<title>Welcome to SmartOrder System by HMC</title>
   <!--Made with love by Mutiullah Samim -->
   
	<!--Bootsrap 4 CDN-->
	<link rel="stylesheet" 
href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
   
 
    <!--Fontawesome CDN-->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt
+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!--Custom styles-->
	<link rel="stylesheet" type="text/css" href="styles.css">
    <link 
href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script 
src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script 
type="text/javascript" src="Scripts/JScript.js"></script>
    <link type="text/css" href="Styles/panel.css" rel="stylesheet" />
    <link type="text/css" href="Styles/w3.css" rel="stylesheet" />
    <style>
        /* Made with love by Mutiullah Samim*/

@import url('https://fonts.googleapis.com/css?family=Numans');

html,body{
background-image: url('images/login4.jpg');
background-size: cover;
background-repeat: no-repeat;
height: 100%;
font-family: 'Tahoma', sans-serif;
}

.container{
height: 100%;
align-content: center;
}

.card{
height: 300px;
margin-top:auto;
float:right;
margin-bottom: 
auto;
width: 470px!important;
background-color: rgba(0,0,0,0.5) !important;
}

.cardannounce{
height: 300px;
margin-top:auto;
float:right;
margin-bottom: auto;
width: 350px;
background-color: rgba
(0,0,0,0.5) !important;
}
.social_icon span{
font-size: 60px;
margin-left: 10px;
color: #FFC312;
}

.social_icon span:hover{
color: white;
cursor: pointer;
}

.card-header h3{
color: white;
}

.social_icon{
position: absolute;
right: 20px;
top: -45px;
}

.input-group-prepend span{
width: 50px;
background-color: #ffc107;
color: black;
border:0 !important;
}

input:focus{
outline: 0 0 0 0  !important;
box-
shadow: 0 0 0 0 !important;

}

.remember{
color: white;
}

.remember input
{
width: 20px;
height: 20px;
margin-left: 15px;
margin-right: 5px;
}

.login_btn{
color: black;
background-color: #FFC312;
width: 
100px;
}

.login_btn:hover{
color: black;
background-color: white;
}

.links{
color: white;
}

.links a{
margin-left: 4px;
}

.Absolute-Center {
  margin: auto;
  position: absolute;
  top: 0; left: 0; bottom: 0; right: 0;
}

.Absolute-Center.is-Responsive {
  width: 50%; 
  height: 50%;
  min-width: 900px;
  max-width: 1000px;
  padding: 40px;
}
    </style>
    <script type='text/javascript' src='jquery-3.1.1.min.js'></script>
    <script type='text/javascript' src='jquery-ui/jquery-ui.js'></script>
    <script type='text/javascript'>
$(document).on('click', '.toggle-password', function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
    
    var input = $("#txtPassword");
    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
});
    </script>





<script type="text/javascript">
        function CB() {
    try {
        var p = [
            'images/login1.jpg',
            'images/login2.jpg',
            'images/login4.jpg'
        ];

        var counter = 0;

        setInterval(function(){
          //  document.body.style.backgroundImage = url(p[counter++]);
          //  alert('url('+p[counter++]+')');
             $('body').css('background-image', 'url('+p[counter]+')');
            console.log(counter++);
         //   alert(counter);
            if(counter == 3){
                counter = 0;
            }
        }, 8000)
    } catch(err) {
        alert(err.message);
    }
}

//CB();

    </script>

</head>
<body  onload="CB()">
        <form id="form1" runat="server">
         <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
      <%--          <div class="w3-display-container">
        <div class="mySlides w3-animate-opacity" style="height: 100%; max-width: 100%; background: url('Images/login1.jpg') center center;"></div>
        <div class="mySlides w3-animate-opacity" style="height: 100%; max-width: 100%; background: url('Images/login2.jpg') center center;"></div>
        <div class="mySlides w3-animate-opacity" style="height: 100%; max-width: 100%; background: url('Images/login3.jpg') center center;"></div>

        <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
        <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
    </div>--%>
<div class="container Absolute-Center is-Responsive">

	<div class="d-flex justify-content-end h-100 ">

     <div class="cardannounce">
          <div class="card-header" style="width:350px;background-color:#ffc107;color:black"><b>Welcome to SmartOrder System</b></div>
          <div class="panel-body"> <div class="alert alert-success" role="alert" style="height:258.5px;background-color:white">
              HMC Shows at Chinaplas
HMC Polymers joined with joint venture associate PTTGC to exhibit at the recent Chinaplas 2019</div>
        </div>
</div>


		<div class="card" >
			<div class="card-header" style="width:470px;color:white;">
				<b>Sign In</b>
			
	
			</div>
			<div class="card-body" style="width:470px;">

					<div class="input-group form-group">
			
			<div class="input-group-prepend">
							<span class="input-group-text"><i class="fas fa-user"></i></span>
		
				</div>
						
						 <asp:TextBox ID="txtUsername" runat="server" CssClass="form-
control" MaxLength="40"
                                    placeholder="enter your username/email" />
                                <asp:RequiredFieldValidator ID="vrqUsername" ControlToValidate="txtUsername" runat="server"
                                    CssClass="label-r" ValidationGroup="login" SetFocusOnError="true" Display="Dynamic" />
					</div>
				
	<div class="input-group form-group">
						<div class="input-group-prepend">
							<span 
class="input-group-text"><i class="fas fa-key"></i></span>
						</div>
						
                         <asp:TextBox 
ID="txtPassword" runat="server" CssClass="form-control" MaxLength="20"
                                    TextMode="Password" placeholder="enter your password" />
                                <asp:RequiredFieldValidator ID="vrqPass" ControlToValidate="txtPassword" runat="server"
                                    CssClass="label-r" ValidationGroup="login" SetFocusOnError="true" Display="Dynamic" />

<span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password"  style="background-color:#ffc107;vertical-align:bottom" ></span></div>
					
<div class="w3-row-padding w3-margin">
                            <div class="w3-half">
                                <asp:Button ID="btnSignIn" Text="SIGN IN" runat="server" CssClass="btn btn-warning" 
                                    ValidationGroup="login" OnClick="btnSignIn_Click" />
                                <asp:Label ID="txtMsg" runat="server" Style="color: red" />
                            </div>
                            <div class="w3-half">
                                <asp:LinkButton ID="lnkResetPassword" Text="Reset your password?" PostBackUrl="~/ResetPassword.aspx"
                                    runat="server" CssClass="label" ForeColor="White" Font-Size="10pt" />
                            </div>
                        </div>
                            
					

			</div>

		
		</div>

      
	</div>
    <%--	<div 
class="card-footer">
				
				<div class="d-flex justify-content-center">
				   <a 
href="https://www.hmcpolymers.com/applications" target="_blank">Application</a> |
                    <a href="https://www.hmcpolymers.com/product-table" target="_blank">Product Table</a> |
                    <a href="https://www.hmcpolymers.com/uploads/files/resources/hmc-product-selection-guide.pdf">Product Quick Reference Guide</a> |
                    <a href="https://www.hmcpolymers.com/latest-news" target="_blank">Latest News</a>
				</div>
			</div>--%>
</div>
            </form>
</body>
</html>

