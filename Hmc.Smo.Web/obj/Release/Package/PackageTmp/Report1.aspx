﻿<%@ Page Title="Orders : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Report1.aspx.cs" Inherits="Hmc.Smo.Web.Report1"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageName" runat="server">

    <div class="panel-heading" style="font-size: 20px; font-weight: bold;">
        <a href="#" onclick="Toggle()" style="color: #f0ad4e; cursor: pointer">☰</a> Sales Order by Price Category<div style="float: right; z-index: 10; right: 0; vertical-align: middle"><%=CurrentUser.FullName%></div>
    </div>
    <style>
        a:link {
            color: #337ab7;
        }
        /* unvisited link  */
        a:visited {
            color: #337ab7;
        }
        /* visited link    */
        a:hover {
            color: #337ab7;
        }
        /* mouse over link */
        a:active {
            color: #337ab7;
        }
        /* selected link   */
        #chartjs-tooltip {
            opacity: 1;
            position: absolute;
            background: rgba(0, 0, 0, .7);
            color: white;
            border-radius: 3px;
            -webkit-transition: all .1s ease;
            transition: all .1s ease;
            pointer-events: none;
            -webkit-transform: translate(-50%, 0);
            transform: translate(-50%, 0);
        }

        .chartjs-tooltip-key {
            display: inline-block;
            width: 10px;
            height: 10px;
            margin-right: 10px;
        }
    </style>
    <link type="text/css" href="assets/css/argon.css?v=1.0.0" rel="stylesheet">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 7px">
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphCriteria" runat="server">
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">


    <div class="col">
        <ul class="nav nav-pills justify-content-end" style="color: white">
            <button id="btnYear" type="button" class="btn btn-warning" onclick="LoadChart('Y');">
                Year
            </button>
            <button id="btnQ" type="button" class="btn btn-warning" onclick="LoadChart('Q');">
                Quarter
            </button>

            <%--    <a href="#"  onclick="downloadImage();" class="btn btn-warning" style="color:white">Save as image</a> --%>
                  
                     

                    </li>
                     
        </ul>
    </div>
    <%-- <div class="col-lg-12">--%>
    <%--         <div class="row"></div>
          <div class="card card-stats mb-12 mb-xl-0">--%>
    <div class="panel panel-primary">
        <div class="panel-heading" style="height: 36px; vertical-align: text-top">
            <a data-toggle="collapse" href="#fildiv" style="color: white; font-weight: bold; font-size: 14px">Sales Order by Price Category <%=DateTime.Now.Year %></a>
        </div>
        <div class="card shadow" style="height: 400px;">

            <div class="card-body">
                <!-- Chart -->
                <div class="chart">
                    <canvas id="canvas" class="chart-canvas" style="height: 400px;"></canvas>
                </div>
                <div id="wrapper" style="float: right; margin-right: 0px; margin-top: 0px; z-index: 3">
                </div>

            </div>

        </div>
    </div>

    <%--  </div>--%>
    <%--  </div>
    </div>--%>


    <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Optional JS -->
    <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
    <%-- <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>--%>
    <!-- Argon JS -->

    <script src="assets/js/argon.js?v=1.0.0"></script>

    <script>



        var myPieChart = null;
        // var myChart;
        $(function () { LoadChart('Y'); });
        function LoadChart(dType) {
            //   myChart.destroy();
            if (myPieChart != null) {
                myPieChart.destroy();
            }
            var chartType = 1;

            $.ajax({
                type: "POST",
                url: "Report1.aspx/R1GetChart",
                data: "{cType: '" + dType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (chData) {
                    var obj = JSON.parse(chData.d);
                    var aCustomerGroupText = new Array();
                    var aMonthText = new Array();

                    var i;
                    for (i = 0; i < obj.length; i++) {

                        aCustomerGroupText[i] = obj[i].CustomerGroupText;
                        aMonthText[i] = obj[i].month;

                    }

                    var lbmonth = aMonthText.filter((x, i, a) => a.indexOf(x) == i);
                    var cg = aCustomerGroupText.filter((x, i, a) => a.indexOf(x) == i);

                    var ctx = document.getElementById("canvas").getContext('2d');
                    ctx.height = 900;

                    var config = {
                        type: 'line',
                        data: {
                            labels: lbmonth,
                            datasets: []
                        },
                        options: {

                            legend: {
                                display: true,
                                position: 'top',
                                labels: {
                                    fontColor: "#000080",
                                }
                            }


                            ,
                            maintainAspectRatio: true,
                            responsive: true,
                            title: {
                                display: false,
                                text: 'Sales Order by Price Category'
                            },
                            tooltips: {
                                enabled: false,
                                mode: 'index',
                                position: 'nearest',
                                custom: customTooltips,
                            },
                            hover: {
                                mode: 'index',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Month'
                                    }, gridLines: {
                                        display: true,
                                        drawOnChartArea: false
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Qty (TON)'
                                    }, gridLines: {
                                        display: true,
                                        drawOnChartArea: true
                                    }
                                }]
                            }
                        }
                    };
                    // var color = Chart.helpers.color;
                    //   var newColor = utils.color(chart.data.datasets.length);
                    var cc = Array();
                    cc[0] = '#ff6384';
                    cc[1] = '#36a2eb';
                    cc[2] = '#cc65fe';
                    cc[3] = '#ffce56';



                    for (i = 0; i < cg.length; i++) {
                        // alert(cg[i]);
                        var newDataset = {
                            label: cg[i],
                            borderColor: cc[i],
                            backgroundColor: cc[i],
                            fill: false,
                            data: [],
                        };

                        var xx = obj.filter(x => x.CustomerGroupText === cg[i]);

                        for (var index = 0; index < xx.length; index++) {
                            newDataset.data.push(xx[index].TotalQty);
                        }
                        //      for (var index = 0; index < 12; ++index) {
                        //newDataset.data.push((10+99/index+1));
                        //      }
                        config.data.datasets.push(newDataset);
                        jQuery('#wrapper').html(dataToTable(config.data));
                    }
                    myPieChart = new Chart(ctx, config);
                    //window.myChart.update();

                },
                failure: function (response) {
                    alert('There was an error.');
                }
            });
        }

        var customTooltips = function (tooltip) {
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-tooltip');

            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                tooltipEl.innerHTML = '<table></table>';
                this._chart.canvas.parentNode.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltip.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltip.yAlign) {
                tooltipEl.classList.add(tooltip.yAlign);
            } else {
                tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
                return bodyItem.lines;
            }

            // Set Text
            if (tooltip.body) {
                var titleLines = tooltip.title || [];
                var bodyLines = tooltip.body.map(getBody);

                var innerHtml = '<thead>';

                titleLines.forEach(function (title) {
                    innerHtml += '<tr><th>' + title + '</th></tr>';
                });
                innerHtml += '</thead><tbody>';

                bodyLines.forEach(function (body, i) {
                    var colors = tooltip.labelColors[i];
                    var style = 'background:' + colors.backgroundColor;
                    style += '; border-color:' + colors.borderColor;
                    style += '; border-width: 2px';
                    var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                    innerHtml += '<tr><td>' + span + body + '</td></tr>';
                });
                innerHtml += '</tbody>';

                var tableRoot = tooltipEl.querySelector('table');
                tableRoot.innerHTML = innerHtml;
            }

            var positionY = this._chart.canvas.offsetTop;
            var positionX = this._chart.canvas.offsetLeft;

            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.left = positionX + tooltip.caretX + 'px';
            tooltipEl.style.top = positionY + tooltip.caretY + 'px';
            tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
            tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
            tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
            tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
        };

        //position: absolute;
        //background: rgba(0, 0, 0, .7);
        //color: white;
        //border-radius: 3px;
        //-webkit-transition: all .1s ease;
        //transition: all .1s ease;
        //pointer-events: none;
        //-webkit-transform: translate(-50%, 0);
        //transform: translate(-50%, 0);


        var dataToTable = function (dataset) {
            var html = '<table  style="width:300px;float:right; background: rgba(0, 0, 0, .7);color: white;border-radius: 3px;-webkit-transform: translate(-50%, 0);transform: translate(-50%, 0);">';
            html += '<thead><tr><th style="width:20px;">#</th>';
            html += '<th style="width:120px;">Y <%=DateTime.Now.Year %></th>';
            html += '<th style="width:120px;">Total</th>';

            var columnCount = 0;
            var columnCount2 = 0;
            //   jQuery.each(dataset.datasets, function (idx, item) {
            ////   html += '<th style="background-color:' + item.fillColor + ';width:120px;" >' + item.label + '</th>';
            //   columnCount += 1;
            //   });
            var indexxxx = 0;
            html += '</tr></thead>';
            jQuery.each(dataset.datasets, function (idx, item) {
                //     alert(item.label);
                html += '<tr><td></td><td style="background-color:' + item.backgroundColor + ';width:120px;" >' + item.label + '</td>';
                //   columnCount += 1;
                //     alert(columnCount);
                //   alert(item.label);
                var cc = 0;

                //for (i = 0; i < 1; i++) {
                //    html += '<td style="background-color:' + dataset.datasets[i].fillColor + ';">' + (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '</td>';
                //     cc += dataset.datasets[i].data[idx];
                //}
                jQuery.each(dataset.labels, function (idx2, item2) {

                   // for (i = 0; i < 1; i++) {
                        //   html += '<td style="background-color:' + dataset.datasets[i].fillColor + ';">' + (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '</td>';
                        //alert(cc);
                        cc += dataset.datasets[indexxxx].data[idx2];
                  //       }
                });
                indexxxx++;
                html += '<td >' + cc + '</td>';
                html += '</tr>';
            });



            //  jQuery.each(dataset.labels, function (idx, item) {
            ////  html += '<tr><td>' + item + 'xxx</td>';
            //  for (i = 0; i < columnCount; i++) {
            //               html += '<td style="background-color:' + dataset.datasets[i].fillColor + ';">' + (dataset.datasets[i].data[idx] === '0' ? '-' : dataset.datasets[i].data[idx]) + '</td>';
            //  }
            //  html += '</tr>';
            //  });

            html += '</tr><tbody></table>';

            return html;
        };



    </script>

</asp:Content>
