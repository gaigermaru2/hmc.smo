﻿<%@ Page Title="Reset Password : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="ResetPassword.aspx.cs" Inherits="Hmc.Smo.Web.ResetPassword" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphPageName" runat="server">
  
     <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e">☰</a>   Reset Password

    </div>
        <style>a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */ </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="cphContent" runat="server">
    <div class="div-content">
        <div class="header-text">
            Infomation
        </div>

        <asp:Panel runat="server" DefaultButton="btnReset">
            <div class="row">
                <div class="control-label col-md-4">
                    Your user <span style="color: red;">*</span>
                </div>
                <div class="col-3">
                    <asp:TextBox ID="txtUsername" MaxLength="40" CssClass="textbox" runat="server" />
                </div>
                <div class="col-5 label">
                    <asp:RequiredFieldValidator ControlToValidate="txtUsername" ValidationGroup="reset"
                        Display="Dynamic" SetFocusOnError="true" CssClass="label-r" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="control-label col-md-4">
                    Current password <span style="color: red;">*</span>
                </div>
                <div class="col-3">
                    <asp:TextBox ID="txtCurPass" MaxLength="20" CssClass="textbox" runat="server" TextMode="Password" />
                </div>
                <div class="col-5 label">
                    <asp:RequiredFieldValidator ControlToValidate="txtCurPass" ValidationGroup="reset"
                        Display="Dynamic" SetFocusOnError="true" CssClass="label-r" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="control-label col-md-4">
                    New password <span style="color: red;">*</span>
                </div>
                <div class="col-3">
                    <asp:TextBox ID="txtNewPass" MaxLength="20" CssClass="textbox" runat="server" TextMode="Password" />
                </div>
                <div class="col-5 label">
                    <asp:RequiredFieldValidator ControlToValidate="txtNewPass" ValidationGroup="reset"
                        Display="Dynamic" SetFocusOnError="true" CssClass="label-r" runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="control-label col-md-4">
                    Confirm new password <span style="color: red;">*</span>
                </div>
                <div class="col-3">
                    <asp:TextBox ID="txtNewPassConfirm" MaxLength="20" CssClass="textbox" runat="server"
                        TextMode="Password" />
                </div>
            </div>
            <div class="row">
                <div class="col-2 label">
                    <asp:Image ID="Image2" ImageUrl="~/Images/space.png" runat="server" />
                </div>
                <div class="col-7">
                    <asp:CompareValidator ID="cpvNewPassConfirm" ErrorMessage="New Password and Confirm new password must be same"
                        ControlToValidate="txtNewPassConfirm" ControlToCompare="txtNewPass" ValidationGroup="reset"
                        Display="Dynamic" SetFocusOnError="true" CssClass="label-r" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtNewPassConfirm"
                        ValidationGroup="reset" Display="Dynamic" SetFocusOnError="true" CssClass="label-r"
                        runat="server" />
                </div>
            </div>
            <div class="row">
                <div class="col-2 label">
                    <asp:Image ID="Image1" ImageUrl="~/Images/space.png" runat="server" />
                </div>
                <div class="col-3">
                    <asp:Button ID="btnReset" CssClass="button-img-s img-correct-white" Text="Reset Password"
                        runat="server" ValidationGroup="reset" OnClick="btnReset_Click" />
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
