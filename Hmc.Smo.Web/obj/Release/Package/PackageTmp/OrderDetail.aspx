﻿<%@ Page Title="Order : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="OrderDetail.aspx.cs" Inherits="Hmc.Smo.Web.OrderDetail"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ContentPlaceHolderID="cphPageName" runat="server" >
    
    <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    Order > <asp:Label ID="lblPageName" Text="Order" runat="server" /><div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>
    </div>
    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script type="text/javascript">
        function CountChars(charLimit, inputId, outputId) {
            var charsCount = document.getElementById(inputId).value.length;
            var remainingChars = charLimit - charsCount;
            document.getElementById(outputId).innerHTML = 'Count char: ' + charsCount + '/255';
        }
        //$(window).bind("load", function() {
        //    var ds = document.getElementById("maindiv");
        //    ds.style.display = 'none';
        //});
    </script>
     <script language="javascript" type="text/javascript">
        var submit = 0;
        function CheckIsRepeat() {
            if (++submit > 1) {
                alert('Please wait for response Message from SAP for a while.');
                return false;
            }
        }
    </script>
                 <style> th {
     text-align: center;
}
             </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 7px">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnDownloadInvoiceOpen" Text="Invoice (PDF)" runat="server" CssClass="btn btn-warning" Visible='<%# InvoiceDownloadVisibility %>' />
                <asp:Button ID="btnAttachFile" Text="ATTACHMENTS" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanSeeAttachFileButton %>' OnClick="btnAttachFile_Click" />
                <asp:Button ID="btnSave" Text="SAVE as DRAFT" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanUserEdit %>' OnClick="btnSave_Click" />
                <asp:Button ID="btnUserSubmit" Text="SUBMIT" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanUserEdit %>' OnClick="btnConfirmOpen_Click"
                    CommandArgument="USR" />
                <asp:Button ID="btnUserCancel" Text="CANCEL" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanUserCancel %>' OnClick="btnReasonOpen_Click"
                    CommandArgument="CancelByUser" />
                <asp:Button ID="btnManagerApprove" Text="APPROVE" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanManagerApprove %>' OnClick="btnConfirmOpen_Click"
                    CommandArgument="MGR" />
                <asp:Button ID="btnManagerReject" Text="REJECT" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanManagerApprove %>' OnClick="btnReasonOpen_Click"
                    CommandArgument="RejectByApprover" />
                <asp:Button ID="btnManagerCancel" Text="CANCEL" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanManagerApprove %>' OnClick="btnReasonOpen_Click"
                    CommandArgument="CancelByApprover" />
                <asp:Button ID="btnHmcSave" Text="SAVE as DRAFT" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanHMCEdit %>' OnClick="btnHmcSave_Click" />
                <asp:Button ID="btnHMCApprove" Text="SUBMIT to SAP" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanHMCEdit %>' OnClick="btnConfirmOpen_Click"
                    CommandArgument="HMC" />
                <asp:Button ID="btnHMCReject" Text="REJECT" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanHMCEdit %>' OnClick="btnReasonOpen_Click"
                    CommandArgument="RejectByHMC" />
                <asp:Button ID="btnHMCCancel" Text="CANCEL" runat="server" CssClass="btn btn-warning"
                    ValidationGroup="order" Visible='<%# CanHMCEdit %>' OnClick="btnReasonOpen_Click"
                    CommandArgument="CancelByHMC" />
                <asp:Panel ID="pnlReason" runat="server" CssClass="panel-popup" Width="600px" DefaultButton="btnReasonYes">
                    <div class="panel panel-primary" >
                            <div class="panel-heading"  style="height:60px;vertical-align:text-top;font-weight:bold">
                            <asp:Image ID="Image1" ImageUrl="~/Images/notice_40_black.png" CssClass="panel-popup-hd-img"
                                runat="server" />
                            <div class="panel-popup-hd-text">
                                <asp:Label ID="txtReasonHeader" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="panel-popup-content">
                        <div class="row">
                            <asp:DropDownList ID="ddlReason" runat="server" DataValueField="Key" DataTextField="Value"
                                CssClass="dropdown" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlReason_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div class="row">
                            <asp:TextBox ID="txtReason" runat="server" CssClass="textbox" Width="97.1%" TextMode="MultiLine" />
                            <asp:RequiredFieldValidator ID="rfvRemark" ControlToValidate="txtReason" runat="server"
                                ValidationGroup="reason" />
                        </div>
                        <div class="row" style="float: right">
                            <asp:Button ID="btnReasonCancel" Text="Close" runat="server" CssClass="btn btn-warning" />
                            <asp:Button ID="btnReasonYes" Text="Reject" runat="server" CssClass="btn btn-warning"
                                OnClick="btnReasonYes_Click" ValidationGroup="reason" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Button ID="btnReasonHidden" runat="server" CssClass="hide" />
                <atk:ModalPopupExtender ID="mpeReason" BackgroundCssClass="panel-bg" PopupControlID="pnlReason"
                    TargetControlID="btnReasonHidden" CancelControlID="btnReasonCancel" runat="server" />
                <asp:Panel ID="pnlConfirm" runat="server" CssClass="panel-popup" Width="600px" DefaultButton="btnConfirmYes">
                   <div class="panel panel-primary" >
                            <div class="panel-heading"  style="height:60px;vertical-align:text-top;font-weight:bold">
                            <asp:Image ID="Image3" ImageUrl="~/Images/notice_40_black.png" CssClass="panel-popup-hd-img"
                                runat="server" />
                            <div class="panel-popup-hd-text">
                                Confirmation
                            </div>
                        </div>
                    </div>
                    <div class="panel-popup-content">
                        <div class="row">
                            <asp:TextBox ID="txtConfirm" runat="server" CssClass="textbox-msg" TextMode="MultiLine"
                                ReadOnly="true" />
                        </div>
                        <div class="row" style="float: right">
                            <asp:Button ID="btnConfirmNo" Text="No" runat="server" CssClass="btn btn-warning" />
                            <asp:Button ID="btnConfirmYes" Text="Yes" runat="server" CssClass="btn btn-warning" OnClientClick="return CheckIsRepeat();"
                                OnClick="btnConfirmYes_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Button ID="btnConfirmHidden" runat="server" CssClass="hide" />
                <atk:ModalPopupExtender ID="mpeConfirm" BackgroundCssClass="panel-bg" PopupControlID="pnlConfirm"
                    TargetControlID="btnConfirmHidden" CancelControlID="btnConfirmNo" runat="server" />
                <asp:Panel ID="pnlAttachment" runat="server" CssClass="panel-popup" Width="600px"
                    DefaultButton="btnAttachmentCancel">
<%--                    <div class="panel-popup-hd">
                        <div class="panel-heading">

                            <asp:Image ID="Image5" ImageUrl="~/Images/notice_40_black.png" CssClass="panel-popup-hd-img"
                                runat="server" />
                            <div class="panel-popup-hd-text">
                                <asp:Label ID="txtAttachmentHeader" runat="server" Text="Attachments" />
                            </div>
                        </div>
                    </div>--%>
                                  <div class="panel panel-primary">
            <div class="panel-heading"  style="height:40px;vertical-align:text-top">
               <a data-toggle="collapse"   style="color:white">Attachments</a>
            </div>
</div>
                    <%--  <div id="fattachment" class="panel-collapse collapse in">--%>
                   <div class="panel-popup-content">
                        <asp:Panel ID="pnlAttachmentAdd" runat="server">
                            <div class="row">
                                <div class="col-2">
                                    Choose file
                                </div>
                                <div class="col-8">
                                    <atk:AsyncFileUpload runat="server" Width="100%" UploaderStyle="Traditional" UploadingBackColor="#CCFFFF"
                                        ThrobberID="pnlImageUpload" OnUploadedComplete="fulAttachment_UploadedComplete"
                                        CssClass="FileUploadClass" />
                                    <asp:Panel ID="pnlImageUpload" runat="server">
                                        <div class="loading-bg">
                                            <asp:Image ID="impupUpload" runat="server" ImageUrl="~/Images/loading.gif" CssClass="loading-img" />
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-7">
                                    <asp:Label ID="lblErrorUpload" runat="server" CssClass="err" />
                                </div>
                                <div class="col-3" style="float: right">
                                    <asp:Button ID="btnAttachmentOK" runat="server" CssClass="button-img-s img-add-w"
                                        OnClick="btnAttachmentUpload_Click" Text="Add Attachment" OnClientClick="this.disabled=true;"
                                        UseSubmitBehavior="false" />
                                </div>
                            </div>
                            <div class="row">
                                <ul class="list-text">
                                    <li>Accept file
                                    <asp:Label ID="lblAcceptExt" runat="server" />
                                    </li>
                                    <li>File size not over
                                    <asp:Label ID="lblAttachMaxSize" runat="server" />
                                        Megabyte </li>
                                    <li>Allow only
                                    <asp:Label ID="lblAttachMaxFile" runat="server" />
                                        files on each order</li>
                                </ul>
                            </div>
                        </asp:Panel>
                        <div class="row">
                            <asp:Panel ID="pnlAttachmentGrid" runat="server" ScrollBars="Vertical" Height="240px">
                                <asp:GridView ID="grdAttachment" Width="100%" runat="server" AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                    DataKeyNames="AttachmentId" OnRowCommand="grdAttachment_RowCommand" OnRowDataBound="grdAttachment_RowDataBound"
                                    CssClass="table table-striped"   PagerStyle-CssClass="pager"
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ACTION">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnAttachmentDelete" ImageUrl="~/Images/delete_40.png" runat="server"
                                                    CommandName="DEL" CommandArgument='<%# Eval("AttachmentId") %>' Height="20px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="FILE NAME">
                                            <ItemTemplate>
                                                <asp:LinkButton Text='<%# Eval("FileName") %>' runat="server" CommandName="DWNLD"
                                                    CommandArgument='<%# Eval("AttachmentId") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CreatedTime" HeaderText="UPLOADED TIME" />
                                        <asp:BoundField DataField="CreatedBy" HeaderText="UPLOADED BY" />
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </div>
                        <div class="row" style="float: right">
                            <asp:Button ID="btnAttachmentCancel" runat="server" CssClass="btn btn-warning"
                                Text="Close" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Button ID="btnAttachmentHidden" runat="server" CssClass="hide" />
                <atk:ModalPopupExtender ID="mpeAttachment" BackgroundCssClass="panel-bg" PopupControlID="pnlAttachment"
                    TargetControlID="btnAttachmentHidden" CancelControlID="btnAttachmentCancel" runat="server" />
                <asp:Panel ID="pnlAttachmentDeleteConfirm" runat="server" CssClass="panel-popup"
                    Width="600px" DefaultButton="btnAttachmentDeleteConfirmYes">
                    <div class="panel panel-primary"  style="font-weight:bold" >
                            <div class="panel-heading"  >
                    
                            <asp:Image ID="Image6" ImageUrl="~/Images/notice_40_black.png" CssClass="panel-popup-hd-img"
                                runat="server" />
                            <div class="panel-popup-hd-text">
                                Confirmation
                            </div>
                        </div>
                    </div>
                    <div class="panel-popup-content">
                        <div class="row">
                            <asp:TextBox ID="txtAttachmentDeleteConfirm" runat="server" CssClass="textbox-msg"
                                TextMode="MultiLine" ReadOnly="true" Text="Are you sure you want to remove this attachment?" />
                        </div>
                        <div class="row" style="float: right">
                            <asp:Button ID="btnAttachmentDeleteConfirmNo" Text="No" runat="server" CssClass="btn btn-warning"
                                OnClick="btnAttachmentDeleteConfirmNo_Click" />
                            <asp:Button ID="btnAttachmentDeleteConfirmYes" Text="Yes" runat="server" CssClass="btn btn-warning"
                                OnClick="btnAttachmentDeleteConfirmYes_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Button ID="btnAttachmentDeleteConfirmHidden" runat="server" CssClass="hide" />
                <atk:ModalPopupExtender ID="mpeAttachmentDeleteConfirm" BackgroundCssClass="panel-bg"
                    PopupControlID="pnlAttachmentDeleteConfirm" TargetControlID="btnAttachmentDeleteConfirmHidden"
                    runat="server" />

                <asp:Panel runat="server" Visible="<%# InvoiceDownloadVisibility %>">
                    <asp:Panel ID="pnlDownloadInvoice" runat="server" CssClass="panel panel-primary" Width="480px" Height="320px" DefaultButton="btnDownloadInvoiceCancel" ScrollBars="Auto">
                        <div class="panel panel-primary" style="width:460px;">
                            <div class="panel-heading"  style="height:60px;vertical-align:text-top;font-weight:bold">

                                <asp:Image ID="Image7" ImageUrl="~/Images/notice_40_blue.png" CssClass="panel-popup-hd-img"
                                    runat="server" />
                                <div class="panel-popup-hd-text">
                                    Invoices (PDF)
                                </div>
                                <div class="panel-popup-hd-close" style="display:none;">
                                    <asp:Button ID="btnDownloadInvoiceCancel" runat="server" CssClass="panel-popup-button-close" />

                                </div>

                                </div>
                        </div>
                        <div class="panel-popup-content">
                        <div class="row"  style="padding-left:10px;">

                                <asp:UpdatePanel runat="server" Height="320px">
                                    <ContentTemplate>
                                        <asp:GridView ID="grdInvoice" runat="server" AutoGenerateColumns="false"
                                            GridLines="None" ShowHeaderWhenEmpty="true" OnRowCommand="grdInvoice_RowCommand"
                                           cssClass="table table-striped"   
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header" ShowHeader="true" Width="435"   >
                                            <Columns>
                                                <asp:BoundField HeaderText="Type" DataField="InvoiceType" ItemStyle-Width="40px" ItemStyle-HorizontalAlign="Left" />
                                                <asp:BoundField HeaderText="INVOICE" DataField="SaleConfirmNo" ItemStyle-Width="260px" ItemStyle-HorizontalAlign="Left" />
                                                
                                                <asp:TemplateField ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ImageUrl="~/Images/download164.png" runat="server" CommandName="INV_DOWNLOAD" CommandArgument='<%# Eval("SaleConfirmNo") %>' Width="20px" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                       </ContentTemplate>
                                </asp:UpdatePanel>


                        </div>
                        <div  class="row" style="float: right">
                            
                                <asp:Button ID="btnInvCancel" Text="Close" runat="server" CssClass="btn btn-warning" />
                            </div>
                            </div>
                    </asp:Panel>
                    <atk:ModalPopupExtender ID="mpeDownloadInvoice" BackgroundCssClass="panel-bg" PopupControlID="pnlDownloadInvoice"
                        TargetControlID="btnDownloadInvoiceOpen" runat="server" CancelControlID="btnInvCancel" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
     <div class="panel panel-primary">
            <div class="panel-heading"  style="height:34px;vertical-align:text-top">
               <a data-toggle="collapse" href="#fildiv"  style="color:white">Order Information</a>
            </div>
         <div id="fildiv" class="panel-collapse collapse in">


    <div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16" style="">
        <div class="w3-bar">
            
            <div class="button-group">
                <asp:Panel runat="server" Visible='<%# !string.IsNullOrEmpty(Order.OrderNo) %>'>
                    <asp:Button ID="btnStatusHistory" Text="STATUS HISTORY" CssClass="btn btn-warning"
                        runat="server" />
                    <asp:Panel ID="pnlHistory" runat="server" CssClass="panel panel-danger" Width="1000px" DefaultButton="btnHistoryCancel"
                         >
                        <div class="panel panel-primary"  style="font-weight:bold" >
                            <div class="panel-heading"  >
                                <asp:Image ID="Image4" ImageUrl="~/Images/notice_40_black.png" CssClass="panel-popup-hd-img"
                                    runat="server" /> Order Status History
                                
                            </div>
                        </div>

                        <div class="panel-popup-content">
                            <div class="row">
                                <asp:Panel runat="server" ScrollBars="Vertical"  BackColor="white">
                                    <asp:GridView ID="grdOrderHistory" Width="100%" runat="server"
                                        AutoGenerateColumns="false" GridLines="None" ShowHeaderWhenEmpty="true"
                                        CssClass="table table-striped"   PagerStyle-CssClass="pager"
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="DOCUMENT NO." HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div class="grid-col-fixed w-10">
                                                        <%# Eval("OrderNo")%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="STATUS" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div class="grid-col-fixed w-17">
                                                        <%# Eval("OrderStatusText") %>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CHANGED BY" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%# Eval("ChangeBy")%>
                                                    <%--<div class="grid-col-fixed w-15">
                                                    <%# Eval("ChangeBy")%>
                                                </div>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CREATED ON" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div class="grid-col-fixed w-15">
                                                        <%# DateTimeFormat(Eval("ChangeTime"))%>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="REASON" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <div class="grid-col-fixed" style="width: 330px">
                                                        <asp:Label Text='<%# Eval("Reason")%>' ToolTip='<%# Eval("Reason")%>'
                                                            runat="server" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                            <div class="row" style="float: right">
                                <asp:Button ID="btnHistoryCancel" Text="Close" runat="server" CssClass="btn btn-warning" />
                            </div>
                        </div>
                    </asp:Panel>
                    <atk:ModalPopupExtender ID="mpeHistory" BackgroundCssClass="panel-bg" PopupControlID="pnlHistory"
                        TargetControlID="btnStatusHistory" CancelControlID="btnHistoryCancel" runat="server" />
                </asp:Panel>
            </div>

        </div>
        <%--   <div class="header-text">
            Order Information
        </div>--%>
<%--        <div id="fildiv2" class="panel-collapse collapse in">
<div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16 " style="margin-top: 0px" >--%>
        <asp:Panel ID="pnlHeader" runat="server" DefaultButton="btnHidden">
            <asp:Button ID="btnHidden" runat="server" CssClass="hide" />
            <div class="w3-col s12">
                <div class="control-label col-md-3">
                    Order Status
                </div>
                <div class="col-md-3">
                    <asp:DropDownList ID="ddlStatus" CssClass="w3-input" Enabled="false" DataValueField="OrderStatusCode"
                        DataTextField="OrderStatusText" runat="server">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="w3-col s12">
                <div class="control-label col-md-3">
                    Document No.
                </div>
                <div class=" col-md-3">
                    <asp:TextBox ID="txtOrderNo" runat="server" CssClass="w3-input" Enabled="false" />
                </div>
                <div class="control-label col-md-3">
                    Pricing Date
                </div>
                <div class=" col-md-3">
                    <asp:TextBox ID="txtPricingDate" runat="server" CssClass="w3-input" Enabled="false" />
                </div>
            </div>
            <div class="w3-col s12">
                <div class="control-label col-md-3">
                    Sales Confirm. No.
                </div>
                <div class=" col-md-3">
                    <asp:TextBox ID="txtSaleConfirmNo" runat="server" CssClass="w3-input" Enabled="false" />
                </div>
                <div class="control-label col-md-3">
                    Sales Confirm. Date
                </div>
                <div class=" col-md-3">
                    <asp:TextBox ID="txtSaleConfirmNoDate" runat="server" CssClass="w3-input" Enabled="false" />
                </div>
            </div>
            <div class="w3-col s12">
                <div class="control-label col-md-3">
                    Customer P/O
                </div>
                <div class=" col-md-3">
                    <asp:TextBox ID="txtPONo" runat="server" CssClass="w3-input" Enabled='<%# CanUserOrHMCEdit %>'
                        MaxLength="35" />
                </div>
                <div class="control-label col-md-3">
                    Customer P/O Date
                </div>
                <div class=" col-md-3">
                    <asp:TextBox ID="txtPODate" runat="server" CssClass="w3-input" Enabled='<%# CanUserOrHMCEdit %>'
                        MaxLength="10" />
                    <atk:CalendarExtender ID="cldPODate" Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                        TargetControlID="txtPODate" runat="server" />
                    <atk:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtPODate" Mask="99/99/9999"
                        MaskType="Date" runat="server" />
                </div>
            </div>
            <div class="w3-col s12">
                <div class="control-label col-md-3">
                    Payment Term <span style="color: red;">*</span>
                </div>
                <div class=" col-md-9">
                    <asp:DropDownList ID="ddlPaymentTerm" runat="server" CssClass="w3-input" DataValueField="PaymentTermCode"
                        DataTextField="PaymentTermDesc" Enabled='<%# CanUserOrHMCEdit %>' />
                    <asp:RequiredFieldValidator ControlToValidate="ddlPaymentTerm" ValidationGroup="order"
                        Display="Dynamic" SetFocusOnError="true" runat="server" />
                </div>
            </div>
            <asp:Panel ID="pnlFieldForHmc" Visible='<%# IsHMC %>' runat="server">
                <div class="w3-col s12">
                    <div class="control-label col-md-3">
                        Validity Date <span style="color: red;">*</span>
                    </div>
                    <div class=" col-md-3">
                        <asp:TextBox ID="txtValidityDate" runat="server" CssClass="w3-input" Enabled='<%# CanHMCEdit %>' />
                        <atk:CalendarExtender ID="cldValidityDate" Animated="true" FirstDayOfWeek="Monday"
                            Format="dd/MM/yyyy" TargetControlID="txtValidityDate" runat="server" />
                        <br />
                        <asp:RequiredFieldValidator ControlToValidate="txtValidityDate" ValidationGroup="order"
                            Display="Dynamic" SetFocusOnError="true" CssClass="label-r" runat="server" />
                    </div>
                    <div class="control-label col-md-3">
                        Customer Name
                    </div>
                    <div class=" col-md-3">
                        <asp:TextBox ID="txtCustomerName" runat="server" CssClass="w3-input" Enabled="false" />
                    </div>
                </div>
                <div class="w3-col s12">
                    <div class="control-label col-md-3">
                        Customer Group <span style="color: red;">*</span>
                    </div>
                    <div class=" col-md-3">
                        <asp:DropDownList ID="ddlCustomerGroup" runat="server" CssClass="w3-input" Enabled='<%# CanHMCEdit %>'
                            DataValueField="CustomerGroupCode" DataTextField="CustomerGroupText" />
                    </div>
                    <div class="control-label col-md-3">
                        Customer Code
                    </div>
                    <div class=" col-md-3">
                        <asp:TextBox ID="txtCustomerCode" runat="server" CssClass="w3-input" Enabled="false" />
                    </div>
                </div>
            </asp:Panel>
            <div class="w3-col s12">
                <div class="control-label col-md-3">
                    Ship To <span style="color: red;">*</span>
                </div>
                <div class=" col-md-9">
                    <asp:DropDownList ID="ddlShipTo" runat="server" CssClass="w3-input" DataValueField="ShipToCode"
                        Width="100%" DataTextField="ShipToText" Enabled='<%# CanUserOrHMCEdit %>' OnSelectedIndexChanged="ddlShipTo_SelectedIndexChanged"
                        AutoPostBack="true" />
                    <asp:RequiredFieldValidator ControlToValidate="ddlShipTo" ValidationGroup="order"
                        Display="Dynamic" SetFocusOnError="true" runat="server" />
                </div>
            </div>
            <div class="w3-col s12" style="height: 80px">
                <div class="control-label col-md-3">
                    Remarks
                </div>
                <div class=" col-md-9">
                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="w3-input"
                        Enabled='<%# CanUserOrHMCEdit %>' Width="98%" MaxLength="255" />
                </div>
                <div class="col-at1">
                    <img src="Images/space.png" />
                </div>
                <div class="col-8">
                    <span id="characterCount" class="label" style="font-size: 12px; margin: 0; padding: 0"></span>
                    <asp:RegularExpressionValidator ErrorMessage="Remark must not exceed 255 characters"
                        ControlToValidate="txtRemark" ValidationGroup="order" Display="Dynamic" SetFocusOnError="true"
                        CssClass="label-r" runat="server" ValidationExpression="^[\s\S]{0,255}$" />
                </div>
            </div>
        </asp:Panel>
    <%--</div>
            </div>--%>
    </div>
             </div>
         </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphContent2" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
              <div class="panel panel-primary">
            <div class="panel-heading"  style="height:34px;vertical-align:text-top">
               <a data-toggle="collapse" href="#prddiv"  style="color:white">Products</a>
            </div>
         <div id="prddiv" class="panel-collapse collapse in">
            <div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16" style="">
                <div class="w3-bar">
                    
                    <div class="button-group">
                        <asp:Button ID="btnProductDetailOpen" Text="+ Add Product" runat="server" CssClass="btn btn-warning"
                            OnClick="btnProductDetailOpen_Click" Visible='<%# CanUserEdit %>' />
                    </div>
                </div>
                <p>
                    <asp:GridView ID="grdOrderItem" Width="100%" runat="server" AutoGenerateColumns="false"
                        GridLines="None" ShowHeaderWhenEmpty="true" OnRowCommand="grdOrderItem_RowCommand"
                        OnRowDataBound="grdOrderItem_RowDataBound"
                        CssClass="table table-striped"   PagerStyle-CssClass="pager"
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header">
                        <Columns>
                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:ImageButton ID="btmProductDelete" ImageUrl="~/Images/delete_40.png" Height="20px"
                                        runat="server" CommandName="DEL" CommandArgument='<%# Eval("ItemNo") + ",DEL" %>'
                                        OnClick="btnConfirmOpen_Click" ToolTip="Delete line" />
                                    <asp:ImageButton ID="btnProductCopy" ImageUrl="~/Images/copy.png" Height="20px"
                                        runat="server" CommandName="COPY" CommandArgument='<%# Eval("ItemNo") %>' ToolTip="Copy this line" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Item no." DataField="ItemNo" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="Product/Grade" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnProductEdit" Text='<%# Eval("ProductText") %>' runat="server"
                                        CommandName="EDT" CommandArgument='<%# Eval("ItemNo") %>' Visible='<%# CanUserOrHMCEdit %>' />
                                    <asp:Label Text='<%# Eval("ProductText") %>' runat="server" Visible='<%# !CanUserOrHMCEdit %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Material code" DataField="MaterialNo" />

                            <asp:BoundField HeaderText="Package Size" DataField="PackageSizeText" />
                            <asp:BoundField HeaderText="Quantity/KG" DataField="Quantity"
                                ItemStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0}" />
                            <asp:BoundField HeaderText="Price/Unit" DataField="UnitPrice"
                                ItemStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0.00}" />
                            <asp:BoundField HeaderText="Amount" DataField="Amount"
                                ItemStyle-HorizontalAlign="Right" DataFormatString="{0:#,##0.00}" />
                            <asp:BoundField HeaderText="Delivery Date" DataField="DeliveryDate"
                                ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MM/yyyy}" />
                        </Columns>
                    </asp:GridView>
                    <div class="w3-row" style="margin-top: 30px">
                        <div class="w3-col s6">
                            <span style="color: #ffffff">.</span>
                        </div>
                        <div class="w3-col s6">
                            <div class="row">
                                <div class="col-4">
                                    Total Quantity
                                </div>
                                <div class="col-4 label-right">
                                    <asp:Label ID="Label1" Text='<%# string.Format("{0:#,##0.00}", Order.TotalQty) %>'
                                        runat="server" />
                                </div>
                                <div class="col-1">
                                    KG
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    Amount
                                </div>
                                <div class="col-4 label-right">
                                    <asp:Label ID="Label2" Text='<%# FormatHelper.TwoPoint(Order.Amount) %>' runat="server" />
                                </div>
                                <div class="col-1">
                                    THB
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <asp:Label ID="lblVat" runat="server" />
                                </div>
                                <div class="col-4 label-right">
                                    <asp:Label ID="Label3" Text='<%# FormatHelper.TwoPoint(Order.TotalTaxAmount) %>'
                                        runat="server" />
                                </div>
                                <div class="col-1">
                                    THB
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    Total Amount
                                </div>
                                <div class="col-4 label-right">
                                    <asp:Label ID="Label4" Text='<%# FormatHelper.TwoPoint(Order.TotalAmount) %>'
                                        runat="server" />
                                </div>
                                <div class="col-1">
                                    THB
                                </div>
                            </div>
                        </div>
                    </div>
                </p>
            </div>
</div></div>
            <asp:Panel ID="pnlProductDetail" runat="server" Width="500px" CssClass="panel-popup"
                DefaultButton="btnProductDetailSave">
                 <div class="panel panel-primary" >
                            <div class="panel-heading"  style="height:40px;vertical-align:text-top;font-weight:bold">
                        <asp:Image ImageUrl="~/Images/product_40_black.png" CssClass="panel-popup-hd-img"  Visible="false"
                            runat="server" />
                        <div class="panel-popup-hd-text">
                            <asp:Label ID="lblProductHeader" runat="server" />
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="control-label col-md-4">
                            Product <span style="color: red;">*</span>
                        </div>
                        <div class="col-6">
                            <asp:DropDownList ID="ddlProduct" runat="server" CssClass="w3-input" DataValueField="ProductNo"
                                DataTextField="ProductText" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged"
                                Enabled='<%# CanUserOrHMCEdit %>'>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ddlProduct" ValidationGroup="product"
                                Display="Dynamic" SetFocusOnError="true" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="control-label col-md-4">
                            Package Size <span style="color: red;">*</span>
                        </div>
                        <div class="col-6">
                            <asp:DropDownList ID="ddlPackageSize" runat="server" CssClass="w3-input" AutoPostBack="true"
                                Enabled='<%# CanUserOrHMCEdit %>' DataValueField="PackageSizeCode" DataTextField="PackageSizeText"
                                OnSelectedIndexChanged="ddlPackageSize_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ddlPackageSize" ValidationGroup="product"
                                Display="Dynamic" SetFocusOnError="true" runat="server" />
                        </div>
                    </div>
                    <asp:Panel ID="Panel1" runat="server" Visible='<%# IsHMC %>'>
                        <div class="row">
                            <div class="control-label col-md-4">
                                Material Code <span style="color: red;">*</span>
                            </div>
                            <div class="col-6">
                                <asp:DropDownList ID="ddlMaterialNo" runat="server" CssClass="w3-input" DataValueField="MaterialNo"
                                    DataTextField="MaterialNo" AutoPostBack="false" Enabled='<%# CanHMCEdit %>' />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlMaterialNo"
                                    ValidationGroup="product" Display="Dynamic" SetFocusOnError="true" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row">
                        <div class="control-label col-md-4">
                            Quantity/KG <span style="color: red;">*</span>
                        </div>
                        <div class="col-2">
                            <asp:TextBox ID="txtProductQty" runat="server" CssClass="w3-input" MaxLength="7" />
                            <atk:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" FilterType="Numbers" TargetControlID="txtProductQty"
                                runat="server" />
                            <br />
                            <asp:RequiredFieldValidator ControlToValidate="txtProductQty" ValidationGroup="product"
                                Display="Dynamic" SetFocusOnError="true" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="control-label col-md-4">
                            Price/Unit <span style="color: red;">*</span>
                        </div>
                        <div class="col-2">
                            <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="w3-input" MaxLength="10" />
                            <atk:FilteredTextBoxExtender FilterType="Numbers, Custom" ValidChars="." TargetControlID="txtUnitPrice"
                                runat="server" />
                            <asp:RequiredFieldValidator ControlToValidate="txtUnitPrice" ValidationGroup="product"
                                Display="Dynamic" SetFocusOnError="true" runat="server" />
                        </div>
                    </div>


                    <div class="row">
                        <div class="control-label col-md-4">
                            Delivery date
                        </div>
                        <div class="col-2">
                            <asp:TextBox ID="txtProductDlvDate" runat="server" CssClass="w3-input" MaxLength="10" />
                            <atk:MaskedEditExtender ID="MaskedEditExtender2" CultureName="en-US" runat="server" Mask="99/99/9999"  
                                MaskType="Date" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" 
                                OnInvalidCssClass="MaskedEditError" TargetControlID="txtProductDlvDate">
                            </atk:MaskedEditExtender>
                            <atk:CalendarExtender TargetControlID="txtProductDlvDate" runat="server" Format="dd/MM/yyyy" OnClientDateSelectionChanged="checkDate"  
                                 ID="txtProductDlvDate_cal" 
                       BehaviorID = "txtProductDlvDate_cal"
                                
                                ></atk:CalendarExtender>
                        </div>
                    </div>

                    <div class="row">
                        <asp:Label ID="lblProductMsg" CssClass="label-r" runat="server" />
                    </div>
                    <div class="row" style="float: right">
                        <asp:Button ID="btnProductDetailClose" Text="CANCEL" runat="server" CssClass="btn btn-warning" />
                        <asp:Button ID="btnProductDetailSave" Text="ADD" runat="server" CssClass="btn btn-warning"
                            ValidationGroup="product" OnClick="btnProductDetailSave_Click" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnProductHidden" runat="server" CssClass="hide" />
            <atk:ModalPopupExtender ID="mpeProduct" BackgroundCssClass="panel-bg" PopupControlID="pnlProductDetail"
                TargetControlID="btnProductHidden" CancelControlID="btnProductDetailClose" runat="server" />
            <asp:Panel ID="pnlMsg" runat="server" CssClass="panel-popup" Width="600px" DefaultButton="btnMsgCancel">
                                                  <div class="panel panel-primary">
            <div class="panel-heading"  style="height:40px;vertical-align:text-top">
               <a data-toggle="collapse"   style="color:white">Message</a>
            </div>
</div>
                <div class="panel-popup-content">
                    <div class="row">
                        <asp:TextBox ID="txtMsg" runat="server" CssClass="textbox-msg" TextMode="MultiLine"
                            ReadOnly="true" />
                    </div>
                    <div class="row" style="float: right">
                        <asp:Button ID="btnMsgCancel" Text="Close" runat="server" CssClass="button-img-s img-reject-white"
                            OnClick="btnMsgCancel_Click" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Button ID="btnMsgHidden" runat="server" CssClass="hide" />
            <atk:ModalPopupExtender ID="mpeMsg" BackgroundCssClass="panel-bg" PopupControlID="pnlMsg"
                TargetControlID="btnMsgHidden" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div class="loading-bg">
                <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="~/Images/loading.gif"
                    AlternateText="Loading ..." ToolTip="Loading ..." CssClass="loading-img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <style>a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */ </style>
    <script type="text/javascript">
        function checkDate(sender, args) {
            var toDate= new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if (sender._selectedDate < toDate) {
                alert("You can't select day earlier than today!");
                    $find("txtProductDlvDate_cal").set_selectedDate(null);
        $("[id*=txtProductDlvDate_cal]").val("");
        $(".ajax__calendar_active").removeClass("ajax__calendar_active");
                return false;


             //   $('#<%= txtProductDlvDate.ClientID %>').val('');
             //   return;
             //   sender._selectedDate = toDate;
             //   sender._textbox.set_Value(toDate);
                //set the date back to the current date
                
            } else {
              
            }
        }
    </script>
</asp:Content>
