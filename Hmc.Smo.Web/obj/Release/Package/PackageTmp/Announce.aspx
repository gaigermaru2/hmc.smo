﻿<%@ Page Title="Announce : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Announce.aspx.cs" Inherits="Hmc.Smo.Web.Announce"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ContentPlaceHolderID="cphPageName" runat="server">

    <div class="panel-heading" style="font-size: 20px; font-weight: bold;">
        <a href="#" onclick="Toggle()" style="color: #f0ad4e">☰</a>  Announce
                    <div style="float: right; z-index: 10; right: 0; vertical-align: middle"><%=CurrentUser.FullName%></div>

    </div>
    <style>
        a:link {
            color: #337ab7;
        }
        /* unvisited link  */
        a:visited {
            color: #337ab7;
        }
        /* visited link    */
        a:hover {
            color: #337ab7;
        }
        /* mouse over link */
        a:active {
            color: #337ab7;
        }
        /* selected link   */
        th {
            text-align: center;
        }
    </style>


</asp:Content>
<asp:Content ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNewAnno" Text="ANNOUNCEMENT" runat="server" CssClass="btn btn-warning"
                ValidationGroup="order" OnClick="btnNewAnnounce_Click" />

            <asp:Button ID="btnAttachmentHidden" runat="server" CssClass="hide" />
            <asp:Button ID="btnAttachmentHidden2" runat="server" CssClass="hide" />


            <atk:ModalPopupExtender ID="mpeAdd" BackgroundCssClass="panel-bg" PopupControlID="pnlAttachment"
                TargetControlID="btnAttachmentHidden" CancelControlID="btnAttachmentCancel" runat="server" />


         

            <atk:ModalPopupExtender ID="mpeUpdate" BackgroundCssClass="panel-bg" PopupControlID="pnlUpdate"
                TargetControlID="btnAttachmentHidden2" CancelControlID="btnAttachmentCancel2" runat="server" />



            <asp:Panel ID="pnlAttachment" runat="server" CssClass="panel-popup" Width="600px"
                DefaultButton="btnAttachmentCancel">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="height: 40px; vertical-align: text-top">
                        <a data-toggle="collapse" style="color: white">ANNOUNCEMENT</a>
                    </div>
                </div>

                <div class="panel-popup-content">
                    <asp:Panel ID="pnlAttachmentAdd" runat="server">

                        <div class="row">
                            <div class="col-md-4">
                                Type
                            </div>
                            <div class=" col-md-6">
                                <asp:DropDownList ID="ddlType" runat="server" CssClass="w3-input" DataTextField="Name" DataValueField="Code" AutoPostBack="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Text
                            </div>
                            <div class=" col-md-6">
                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="w3-input" HtmlEncode="false"
                                    Width="98%" MaxLength="200000" Height="200" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Valid From <span style="color: red;">*</span>
                            </div>
                            <div class="col-6">
                                <asp:TextBox ID="txtFrom" runat="server" CssClass="w3-input"
                                    MaxLength="10" />
                                <atk:CalendarExtender ID="cldFrom" Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="txtFrom" runat="server" />
                                <atk:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtFrom" Mask="99/99/9999"
                                    MaskType="Date" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Valid To <span style="color: red;">*</span>
                            </div>
                            <div class="col-6">
                                <asp:TextBox ID="txtTo" runat="server" CssClass="w3-input"
                                    MaxLength="10" />
                                <atk:CalendarExtender ID="cldTo" Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="txtTo" runat="server" />
                                <atk:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtTo" Mask="99/99/9999"
                                    MaskType="Date" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row">
                    </div>
                    <div class="row" style="float: right">
                        <asp:Button ID="btnAttachmentOK" runat="server" CssClass="button-img-s img-add-w"
                            OnClick="btnCreateAnnounce_Click" Text="Create" />
                        <asp:Button ID="btnAttachmentCancel" runat="server" CssClass="btn btn-warning"
                            Text="Close" />
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlUpdate" runat="server" CssClass="panel-popup" Width="600px"
                DefaultButton="btnAttachmentCancel2">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="height: 40px; vertical-align: text-top">
                        <a data-toggle="collapse" style="color: white">ANNOUNCEMENT</a>
                    </div>
                </div>

                <div class="panel-popup-content">
                    <asp:Panel ID="Panel2" runat="server">
                        <asp:HiddenField ID="annoID" runat="server" />
                        <div class="row">
                            <div class="col-md-4">
                                Type
                            </div>
                            <div class=" col-md-6">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="w3-input" DataTextField="Name" DataValueField="Code" AutoPostBack="false">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Text
                            </div>
                            <div class=" col-md-6">
                                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" CssClass="w3-input" HtmlEncode="false"
                                    Width="98%" MaxLength="200000" Height="200" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Valid From <span style="color: red;">*</span>
                            </div>
                            <div class="col-6">
                                <asp:TextBox ID="TextBox2" runat="server" CssClass="w3-input"
                                    MaxLength="10" />
                                <atk:CalendarExtender ID="CalendarExtender1" Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="TextBox2" runat="server" />
                                <atk:MaskedEditExtender ID="MaskedEditExtender3" TargetControlID="TextBox2" Mask="99/99/9999"
                                    MaskType="Date" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                Valid To <span style="color: red;">*</span>
                            </div>
                            <div class="col-6">
                                <asp:TextBox ID="TextBox3" runat="server" CssClass="w3-input"
                                    MaxLength="10" />
                                <atk:CalendarExtender ID="CalendarExtender2" Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="TextBox3" runat="server" />
                                <atk:MaskedEditExtender ID="MaskedEditExtender4" TargetControlID="TextBox3" Mask="99/99/9999"
                                    MaskType="Date" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="row">
                    </div>
                    <div class="row" style="float: right">
                        <asp:Button ID="Button1" runat="server" CssClass="button-img-s img-add-w"
                            OnClick="btnUpdateAnnounce_Click" Text="Update" />
                        <asp:Button ID="btnAttachmentCancel2" runat="server" CssClass="btn btn-warning"
                            Text="Close" />
                    </div>
                </div>
            </asp:Panel>
            <div class="panel panel-primary">
                <div class="panel-heading" style="height: 36px; vertical-align: text-top">
                    <a data-toggle="collapse" href="#prddiv" style="color: white">Results</a>
                </div>
                <div id="prddiv" class="panel-collapse collapse in" style="overflow-x: auto;">
                    <div class="panel-body">
                        <asp:GridView ID="grd" runat="server" AutoGenerateColumns="false" GridLines="None"
                            ShowHeaderWhenEmpty="true" AllowSorting="true" AllowPaging="True"
                            PageSize="10"
                            DataSource='<%# AnnouncesDT %>'
                            CssClass="table table-striped" PagerStyle-CssClass="pager"
                            RowStyle-CssClass="odd gradeX" HeaderStyle-CssClass="header" Width="100%"
                            OnRowCommand="grd_RowCommand">
                            <Columns>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="No">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="announce_text" HeaderText="Text" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="250" HtmlEncode="false" />
                                <asp:BoundField DataField="type" HeaderText="Announce Type" SortExpression="type" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Valid From" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="valid_from">
                                    <ItemTemplate>
                                        <%# FormatHelper.DateToStr(Eval("valid_from")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Valid To" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="valid_to">
                                    <ItemTemplate>
                                        <%# FormatHelper.DateToStr(Eval("valid_to")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField >
    <ItemTemplate><asp:Button ID="pop" runat="server" Text="Edit" CommandName="pop"  CommandArgument='<%# Eval("announce_id") %>' OnClick="pop_Click"/></ItemTemplate>
    </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <script type="text/javascript" src="Scripts/jquery.min.js"></script>
                <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
                <script type="text/javascript" src="Scripts/gridviewScroll.min.js"></script>
                <script type="text/javascript"> 
                    function pageLoad() {
                        $('#<%=grd.ClientID%>').gridviewScroll({
                            width: 100 %,
                            height: 700,
                            arrowsize: 30,
                            varrowtopimg: "Images/arrowvt.png",
                            varrowbottomimg: "Images/arrowvb.png",
                            harrowleftimg: "Images/arrowhl.png",
                            harrowrightimg: "Images/arrowhr.png"
                        });
                    }
                </script>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
