﻿<%@ Page Title="Orders : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="OrderList.aspx.cs" Inherits="Hmc.Smo.Web.OrderList"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageName" runat="server">
         <style>
  .trimmer{
   overflow: hidden;
   }


a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */

th,td{
     /*text-align: left;
      white-space: nowrap;*/
     white-space: nowrap;
}


         </style>
 <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e;cursor:pointer">☰</a>  Orders<div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 7px;background-color:transparent"> 
        <asp:Button ID="btnUploadExcel" Text="Upload" runat="server" CssClass="btn btn-warning"
            PostBackUrl="~/OrderUploadExcel.aspx" Visible='false' />
        <asp:Button ID="btnExportExcel" Text="Export" runat="server" CssClass="btn btn-warning"
            Visible="true" OnClick="btnExportExcel_Click" />
        <asp:Button ID="btnNewOrder" Text="New Order" PostBackUrl="~/OrderDetail.aspx" runat="server"
            CssClass="btn btn-warning" Visible='<%# IsUser %>' />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphCriteria" runat="server">
     <div class="panel panel-primary">
            <div class="panel-heading"  style="height:36px;vertical-align:text-top">
               <a data-toggle="collapse" href="#fildiv" style="color:white">Criteria</a>
            </div>
         <div id="fildiv" class="panel-collapse collapse in">
<div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16 " style="margin-top: 0px" >
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                <asp:Panel ID="pnlSearhFieldForHMC" runat="server" Visible='<%# IsHMC %>'>
                    <div class="w3-col s6">
                        <div class="control-label col-md-5">
                            Customer Code
                        </div>
                        <div class=" col-md-7">
                            <asp:TextBox ID="txtCustCodeFrom" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged" />
                        </div>
                    </div>
                    <div class="w3-col s6">
                        <div class="control-label col-md-5">
                            Customer Name
                        </div>
                        <div class=" col-md-7">
                            <asp:TextBox ID="txtCustName" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged" />
                        </div>
                    </div>
                    <div class="w3-col s6">
                        <div class="control-label col-md-5">
                            Sales Segment Manager
                        </div>
                        <div class=" col-md-7">
                            <asp:DropDownList ID="ddlSaleSegMgrFrom" runat="server" CssClass="w3-input" DataValueField="SegmentManagerCode"
                                DataTextField="SegmentManagerName" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="w3-col s6">
                        <div class="control-label col-md-5">
                            To
                        </div>
                        <div class=" col-md-7">
                            <asp:DropDownList ID="ddlSaleSegMgrTo" runat="server" CssClass="w3-input" DataValueField="SegmentManagerCode"
                                DataTextField="SegmentManagerName" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </asp:Panel>
                <div class="w3-col s6">
                    <div class="control-label col-md-5">
                        Sales Confirmation No.
                    </div>
                    <div class=" col-md-7">
                        <asp:TextBox ID="txtSaleConfirmNoFrom" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged"
                            MaxLength="8" />
                    </div>
                </div>
                <div class="w3-col s6">
                    <div class="control-label col-md-5">
                        To
                    </div>
                    <div class=" col-md-7">
                        <asp:TextBox ID="txtSaleConfirmNoTo" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged"
                            MaxLength="8" />
                    </div>
                </div>
            </asp:Panel>
            <div class="w3-col s6">
                <div class="control-label col-md-5">
                    Created Date
                </div>
                <div class=" col-md-7">
                    <asp:TextBox ID="txtCDateFrom" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged" />
                    <atk:CalendarExtender ID="cldPODate" Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                        TargetControlID="txtCDateFrom" runat="server" />
                    <atk:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txtCDateFrom" Mask="99/99/9999"
                        MaskType="Date" runat="server" />
                </div>
            </div>
            <div class="w3-col s6">
                <div class="control-label col-md-5">
                    To
                </div>
                <div class=" col-md-7">
                    <asp:TextBox ID="txtCDateTo" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged" />
                    <atk:CalendarExtender ID="CalendarExtender1" Animated="true" FirstDayOfWeek="Monday"
                        Format="dd/MM/yyyy" TargetControlID="txtCDateTo" runat="server" />
                    <atk:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtCDateTo" Mask="99/99/9999"
                        MaskType="Date" runat="server" />
                </div>
            </div>

            <div class="w3-col s6">
                <div class="control-label col-md-5">
                    Product/Grade
                </div>
                <div class=" col-md-7">
                    <asp:TextBox ID="txtProduct" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged"
                        MaxLength="50" />
                </div>
            </div>
            <div class="w3-col s6">
                <div class="control-label col-md-5">
                    Customer P/O
                </div>
                <div class=" col-md-7">
                    <asp:TextBox ID="txtPONo" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged"
                        MaxLength="20" />
                </div>
            </div>
            <div class="w3-col s6">
                <div class="control-label col-md-5">
                    Document No.
                </div>
                <div class=" col-md-7">
                    <asp:TextBox ID="txtOrderNo" runat="server" CssClass="w3-input" OnTextChanged="TextBox_TextChanged"
                        MaxLength="10" />
                </div>
            </div>
            <div class="w3-col s6">
                <div class="control-label col-md-5">
                    Status
                </div>
                <div class=" col-md-7">
                    <div class="my-ddl">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="w3-input" DataValueField="OrderStatusCode"
                            DataTextField="OrderStatusText" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                            AutoPostBack="true" />
                    </div>
                </div>
            </div>
</div>
             </div>
         <div class="panel-footer">
               <asp:Button Text="Search" runat="server" CssClass="button-img-s img-search" ID="btnSearch"
                OnClick="btnSearch_Click" />
            </div>
        
         </div>
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="" style="min-height: 500px">
        <div class="w3-bar">
            <asp:Button ID="btnApproveMulti" Text="APPROVE" runat="server" CssClass="w3-button w3-blue w3-tiny w3-round w3-right"
                Visible='<%# IsManager %>' OnClick="btnApproveMulti_Click" />
        </div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <script type="text/javascript">    
                    function toggleCheckBoxes(elem) {
                        var div = document.getElementById('<% = grdOrder.ClientID %>');
                        var chk = div.getElementsByTagName('input');
                        var len = chk.length;
                        for (var i = 0; i < len; i++) {
                            if (chk[i].type === 'checkbox') {
                                chk[i].checked = elem.checked;
                            }
                        }
                    }
                </script>

           <div class="panel panel-primary">
            <div class="panel-heading"  style="height:36px;vertical-align:top">
               <a data-toggle="collapse" style="color:white;width:100%"  href="#resdiv" aria-expanded="true" aria-controls="resdiv" >Results</a>
            </div>           
        <!-- /.panel-heading -->
            <div id="resdiv" class="panel-collapse collapse in"  style="overflow:auto">
        <div class="panel-body">
                <asp:GridView ID="grdOrder" runat="server" AutoGenerateColumns="false" GridLines="None"
                    ShowHeaderWhenEmpty="true" AllowSorting="true" OnSorting="grdOrder_Sorting" AllowPaging="true"
                    PageSize="10" OnPageIndexChanging="grdOrder_PageIndexChanging" OnRowDataBound="grdOrder_RowDataBound"
                    CssClass="table table-striped"   PagerStyle-CssClass="pager"  HeaderStyle-CssClass="header"
                        >
                    <Columns>

                        <asp:TemplateField ItemStyle-Width="25px" ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <input type="checkbox" id="chkAll" onclick="toggleCheckBoxes(this)" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelect" ToolTip='<%# Eval("OrderNo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Inv." ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                               <asp:HyperLink NavigateUrl='<%# "ShipmentDetailList.aspx?OrderNo=" + Eval("OrderNo") + "&InvoiceType=INV"%>'
                                    Text='' runat="server" >
                                <asp:Image ImageUrl="~/Images/ping.png" runat="server" Visible='<%# ((int)Eval("InvoiceCount")) > 0 %>' />
                                   </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CN" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink NavigateUrl='<%# "ShipmentDetailList.aspx?OrderNo=" + Eval("OrderNo") + "&InvoiceType=CN"%>'
                                    Text='' runat="server" >
                                <asp:Image ImageUrl="~/Images/pin.png" runat="server" Visible='<%# ((int)Eval("OtherCount")) > 0 %>' />
                                   </asp:HyperLink>
                                <%--<asp:Image ImageUrl="~/Images/pin.png" runat="server" Visible='<%# ((int)Eval("OtherCount")) > 0 %>' />--%>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Doc no." ItemStyle-HorizontalAlign="Center" SortExpression="OrderNo" ItemStyle-Width="85px" ItemStyle-CssClass="">
                            <ItemTemplate>
                                <asp:HyperLink NavigateUrl='<%# "OrderDetail.aspx?OrderNo=" + Eval("OrderNo") %>'
                                    Text='<%# Eval("OrderNo") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Center" SortExpression="CustomerNo">
                            <ItemTemplate>
                                <asp:Label Text='<%# Eval("CustomerNo") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Center" SortExpression="CustomerName"   ItemStyle-Width="110px"  ItemStyle-CssClass="trimmer">
                            <ItemTemplate>
                               <%-- <asp:Label Text='<%# ProcessMyDataItem(Eval("CustomerName"),10)+".." %>' runat="server" Style=""  ToolTip='<%# Eval("CustomerName") %>'/>--%>
                                 <asp:Label Text='<%#Eval("CustomerName")%>' runat="server" Style=""  ToolTip='<%# Eval("CustomerName") %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Customer P/O" HeaderStyle-HorizontalAlign="Left" SortExpression="PONo"   ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label Text='<%# FormatString(Eval("PONo"), 20) %>' runat="server" ToolTip='<%# Eval("PONo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sales confirm" HeaderStyle-HorizontalAlign="Center" SortExpression="SaleConfirmNo"  ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label Text='<%# FormatString(Eval("SaleConfirmNo"), 10) %>' runat="server"
                                    ToolTip='<%# Eval("SaleConfirmNo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Product/Grade" HeaderStyle-HorizontalAlign="Center" SortExpression="ProductText"    ItemStyle-CssClass="trimmer">
                            <ItemTemplate>
                                <%--<asp:Label Text='<%# ProcessMyDataItem(Eval("ProductText"),10)+".." %>' runat="server" Style=""  ToolTip='<%# Eval("ProductText") %>'/>--%>
                                <asp:Label Text='<%#Eval("ProductText")%>' runat="server" Style=""  ToolTip='<%# Eval("ProductText") %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Package" HeaderStyle-HorizontalAlign="Center" SortExpression="PackageSizeText"    ItemStyle-HorizontalAlign="Left"   ItemStyle-CssClass="trimmer">
                            <ItemTemplate>
                                <asp:Label Text='<%#Eval("PackageSizeText")%>' runat="server" Style=""  ToolTip='<%# Eval("PackageSizeText") %>'/>
                                <%--<asp:Label Text='<%# ProcessMyDataItem(Eval("PackageSizeText"),6)+".." %>' runat="server" Style=""  ToolTip='<%# Eval("PackageSizeText") %>'/>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Quantity/kg" DataField="Quantity" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                            DataFormatString="{0:#,##0}"  SortExpression="Quantity" />
                        <asp:BoundField HeaderText="Unit price" DataField="UnitPrice" DataFormatString="{0:#,##0.00}"
                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"    SortExpression="UnitPrice" />
                        <asp:TemplateField HeaderText="Status"  ControlStyle-CssClass="w3-left"  HeaderStyle-CssClass="w3-center"
                            SortExpression="OrderStatusText">
                            <ItemTemplate>
                                <asp:Label Text='<%#Eval("OrderStatusText")%>' runat="server" Style=""  ToolTip='<%# Eval("OrderStatusText") %>'  />
                                <%--<asp:Label Text='<%# ProcessMyDataItem(Eval("OrderStatusText"),5)+".."%>' runat="server"   ToolTip='<%# Eval("OrderStatusText") %>'/>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Created on" DataField="OrderDate" DataFormatString="{0:dd/MM/yyyy HH:mm}"
                            ControlStyle-CssClass="w3-left"  HeaderStyle-CssClass="w3-center" SortExpression="OrderDate" />
                    </Columns>
                </asp:GridView>
                </div>
            </div>
               </div>
                       

                <script type="text/javascript" src="Scripts/jquery.min.js"></script>
                <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
                <script type="text/javascript" src="Scripts/gridviewScroll.min.js"></script>
                <script type="text/javascript"> 
                    function pageLoad() {
                        $('#<%=grdOrder.ClientID%>').gridviewScroll({
                            width: 100%,
                            height: 650,
                            arrowsize: 30,
                            varrowtopimg: "Images/arrowvt.png",
                            varrowbottomimg: "Images/arrowvb.png",
                            harrowleftimg: "Images/arrowhl.png",
                            harrowrightimg: "Images/arrowhr.png"
                        });
                    }
                     $('.panel-collapse').on('show.bs.collapse', function () {
    $(this).siblings('.panel-heading').addClass('active');
  });

  $('.panel-collapse').on('hide.bs.collapse', function () {
    $(this).siblings('.panel-heading').removeClass('active');
  });
                </script>
               
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
