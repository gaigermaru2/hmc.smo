﻿<%@ Page Title="Orders : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="Report3.aspx.cs" Inherits="Hmc.Smo.Web.Report3"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphPageName" runat="server">
    
                    <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e;cursor:pointer">☰</a> Sales Order by Price Category<div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>
    </div>   
             <style>a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */ </style>
      <link type="text/css" href="assets/css/argon.css?v=1.0.0" rel="stylesheet">
    <script>
        $(document).ready(function () {
            $(".sub-menu li").removeClass("active");
            $('#nReport3').addClass('active');
            $('#mDashboard').attr("aria-expanded", true); 

        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <div style="padding-top: 7px">
       
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cphCriteria" runat="server">
 
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">

    <div class="row">
             <%--<div class="col">
                  <ul class="nav nav-pills justify-content-end" style="color:white">
                    
                    <a href="#" class="btn btn-warning" style="color:white" >Month</a> 
                      <a href="#" class="btn btn-warning" style="color:white">Quarter</a> 
                      <a href="#"  onclick="downloadImage();" class="btn btn-warning" style="color:white">Save as image</a> 
                      <br>
                     

                    </li>
                     
                  </ul>
                </div>--%>
         <div class="col-lg-12">
          <div class="row"></div>
          
               <div class="panel panel-primary">
                 <div class="panel-heading"  style="height:30px;vertical-align:top;font-weight:bold">
                <a data-toggle="collapse" href="#fildiv" style="color:white">Sales history  2019</a> 
                    </div>
                <div class="card shadow" style="height:500px;">
           
            <div class="card-body">
              <!-- Chart -->
              <div class="chart" >
                  <canvas id="chart-0" class="chart-canvas" style="height:450px;"></canvas>
                
              </div>
            </div>
          </div>
              </div>
            
        </div>
    </div>

        
        <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>

  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.0.0"></script>
    	<script src="chart/chart.min.js"></script>
	<script src="chart/utils.js"></script>
    <style>
          element.style {
    height: 200px;
    display: block;
    width: 328px;
}
    </style>
  		<script>
		var DATA_COUNT = 16;
		var MIN_XY = 0;
		var MAX_XY = 200;

		var utils = Samples.utils;

		utils.srand(110);
              var ict_unit = [];
         var efficiency = [];
              var coloR = [];

         var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ",0.5)";
         };

         for (var i in data) {
            ict_unit.push("ICT Unit " + data[i].ict_unit);
            efficiency.push(data[i].efficiency);
            coloR.push(dynamicColors());
              }



		function colorize(opaque, context) {
			var value = context.dataset.data[context.dataIndex];
			//var x = value.x / 100;
            //var y = value.y / 100;
            var x = value.x ;
			var y = value.y ;
			var r = x < 0 && y < 0 ? 250 : x < 0 ? 150 : y < 0 ? 50 : 0;
			var g = x < 0 && y < 0 ? 0 : x < 0 ? 50 : y < 0 ? 150 : 250;
			var b = x < 0 && y < 0 ? 0 : x > 0 && y > 0 ? 250 : 150;
			var a = opaque ? 1 : 0.5 * value.v / 1000;

			return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
		}

		function generateData() {
			var data = [];
			var i;

			for (i = 0; i < DATA_COUNT; ++i) {
				data.push({
					x: utils.rand(MIN_XY, MAX_XY),
					y: utils.rand(MIN_XY, MAX_XY),
                    v: utils.rand(0, 1000),

				});
			}

			return data;
		}

              var data = {
            labels: ['XX'],
			datasets: [{
				data: generateData()
			}, {
				data: generateData()
			}]
		};

		var options = {
			aspectRatio: 1,
            legend: false,
              scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Total (T)'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Frequency'
						}
					}]
				},
			tooltips: {
            callbacks: {
                label: function (t, d) {
                   // var xx = t;
                    var rLabel = d.datasets[t.datasetIndex].data[t.index].r;
                    return d.datasets[t.datasetIndex].label + ': (Total:' + t.xLabel + ' Ton, Freq:' + t.yLabel + ', Value:' + rLabel + ' Baht)';
                }
              
         }
      },

			elements: {
				point: {
					backgroundColor: colorize.bind(null, false),

					borderColor: colorize.bind(null, true),

					borderWidth: function(context) {
						return Math.min(Math.max(1, context.datasetIndex + 1), 8);
					},

					hoverBackgroundColor: 'transparent',

					hoverBorderColor: function(context) {
						return utils.color(context.datasetIndex);
					},

					hoverBorderWidth: function(context) {
						var value = context.dataset.data[context.dataIndex];
						return Math.round(8 * value.v / 1000);
					},

					radius: function(context) {
						var value = context.dataset.data[context.dataIndex];
						var size = context.chart.width;
						var base = Math.abs(value.v) / 1000;
						return (size / 24) * base;
					}
				}
			}
		};

		var chart = new Chart('chart-0', {
			type: 'bubble',
			 data: {
    labels: "Customer1",
    datasets: [{
      label: ["Customer1"],
      backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle1",//adding the title you want to show
      data: [{
        x: 99,
        y: 5,
        r: 90
      }]
    }, {
      label: ["Customer2"],
      backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle2",
      data: [{
        x: 20,
        y: 20,
        r: 10
      }]
        }
        , {
      label: ["Customer3"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle3",//adding the title you want to show
      data: [{
        x: 150,
        y: 5,
        r: 20
      }]
        },
        {
      label: ["Customer4"],
backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 78,
        y: 4,
        r: 25
      }]
        }
        , {
      label: ["Customer5"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 133,
        y: 25,
        r: 15
      }]
        },
        {
      label: ["Customer6"],
      backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 33,
        y: 5,
        r: 15
      }]
        },
        {
      label: ["Customer7"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 55,
        y: 3,
        r: 30
      }]
        },
        {
      label: ["Customer9"],
      backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 45,
        y: 12,
        r: 15
      }]
        },
        {
      label: ["Customer8"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 32,
        y:6,
        r: 15
      }]
        },
        {
      label: ["Customer10"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 50,
        y: 4,
        r: 30
      }]
        },
        {
      label: ["Customer11"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 177,
        y: 20,
        r: 15
      }]
        },
        {
      label: ["Customer12"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 33,
        y: 4,
        r: 25
      }]
        },
        {
      label: ["Customer8"],
     backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 66,
        y: 15,
        r: 15
      }]
        },
        {
      label: ["Customer10"],
      backgroundColor: dynamicColors(),
      borderColor: dynamicColors(),
      title: "dataTitle4",//adding the title you want to show
      data: [{
        x: 99,
        y: 4,
        r: 15
      }]
        }
    ]
  },
			options: options
		});

		// eslint-disable-next-line no-unused-vars
		function randomize() {
			chart.data.datasets.forEach(function(dataset) {
				dataset.data = generateData();
			});
			chart.update();
		}

		// eslint-disable-next-line no-unused-vars
		function addDataset() {
			chart.data.datasets.push({
				data: generateData()
			});
			chart.update();
		}

		// eslint-disable-next-line no-unused-vars
		function removeDataset() {
			chart.data.datasets.shift();
			chart.update();
              }

	</script>

</asp:Content>
