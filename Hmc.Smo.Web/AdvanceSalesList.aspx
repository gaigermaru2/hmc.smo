﻿<%@ Page Title="Advance Sales : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="AdvanceSalesList.aspx.cs" Inherits="Hmc.Smo.Web.AdvanceSalesList"
    MaintainScrollPositionOnPostback="true" %>
<%@ Register src="MultiCheckCombo.ascx" tagname="MultiCheckCombo" tagprefix="uc1" %>
<%@ Register src="MultiCheckComboShipto.ascx" tagname="MultiCheckComboShipto" tagprefix="uc2" %>
<asp:Content ContentPlaceHolderID="cphPageName" runat="server">
    
                <div class="panel-heading" style="font-size:20px;font-weight:bold;">
    <a href="#"  onclick="Toggle()" style="color:#f0ad4e">☰</a>  Advance Sales
                    <div style="float: right;z-index: 10;  right: 0;vertical-align:middle"><%=CurrentUser.FullName%></div>

    </div>  
             <style>a:link    {color:#337ab7;}  /* unvisited link  */
a:visited {color:#337ab7;}  /* visited link    */
a:hover   {color:#337ab7;}  /* mouse over link */
a:active  {color:#337ab7;}  /* selected link   */
 th {
     text-align: center;
}

             </style>
       <script type="text/javascript">
        $(function () {
            $('[id*=ddlSoldTo]').multiselect({
                includeSelectAllOption: true
            });
            $('[id*=ddlShipTo]').multiselect({
            includeSelectAllOption: true
            });
            //$("#Button1").click(function () {
            //    alert($(".multiselect-selected-text").html());
            //});
           });

    </script>
   

</asp:Content>
<asp:Content ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
                                     <div class="panel panel-primary">
            <div class="panel-heading"  style="height:36px;vertical-align:top">
               <a data-toggle="collapse" href="#resdiv" aria-expanded="true" aria-controls="resdiv"  style="color:white">Criteria</a>
            </div>           
        <!-- /.panel-heading -->
            <div id="resdiv" class="panel-collapse collapse in"  style="overflow-x: auto;">
            <div class="w3-panel w3-white w3-card-4 w3-section w3-padding-16" style="margin-top: 0px">
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="w3-row">
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sold-to
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <uc1:MultiCheckCombo ID="ddlSoldTo" runat="server" OnSelectedIndexChanged="ddlSoldTo_SelectedIndexChanged" /> 
                                    <%--<asp:ListBox ID="ddlSoldTo" runat="server" SelectionMode="Multiple"  CssClass="w3-input" DataTextField="CustomerName" DataValueField="CustomerNo" AutoPostBack="true" OnSelectedIndexChanged="ddlSoldTo_SelectedIndexChanged">
                                    </asp:ListBox>--%>

                                </div>
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Ship-to
                            </div>
                            <div class="w3-col s8">
                                <div class="my-ddl">
                                    <uc2:MultiCheckComboShipto ID="ddlShipTo" runat="server" /> 
                                    <%--<asp:DropDownList ID="ddlShipTo" runat="server" CssClass="w3-input" DataTextField="Name" DataValueField="Code">
                                    </asp:DropDownList>--%>
                                </div>
                            </div>
                        </div>
                            <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Sales confirmation
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtSaleConfirmno" runat="server" CssClass="w3-input" MaxLength="10" />
                              
                            </div>
                        </div>
                          <div class="w3-col s6">
                            <div class="control-label col-md-4">
                              Sales order no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtSaleOrderno" runat="server" CssClass="w3-input" MaxLength="10" />
                              
                            </div>
                        </div>


                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Invoice Date.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="invDateFrom" runat="server" CssClass="w3-input" MaxLength="10" />
                                <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="invDateFrom" runat="server" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Invoice no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="w3-input" MaxLength="10" />
                               <%-- <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="invDateTo" runat="server" />--%>
                            </div>
                        </div>

                           <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Delivery no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDeliveryNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Delivery date
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDelivertDate" runat="server" CssClass="w3-input" MaxLength="10" />
                                <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="txtDelivertDate" runat="server" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Shipment no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtShipmentNo" runat="server" CssClass="w3-input" MaxLength="20" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Shipment date.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtShipmentDate" runat="server" CssClass="w3-input" MaxLength="20" />
                                <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="txtShipmentDate" runat="server" />
                            </div>
                        </div>

                     
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                              Product/Grade
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtProductGrade" runat="server" CssClass="w3-input" MaxLength="10" />
                                <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="invDateFrom" runat="server" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                Customer P/O
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtPO" runat="server" CssClass="w3-input" MaxLength="10" />
                              <%--  <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="invDateTo" runat="server" />--%>
                            </div>
                        </div>
                          <div class="w3-col s6">
                            <div class="control-label col-md-4">
                              Document no.
                            </div>
                            <div class="w3-col s8">
                                <asp:TextBox ID="txtDocno" runat="server" CssClass="w3-input" MaxLength="10" />
                                <atk:CalendarExtender Animated="true" FirstDayOfWeek="Monday" Format="dd/MM/yyyy"
                                    TargetControlID="invDateFrom" runat="server" />
                            </div>
                        </div>
                        <div class="w3-col s6">
                            <div class="control-label col-md-4">
                                
                            </div>
                            <div class="w3-col s8">
                                
                            </div>
                        </div>
            
            
                    </div>
                    <p>
                         <input type="hidden" name="soh" id="soh" runat="server"  value="" />
    <input type="hidden" name="shh" id="shh" runat="server" value="" />
                        <asp:Button Text="Search" runat="server" CssClass="button-img-s img-search" ID="btnSearch"
                            OnClick="btnSearch_Click" />
                    </p>
                </asp:Panel>
            </div>
                </div>
                             </div>
            <div class="panel panel-primary">
            <div class="panel-heading"  style="height:36px;vertical-align:text-top">
               <a data-toggle="collapse" href="#prddiv"  style="color:white">Results</a>
            </div>
         <div id="prddiv" class="panel-collapse collapse in" style="overflow-x: auto;">
            <div class="panel-body">
                    <asp:GridView ID="grd" runat="server" AutoGenerateColumns="false" GridLines="None"
                        ShowHeaderWhenEmpty="true" AllowSorting="true" OnSorting="grd_Sorting" AllowPaging="True"
                        PageSize="10" OnPageIndexChanging="grd_PageIndexChanging"
                        DataSource='<%# AdvanceSalesDT %>'
                        CssClass="table table-striped"   PagerStyle-CssClass="pager"
                    RowStyle-CssClass="odd gradeX"  HeaderStyle-CssClass="header" Width="100%"
                        OnRowCommand="grd_RowCommand">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="No">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Shipment_Number" HeaderText="Shiptment" SortExpression="Shipment_Number" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Customer_PO" HeaderText="Customer P/O" SortExpression="Customer_PO" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center"  />
                            <asp:BoundField DataField="Sales_Confrim" HeaderText="Sales Confirm" SortExpression="Sales_Confrim" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Sales_Order" HeaderText="Sales Order" SortExpression="Sales_Order" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                           <asp:BoundField DataField="Invoice" HeaderText="Invoice" SortExpression="Invoice" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />




                             <asp:TemplateField HeaderText="Invoice Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# FormatHelper.DateToStr(Eval("Invoice_Date")) %>
                                </ItemTemplate>
                            </asp:TemplateField>


                          <%-- <asp:BoundField DataField="Delivery" HeaderText="Delivery" SortExpression="Delivery" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />--%>
                            <asp:TemplateField HeaderText="Delivery" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Eval("Delivery") %>' runat="server" CommandName="ORDER_DOWNLOAD" CommandArgument='<%# Eval("Delivery")  %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Delivery Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%# FormatHelper.DateToStr(Eval("Delivery_Date")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:BoundField DataField="Shipment_Number" HeaderText="Shipment No." SortExpression="Shipment_Number" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                          --%>  <asp:BoundField DataField="Ship_to_party" HeaderText="Ship to" SortExpression="Ship_to_party" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Ship_to_party_name" HeaderText="Name" SortExpression="Ship_to_party_name" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                            
                             
                            <asp:BoundField DataField="Product_Grade" HeaderText="Product Grade" SortExpression="Product_Grade" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                            <asp:BoundField DataField="Qty_MT" HeaderText="Qty M/T" SortExpression="Qty_MT" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" />
                         
                       

                        </Columns>
                    </asp:GridView>
                </div>
             </div>
                    <script type="text/javascript" src="Scripts/jquery.min.js"></script>
                    <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
                    <script type="text/javascript" src="Scripts/gridviewScroll.min.js"></script>
                    <script type="text/javascript"> 
                        function pageLoad() {
                            $('#<%=grd.ClientID%>').gridviewScroll({
                                width: 2000,
                                height: 700,
                                arrowsize: 30,
                                varrowtopimg: "Images/arrowvt.png",
                                varrowbottomimg: "Images/arrowvb.png",
                                harrowleftimg: "Images/arrowhl.png",
                                harrowrightimg: "Images/arrowhr.png"
                            });
                        }
                    </script>
               
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
