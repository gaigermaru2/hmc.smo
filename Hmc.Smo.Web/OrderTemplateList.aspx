﻿<%@ Page Title="Shipment Detail : SmartOrder by HMC" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeBehind="OrderTemplateList.aspx.cs" Inherits="Hmc.Smo.Web.OrderTemplateList"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ContentPlaceHolderID="cphPageName" runat="server">
    Order Template by Customer
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpnButtons" runat="server">
    <asp:Button ID="btnNew" Text="NEW TEMPLATE" runat="server" CssClass="button-img-blue img-plus-gray" OnClick="btnNew_Click" />
    <asp:Panel ID="pnlTemplate" runat="server" CssClass="panel-popup" Width="600px"
        DefaultButton="btnTemplateCancel">
        <div class="panel-popup-hd">
            <div class="w3-row">
                <asp:Image ID="Image5" ImageUrl="~/Images/notice_40_black.png" CssClass="panel-popup-hd-img"
                    runat="server" />
                <div class="panel-popup-hd-text">
                    <asp:Label ID="txtTemplateHeader" runat="server" Text="Template" />
                </div>
                <div class="panel-popup-hd-text" style="float: right">
                    <asp:Button ID="btnTemplateCancel" runat="server" Text="X" />
                </div>
            </div>
        </div>
        <div class="panel-popup-content">
            <asp:Panel ID="pnlTemplateAdd" runat="server">
                <div class="w3-row">
                    <div class="control-label col-md-4">
                        Customer
                    </div>
                    <div class="w3-col s8 w3-left-align">
                        <div style="margin-left: 20px; margin-top: 5px">
                            <asp:DropDownList ID="ddlCustomer" runat="server" DataSource='<%# Customers %>' CssClass="w3-input"
                                DataTextField="CustomerText" DataValueField="CustomerNo">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="w3-row">
                    <div class="control-label col-md-4">
                        Choose file
                    </div>
                    <div class="w3-col s8 w3-left-align">
                        <div style="margin-left: 20px; margin-top: 5px">
                            <asp:FileUpload CssClass="FileUploadClass" runat="server" ID="ful" />
                            <asp:RequiredFieldValidator ID="reqUpload" ErrorMessage="*" ControlToValidate="ful" runat="server" ValidationGroup="detail" />
                        </div>
                    </div>
                </div>
                <div class="w3-row">
                    <div class="control-label col-md-4">
                        Allow download
                    </div>
                    <div class="w3-col s8 w3-left-align">
                        <div style="margin-left: 20px; margin-top: 10px">
                            <asp:CheckBox ID="chkAllow" runat="server" Checked='<%# OrderTemplate.AllowDownload %>' />
                        </div>
                    </div>
                </div>
                <div class="w3-row">
                    <div class="w3-col s7">
                        <asp:Label ID="Label1" runat="server" CssClass="err" />
                    </div>
                    <div class="w3-col s2" style="float: right">
                        <asp:Button ID="btnTemplateOK" runat="server" CssClass="button-img-s img-add-w"
                            Text="SAVE" ValidationGroup="detail"
                            UseSubmitBehavior="false" OnClick="btnTemplateOK_Click" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </asp:Panel>
    <asp:Button ID="btnTemplateHide" CssClass="hide" runat="server" />
    <atk:ModalPopupExtender ID="mpeTemplate" BackgroundCssClass="panel-bg" PopupControlID="pnlTemplate"
        TargetControlID="btnTemplateHide" CancelControlID="btnTemplateCancel" runat="server" />
</asp:Content>
<asp:Content ID="ctContent" ContentPlaceHolderID="cphContent" runat="server">

    <div class="w3-panel w3-white w3-card-4 w3-section" style="margin-top: 0px; padding-top: 10px;">
        <div class="w3-row">
            <p>
                <asp:GridView ID="grd" runat="server" AutoGenerateColumns="false" GridLines="None"
                    ShowHeaderWhenEmpty="true" DataSource='<%# OrderTemplates %>'
                    CssClass="grid" HeaderStyle-CssClass="header" PagerStyle-CssClass="pager"
                    RowStyle-CssClass="item" AlternatingRowStyle-CssClass="item-alt" Width="100%"
                    OnRowCommand="grd_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="CUSTOMER NO." ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140px">
                            <ItemTemplate>
                                <asp:LinkButton Text='<%# Eval("CustomerNo") %>' runat="server" CommandName="EDIT_ITEM" CommandArgument='<%# Eval("ID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CustomerName" HeaderText="NAME" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                        <asp:CheckBoxField DataField="AllowDownload" HeaderText="ALLOW DOWNLOAD" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140px" />
                        <asp:BoundField DataField="LastUploadtedTime" HeaderText="UPDATED TIME" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140px" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                    </Columns>
                </asp:GridView>
            </p>
        </div>
    </div>
</asp:Content>
