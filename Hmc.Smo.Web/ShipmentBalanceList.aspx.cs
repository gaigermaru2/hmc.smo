﻿using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Linq;

namespace Hmc.Smo.Web
{
    public partial class ShipmentBalanceList : BasePage
    {
        #region property

        public SearchShipmentCriteria Search
        {
            get { return Session["x"] as SearchShipmentCriteria; }
            set { Session["x"] = value; }
        }
        public DataTable ShipmentBalanceDT
        {
            get { return Session["ShipmentBalanceDT"] as DataTable; }
            set { Session["ShipmentBalanceDT"] = value; }
        }

        public List<ReportShipmentBalance> ShipmentBalances
        {
            get { return Session["ShipmentBalances"] as List<ReportShipmentBalance>; }
            set { Session["ShipmentBalances"] = value; }
        }

        public List<Customer> SoldTos
        {
            get { return Session["SoldTos"] as List<Customer>; }
            set { Session["SoldTos"] = value; }
        }
        public List<DropDownValue> ShipTos
        {
            get { return Session["ShipTos"] as List<DropDownValue>; }
            set { Session["ShipTos"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                SetTextBoxSelectAllOnFocus(txtQuotNo);

                IsShowCriteria = true;
                SortImage = new Image();
                SortImage.CssClass = "img-sort";

                using (var sv = new ServiceClient())
                {
                    if (IsUser || IsManager)
                    {
                        SoldTos = sv.SelectCustomersForList(CurrentCustomer.CustomerNo);
                    }
                    else
                    {
                        SoldTos = sv.SelectCustomersForList(null);
                    }

                    SoldTos.ForEach(o => o.CustomerName = string.Format("{0} : {1}", o.CustomerNo, o.CustomerName));

                    if (IsHMC)
                    {
                        SoldTos.Insert(0, new Customer() { CustomerNo = "", CustomerName = "-----Select Sold To-----" });
                    }
                    ddlSoldTo.DataSource = SoldTos;
                    ddlSoldTo.DataBind();
                }

                LoadSearchCriteria();

                if (string.IsNullOrEmpty(Search.SoldTo))
                {
                    ShipmentBalances = new List<ReportShipmentBalance>();
                    grd.DataSource = ShipmentBalances;
                    grd.DataBind();
                }
                else
                {
                    LoadReportShipmentBalances();
                }

                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadSearchCriteria()
        {
            if (Search == null)
            {
                Search = new SearchShipmentCriteria();

                if (IsUser || IsManager)
                {
                    Search.SoldTo = CurrentCustomer.CustomerNo;
                }
            }
            else
            {
                txtQuotNo.Text = Search.QuotationNo;
                txtDocNo.Text = Search.DocNo;
                ddlSoldTo.SelectedIndex = ddlSoldTo.Items.IndexOf(ddlSoldTo.Items.FindByValue(Search.SoldTo));
                ddlShipTo.SelectedIndex = ddlShipTo.Items.IndexOf(ddlShipTo.Items.FindByValue(Search.SoldTo));
                txtProductName.Text = Search.ProductName;
                ddlShipmentStatus.SelectedIndex = ddlShipmentStatus.Items.IndexOf(ddlShipmentStatus.Items.FindByValue(((int)Search.ShipmentStatus).ToString()));

                txtPONo.Text = Search.CustRef;
            }
        }

        private void SaveSearchCriteria()
        {
            Search = new SearchShipmentCriteria();
            Search.QuotationNo = string.IsNullOrEmpty(txtQuotNo.Text.Trim()) ? null : txtQuotNo.Text.Trim();
            Search.DocNo = string.IsNullOrEmpty(txtDocNo.Text.Trim()) ? null : txtDocNo.Text.Trim();
            Search.SoldTo = string.IsNullOrEmpty(ddlSoldTo.SelectedValue) ? null : ddlSoldTo.SelectedValue;
            if (string.IsNullOrEmpty(Search.SoldTo))
                Search.ShipTo = null;
            else
                Search.ShipTo = string.IsNullOrEmpty(ddlShipTo.SelectedValue) ? null : ddlShipTo.SelectedValue;
            Search.ProductName = string.IsNullOrEmpty(txtProductName.Text.Trim()) ? null : txtProductName.Text.Trim();
            Search.ShipmentStatus = (ShipmentStatus)int.Parse(ddlShipmentStatus.SelectedValue);
            Search.CustRef = txtPONo.Text.Trim();
        }

        private void LoadReportShipmentBalances(bool bind = true)
        {
            using (var sv = new ServiceClient())
            {

                ShipmentBalances = sv.SelectReportShipmentBalancesForList(Search);

                ShipmentBalanceDT = ConvertToDataTable<ReportShipmentBalance>(ShipmentBalances);
                ShipmentBalanceDT.DefaultView.Sort = "DocDate desc, QuotationNo desc, QuotationItem asc";

                if (bind)
                {
                    grd.DataSource = ShipmentBalanceDT;
                    grd.DataBind();
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadReportShipmentBalances();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void grd_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                GridSortData(CurrentSortDirection, e.SortExpression, ShipmentBalanceDT, grd);
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void GridSortData(string currentSortDirection, string sortExpression, DataTable dt, GridView grd)
        {
            if (currentSortDirection == "ASC")
            {
                NewSortDirection = "DESC";
                SortImage.ImageUrl = "~/Images/view_sort_ascending.png";
            }
            else
            {
                NewSortDirection = "ASC";
                SortImage.ImageUrl = "~/Images/view_sort_descending.png";
            }

            if (dt != null)
            {
                //Sort the data.
                SortExpression = sortExpression;

                var sort = sortExpression + " " + NewSortDirection;
                if (sortExpression == "DocDate")
                    sort += ", QuotationNo desc, QuotationItem asc";
                if (sortExpression == "QuotationNo")
                    sort += ", QuotationItem asc";

                dt.DefaultView.Sort = sort;
                grd.DataSource = dt;
                grd.DataBind();
                CurrentSortDirection = NewSortDirection;
            }

            int columnIndex = 0;
            foreach (DataControlFieldHeaderCell headerCell in grd.HeaderRow.Cells)
            {
                if (headerCell.ContainingField.SortExpression == sortExpression)
                {
                    columnIndex = grd.HeaderRow.Cells.GetCellIndex(headerCell);
                }
            }
            grd.HeaderRow.Cells[columnIndex].Controls.Add(SortImage);
        }

        protected void grd_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grd.PageIndex = e.NewPageIndex;
                grd.DataSource = ShipmentBalanceDT;
                grd.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void TextBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string id = ((TextBox)sender).ID;

                var ctrl = pnlSearch.FindControl(id);
                if (ctrl != null)
                    ctrl.Focus();

            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void ddlSoldTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SaveSearchCriteria();
                LoadReportShipmentBalances();
                LoadShipTos();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        private void LoadShipTos()
        {
            if (string.IsNullOrEmpty(Search.SoldTo))
            {
                ShipTos = new List<DropDownValue>();
            }
            else
            {
                ShipTos = ShipmentBalances.GroupBy(o => o.ShipTo, o => o.ShipToName, (id, text) => new DropDownValue() { Code = id, Name = string.Format("{0} : {1}", id, text.First()) }).ToList();
                ShipTos.Insert(0, new DropDownValue() { Code = "", Name = "All" });
            }
            ddlShipTo.DataSource = ShipTos;
            ddlShipTo.DataBind();
        }
    }
}