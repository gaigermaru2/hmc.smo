﻿using Hmc.Smo.Common;
using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class OrderDetail : BasePage
    {
        #region property
        public string CustomerNo
        {
            get
            {
                string custId = CurrentUser.Role == Role.HMCSaleCo ? Order.CustomerNo : CurrentCustomer.CustomerNo;
                return custId;
            }
        }

        public List<MailPDFSelectForList> Invoices
        {
            get { return Session["Invoices"] as List<MailPDFSelectForList>; }
            set { Session["Invoices"] = value; }
        }
        public List<MailPDFSelectForList> InvoicesOther
        {
            get { return Session["InvoicesOther"] as List<MailPDFSelectForList>; }
            set { Session["InvoicesOther"] = value; }
        }
        public List<AttachmentForList> Attachments
        {
            get { return Session["Attachments"] as List<AttachmentForList>; }
            set { Session["Attachments"] = value; }
        }

        protected bool InvoiceDownloadVisibility
        {
            get
            {
                bool rs = Invoices != null && Invoices.Count > 0;
                return rs;
            }

        }

        public bool CanSeeAttachFileButton
        {
            get
            {
                bool canSee = false;
                int cnt = Attachments != null ? Attachments.Count : 0;

                if (IsUser)
                {
                    if (OrderNo == null)
                    {
                        canSee = true;
                    }
                    else if (Order.OrderStatusCode == OrderStatusCode.New || Order.OrderStatusCode == OrderStatusCode.Draft || Order.OrderStatusCode == OrderStatusCode.Rejected)
                    {
                        canSee = true;
                    }
                    else
                    {
                        canSee = cnt > 0;
                    }
                }
                else
                {
                    canSee = cnt > 0;
                }
                return canSee;
            }
        }
        public bool CanAddAttachment
        {
            get
            {
                bool canSee = false;

                if (IsUser)
                {
                    if (Order.OrderStatusCode == OrderStatusCode.New
                        || Order.OrderStatusCode == OrderStatusCode.Draft
                        || Order.OrderStatusCode == OrderStatusCode.Rejected)
                    {
                        canSee = true;
                    }
                }
                return canSee;
            }
        }


        public bool CanUserEdit
        {
            get { return IsUser && (Order.OrderStatusCode == OrderStatusCode.New || Order.OrderStatusCode == OrderStatusCode.Draft || Order.OrderStatusCode == OrderStatusCode.Rejected); }
        }
        public bool CanUserCancel
        {
            get { return CanUserEdit && Order != null && !string.IsNullOrEmpty(Order.OrderNo); }
        }
        public bool CanManagerApprove
        {
            get { return IsManager && (Order.OrderStatusCode == OrderStatusCode.WaitingForApproval); }
        }
        public bool CanHMCEdit
        {
            get { return IsHMC && (Order.OrderStatusCode == OrderStatusCode.SubmittedToHMC); }
        }
        public bool CanUserOrHMCEdit
        {
            get { return CanUserEdit || CanHMCEdit; }
        }
        public string Arg
        {
            get { return ViewState["Arg"] as string; }
            set { ViewState["Arg"] = value; }
        }
        public string OrderNo
        {
            get { return Session["OrderNo"] as string; }
            set { Session["OrderNo"] = value; }
        }
        public string TempIdForAttachment
        {
            get { return Session["TempIdForAttachment"] as string; }
            set { Session["TempIdForAttachment"] = value; }
        }
        public string UrlRedirect
        {
            get { return Session["UrlRedirect"] as string; }
            set { Session["UrlRedirect"] = value; }
        }
        public int? ItemNo
        {
            get { return Session["ItemNo"] as int?; }
            set { Session["ItemNo"] = value; }
        }
        public OrderItem OrderItem
        {
            get { return Session["OrderItem"] as OrderItem; }
            set { Session["OrderItem"] = value; }
        }
        public Order Order
        {
            get { return Session["Order"] as Order; }
            set { Session["Order"] = value; }
        }
        public List<OrderItem> OrderItems
        {
            get { return Session["OrderItems"] as List<OrderItem>; }
            set { Session["OrderItems"] = value; }
        }
        public string ProductNo
        {
            get { return ddlProduct.SelectedValue; }
        }

        public string ProductNoTemp
        {
            get { return ViewState["ProductNoTemp"] as string; }
            set { ViewState["ProductNoTemp"] = value; }
        }

        public List<DBService.OrderHistory> OrderHistorys
        {
            get { return Session["OrderHistorys"] as List<DBService.OrderHistory>; }
            set { Session["OrderHistorys"] = value; }
        }
        public List<ProductMaterialMap> Materials
        {
            get { return Session["Materials"] as List<ProductMaterialMap>; }
            set { Session["Materials"] = value; }
        }
        public List<PaymentTerm> PaymentTerms
        {
            get { return Session["PaymentTerms"] as List<PaymentTerm>; }
            set { Session["PaymentTerms"] = value; }
        }
        public List<PackageSize> PackageSizes
        {
            get { return Session["PackageSizes"] as List<PackageSize>; }
            set { Session["PackageSizes"] = value; }
        }
        public List<PackageSize> PackageSizesRemove
        {
            get { return Session["PackageSizesRemove"] as List<PackageSize>; }
            set { Session["PackageSizesRemove"] = value; }
        }
        public List<Product> Products
        {
            get { return Session["Products"] as List<Product>; }
            set { Session["Products"] = value; }
        }
        public List<CustomerShipToMap> ShipTos
        {
            get { return Session["ShipTos"] as List<CustomerShipToMap>; }
            set { Session["ShipTos"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "fnOnUpdateValidators();");
            txtRemark.Attributes.Add("onkeyup", "CountChars(" + 255 + ", '" + txtRemark.ClientID + "'," + "'characterCount');");
            IsShowCriteria = true;

            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                OrderNo = Request.QueryString["OrderNo"];

                TempIdForAttachment = OrderNo == null
                      ? Guid.NewGuid().ToString()
                      : null;

                UrlRedirect = null;

                LoadOrder();
                LoadOrderItems();
                LoadAttachments();
              //  LoadAttachmentsCnt();
                LoadInvoices();

                bool isNew = OrderNo == null;
                bool isHmc = CurrentUser.Role == Role.HMCSaleCo;
                bool isSameCustomer = CurrentUser.CustomerNo == Order.CustomerNo;
                if (!(isNew || isHmc || isSameCustomer))
                {
                    DataBind();
                    UrlRedirect = "OrderList.aspx";
                    ShowMessage("You have no authorize to access this order");
                    return;
                }

                using (var sv = new ServiceClient())
                {
                    ddlStatus.DataSource = OrderStatuses;

                    PaymentTerms = sv.SelectPaymentTermByCustomerNo(CustomerNo);
                    ddlPaymentTerm.DataSource = PaymentTerms;

                    ShipTos = sv.SelectCustomerShipToMapsByCustomerNo(CustomerNo);
                    ddlShipTo.DataSource = ShipTos;

                    var customerGroups = sv.SelectCustomerGroupsForList();
                    ddlCustomerGroup.DataSource = customerGroups;

                    OrderHistorys = sv.SelectOrderHistoryByOrderNo(OrderNo);
                }

                ddlReason.DataSource = Const.ReasonPickers;
                ddlReason.DataBind();

                string status = OrderNo == null ? "New "
                    : (IsUser && CanUserEdit) || (IsHMC && CanHMCEdit) ? "Edit "
                    : "View";
                lblPageName.Text = status;

                BindControl();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void LoadMaterials()
        {
            if (!string.IsNullOrEmpty(ProductNo) && (!string.IsNullOrEmpty(ddlPackageSize.SelectedValue)))
            {
                using (var sv = new ServiceClient())
                {
                    var allMat = sv.SelectMaterialsProductNo(ProductNo);

                    string pack = ddlPackageSize.SelectedValue.ToUpper().Replace("KG", string.Empty);
                    pack = pack.Replace(" ", "");
                    var mats = allMat.Where(a => a.MaterialNo.Contains(pack)).ToList();

                    ddlMaterialNo.DataSource = mats;
                    ddlMaterialNo.DataBind();

                    var defMat = sv.SelectMaterialByByProductNoAndPackageSizeCode(ProductNo, ddlPackageSize.SelectedValue);

                    string matNo = ItemNo != null ? OrderItem.MaterialNo : defMat != null ? defMat.MaterialNo : string.Empty;
                    ddlMaterialNo.SelectedIndex = ddlMaterialNo.Items.IndexOf(ddlMaterialNo.Items.FindByValue(matNo));
                }
            }
            else
            {
                ddlMaterialNo.DataSource = null;
                ddlMaterialNo.DataBind();
            }
        }

        private void LoadOrder()
        {
            if (OrderNo == null)
            {
                Order = new Order();
                Order.OrderStatusCode = OrderStatusCode.New;
                Order.PODate = DateTime.Now;
            }
            else
            {
                using (var sv = new ServiceClient())
                {
                    Order = sv.SelectOrderByPK(OrderNo);
                }

                RedirectUrl += "?OrderNo=" + Order.OrderNo;
            }
        }

        private void LoadOrderItems()
        {
            if (OrderNo == null)
            {
                OrderItems = new List<OrderItem>();
            }
            else
            {
                using (var sv = new ServiceClient())
                {
                    var rev= sv.SelectOrderItemsByOrderRevisionNo(OrderNo);
                    if (rev.Count > 0) {
                        OrderItems = rev;
                    }
                    else
                    {
                        OrderItems = sv.SelectOrderItemsByOrderNo(OrderNo);
                    }
                    
                }
            }
        }

        private void BindControl()
        {
            grdOrderHistory.DataSource = OrderHistorys;
            grdOrderItem.DataSource = OrderItems;
            DataBind();

            if (OrderNo == null)
            {
                PaymentTerm payDefault = PaymentTerms.Where(p => p.PaymentTermCode == CurrentCustomer.PaymentTermCode).FirstOrDefault();
                if (payDefault != null)
                    ddlPaymentTerm.SelectedIndex = ddlPaymentTerm.Items.IndexOf(ddlPaymentTerm.Items.FindByValue(payDefault.PaymentTermCode));

                CustomerShipToMap shipToDefault = ShipTos.Where(p => p.IsDefault).FirstOrDefault();
                if (shipToDefault != null)
                    ddlShipTo.SelectedIndex = ddlShipTo.Items.IndexOf(ddlShipTo.Items.FindByValue(shipToDefault.ShipToCode));

                ddlCustomerGroup.SelectedIndex = ddlCustomerGroup.Items.IndexOf(ddlCustomerGroup.Items.FindByValue(CurrentCustomer.CustomerGroupCode));

                if (Order.PODate.HasValue)
                    txtPODate.Text = Order.PODate.Value.ToString(DataFormat.Date);
            }
            else
            {
                txtOrderNo.Text = Order.OrderNo;
                if (Order.PricingDate.HasValue)
                    txtPricingDate.Text = Order.PricingDate.Value.ToString(DataFormat.DateTime);
                txtSaleConfirmNo.Text = Order.SaleConfirmNo;
                if (Order.SaleConfirmDate.HasValue)
                    txtSaleConfirmNoDate.Text = Order.SaleConfirmDate.Value.ToString(DataFormat.Date);
                txtPONo.Text = Order.PONo;
                if (Order.PODate.HasValue)
                    txtPODate.Text = Order.PODate.Value.ToString(DataFormat.Date);
                if (Order.ValidityDate.HasValue)
                    txtValidityDate.Text = Order.ValidityDate.Value.ToString(DataFormat.Date);
                txtCustomerCode.Text = Order.CustomerNo;
                txtCustomerName.Text = Order.CustomerName;
                txtRemark.Text = Order.Remark;
                ddlShipTo.SelectedIndex = ddlShipTo.Items.IndexOf(ddlShipTo.Items.FindByValue(Order.ShipToCode));
                ddlPaymentTerm.SelectedIndex = ddlPaymentTerm.Items.IndexOf(ddlPaymentTerm.Items.FindByValue(Order.PaymentTermCode));
                ddlCustomerGroup.SelectedIndex = ddlCustomerGroup.Items.IndexOf(ddlCustomerGroup.Items.FindByValue(Order.CustomerGroupCode));
            }
            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Order.OrderStatusCode));

            lblVat.Text = string.Format("VAT {0:#,##0}%", Tax);
        }

        public decimal Tax
        {
            get
            {
                using (var sv = new ServiceClient())
                {
                    //เปลี่ยนการดึง Tax จาก Shipto เป็น Soldto
                    decimal taxClass = sv.SelectTaxPercentByTaxClass(CurrentCustomer.TaxClass);
                    return taxClass;

                    //var shipTo = ShipTos.Where(x => x.ShipToCode == ddlShipTo.SelectedValue).FirstOrDefault();
                    //decimal taxClass = shipTo == null ? 0 : sv.SelectTaxPercentByTaxClass(shipTo.TaxClass);
                    //return taxClass;
                }
            }
        }


        protected void btnProductDetailSave_Click(object sender, EventArgs e)
        {
            try
            {
                string err;
                //check price
                OrderItem temp;
                if (CheckProduct(ddlProduct.SelectedValue, ddlMaterialNo.SelectedValue, txtUnitPrice.Text, ddlPackageSize.SelectedValue, txtProductQty.Text, txtProductDlvDate.Text, Products, PackageSizes, out temp, out err, txtUnitPrice, txtProductQty))
                {
                    if (ItemNo == null)
                    {
                        OrderItem.OrderNo = Guid.NewGuid().ToString();
                        OrderItem.ItemNo = OrderItems.Count + 1;
                    }
                    else
                    {
                        OrderItems = OrderItems.Where(op => op.ItemNo != ItemNo).ToList();
                    }

                    OrderItem.ProductNo = ddlProduct.SelectedValue;
                    OrderItem.ProductText = ((ListItem)ddlProduct.SelectedItem).Text;
                    OrderItem.PackageSizeCode = ddlPackageSize.SelectedValue;
                    OrderItem.PackageSizeText = ((ListItem)ddlPackageSize.SelectedItem).Text;
                    OrderItem.Quantity = int.Parse(txtProductQty.Text);
                    OrderItem.MaterialNo = ddlMaterialNo.SelectedValue;
                    OrderItem.MaterialText = ((ListItem)ddlMaterialNo.SelectedItem).Text;

                    decimal ppp = decimal.Parse(txtUnitPrice.Text);
                    int qty = int.Parse(txtProductQty.Text);
                    decimal amount = ppp * qty;


                    OrderItem.Quantity = qty;
                    OrderItem.UnitPrice = ppp;
                    OrderItem.Amount = amount;

                    //add 2017-06-09
                    DateTime? dlvDate = null;
                    if (!string.IsNullOrEmpty(txtProductDlvDate.Text))
                    {
                        DateTime rs;
                        bool canParseDlvDate = DateTime.TryParseExact(txtProductDlvDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out rs);
                        if (canParseDlvDate)
                            dlvDate = rs;
                    }
                    OrderItem.DeliveryDate = dlvDate;

                    OrderItems.Add(OrderItem);

                    RecalculateOrder(Order, OrderItems, Tax);
                    DataBind();

                    grdOrderItem.DataSource = OrderItems.OrderBy(op => op.ItemNo);
                    grdOrderItem.DataBind();
                }
                else
                {
                    lblProductMsg.Text = err;
                    mpeProduct.Show();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void LoadOrderItem()
        {
            if (ItemNo == null)
            {
                OrderItem = new OrderItem();

                txtProductQty.Text = string.Empty;
                txtUnitPrice.Text = string.Empty;
                txtProductDlvDate.Text = string.Empty;
            }
            else
            {
                OrderItem = OrderItems.Where(pr => pr.ItemNo == ItemNo).FirstOrDefault();

                txtProductQty.Text = OrderItem.Quantity.ToString();
                txtUnitPrice.Text = OrderItem.UnitPrice.ToString();
                if (OrderItem.DeliveryDate.HasValue)
                    txtProductDlvDate.Text = OrderItem.DeliveryDate.Value.ToString("dd/MM/yyyy");
            }
        }

        protected void grdOrderItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "EDT")
                {
                    txtUnitPrice.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.NORMAL_COLOR);
                    txtProductQty.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.NORMAL_COLOR);

                    ItemNo = Convert.ToInt32(e.CommandArgument);
                    btnProductDetailSave.Text = "OK";
                    lblProductHeader.Text = "EDIT PRODUCT";

                    LoadOrderItem();
                    LoadProducts(false);
                    LoadPackageSize();
                    LoadMaterials();

                    lblProductMsg.Text = string.Empty;

                    mpeProduct.Show();
                }
                else if (e.CommandName == "COPY")
                {
                    var itemNo = int.Parse((string)e.CommandArgument);
                    var it = OrderItems.Where(o => o.ItemNo == itemNo).FirstOrDefault();

                    //load date of this items
                    txtUnitPrice.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.NORMAL_COLOR);
                    txtProductQty.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.NORMAL_COLOR);

                    btnProductDetailSave.Text = "OK";
                    lblProductHeader.Text = "COPY PRODUCT";

                    ItemNo = null;

                    LoadOrderItem();

                    OrderItem.ProductNo = it.ProductNo;
                    OrderItem.ProductText = it.ProductText;
                    OrderItem.PackageSizeCode = it.PackageSizeCode;
                    OrderItem.PackageSizeText = it.PackageSizeText;
                    OrderItem.UnitPrice = it.UnitPrice;

                    txtUnitPrice.Text = OrderItem.UnitPrice.ToString();

                    LoadProducts(false);
                    LoadPackageSize();
                    LoadMaterials();

                    lblProductMsg.Text = string.Empty;

                    mpeProduct.Show();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        public class DeliveryDate
        {
            public string OrderNo { get; set; }
            public int ItemNo { get; set; }
            public int Seq { get; set; }
            public int? Qty { get; set; }
            public DateTime? Date { get; set; }
        }

        private void ReOrderItemNoOrderItems()
        {
            for (int i = 0; i < OrderItems.Count; i++)
            {
                OrderItems[i].ItemNo = i + 1;
            }
        }

        protected void btnProductDetailOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if (OrderItems != null && OrderItems.Count >= Config.WEB_ITEM_MAX)
                {
                    ShowMessage(Msg.Max10LinePerOrder);
                    return;
                }

                txtUnitPrice.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.NORMAL_COLOR);
                txtProductQty.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.NORMAL_COLOR);
                ItemNo = null;

                btnProductDetailSave.Text = "Add";
                lblProductMsg.Text = "";
                lblProductHeader.Text = "Add Product";

                LoadOrderItem();
                LoadProducts(true);
                LoadPackageSize();

                lblProductMsg.Text = string.Empty;
                mpeProduct.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void LoadPackageSize()
        {
            if (!string.IsNullOrEmpty(ProductNo))
            {
                using (var sv = new ServiceClient())
                {
                    PackageSizes = new List<PackageSize>();
                    PackageSizesRemove = new List<PackageSize>();
                    List<PackageSize> PackageSizestemp = sv.SelectPackageSizesByProductNo(ProductNo);
                    //PackageSizes = sv.SelectPackageSizesByProductNo(ProductNo);
                    PackageSizesRemove = sv.SelectPackageSizesByProductNoWithCustomer(ProductNo, CurrentUser.CustomerNo);

                    if (PackageSizesRemove.Count > 0)
                    {
                        //PackageSizes = PackageSizesRemove;
                        foreach (PackageSize element in PackageSizestemp)
                        {
                            bool isAdd = false;
                            foreach (PackageSize master in PackageSizesRemove)
                            {
                                if (element.PackageSizeCode + CurrentUser.CustomerNo == master.PackageSizeCode)
                                {
                                    isAdd = true;
                                    break;
                                }
                                else if (!element.PackageSizeCode.Equals(master.PackageSizeCode.Substring(0,5)))
                                {
                                    isAdd = true;
                                    break;
                                }
                            }
                            if (isAdd)
                            {
                                PackageSizes.Add(element);
                            }
                        }
                    }
                    else
                    {
                        PackageSizes = PackageSizestemp;
                    }

                }
            }
            else
            {
                PackageSizes = new List<PackageSize>();
            }

            ddlPackageSize.DataSource = PackageSizes;
            ddlPackageSize.DataBind();
            ddlPackageSize.Items.Insert(0, new ListItem("--- Select Package Size ---", string.Empty));
            ddlPackageSize.SelectedIndex = ddlPackageSize.Items.IndexOf(ddlPackageSize.Items.FindByValue(OrderItem.PackageSizeCode));
        }

        private void LoadProducts(bool onlyFav)
        {
            List<Product> products = new List<Product>();
            bool isLoadAllProducts;

            using (var sv = new ServiceClient())
            {
                products = sv.SelectProductByCustomerNo(CustomerNo);

                isLoadAllProducts = !onlyFav || products.Count == 0;

                if (isLoadAllProducts)
                {
                    var ps_temp = sv.SelectProductExceptCustomerNo(CustomerNo);
                    products.AddRange(ps_temp);
                }
            }

            Products = products;

            ddlProduct.DataSource = products;
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("--- Select Product ---", ""));
            ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(OrderItem.ProductNo));
            if (!isLoadAllProducts)
                ddlProduct.Items.Add(new ListItem("--- ALL ---", "ALL"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveData())
                {
                    ChangeOrderStatusAndRefreshPage(OrderStatusCode.Draft);
                    UrlRedirect = Const.URL_TO_CURRENT + Order.OrderNo;
                    ShowMessage(Msg.MSG_SUCC_SAVE_TH + Environment.NewLine + Msg.MSG_SUCC_SAVE_EN);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private bool SaveData()
        {
            if (Order.TotalAmount >= 9999999999 || Order.TotalAmount <= -9999999999)
            {
                ShowMessage("Total amount must be between -9,999,999,999 and 9,999,999,999");
                return false;
            }
            if (OrderItems.Count == 0)
            {
                ShowMessage("Please add 1 of Product!");
                return false;
            }
            Order.PONo = txtPONo.Text;

            Order.PaymentTermCode = ddlPaymentTerm.SelectedValue;
            Order.ShipToCode = ddlShipTo.SelectedValue;
            Order.CustomerGroupCode = ddlCustomerGroup.SelectedValue;

            Order.SaleConfirmNo = txtSaleConfirmNo.Text;
            Order.Remark = txtRemark.Text;

            if (!string.IsNullOrEmpty(txtValidityDate.Text) && txtPODate.Text != "__/__/____")
                Order.ValidityDate = DateTime.ParseExact(txtValidityDate.Text, DataFormat.Date, null);

            if (!string.IsNullOrEmpty(txtPODate.Text) && txtPODate.Text != "__/__/____")
                Order.PODate = DateTime.ParseExact(txtPODate.Text, DataFormat.Date, null);

            if (OrderNo == null)
            {
                Order.CustomerNo = CurrentCustomer.CustomerNo;
                Order.CustomerName = CurrentCustomer.CustomerName;

                using (var sv = new ServiceClient())
                {
                    Order = sv.CreateOrder(Order, OrderItems, TempIdForAttachment);
                }
            }
            else
            {
                using (var sv = new ServiceClient())
                {
                    Order = sv.UpdateOrder(Order, OrderItems);
                }
            }

            return true;
        }

        public void ChangeOrderStatusAndRefreshPage(string newStatus)
        {
            string reason = string.Empty;

            if (Arg != null && (Arg.Contains("Reject") || Arg.Contains("Cancel")))
            {
                reason = string.Format("{0}, {1}", ddlReason.SelectedItem.Text, txtReason.Text);
            }

            using (var sv = new ServiceClient())
            {
                sv.ChangeOrderStatus(Order.OrderNo, CurrentUser.Username, newStatus, reason);
            }
        }

        protected void btnConfirmOpen_Click(object sender, EventArgs e)
        {
            string arg = string.Empty;
            ItemNo = null;

            if (sender is Button)
            {
                var btn = (Button)sender;
                arg = btn.CommandArgument;

            }
            else if (sender is ImageButton)
            {
                var btn = (ImageButton)sender;
                var args = btn.CommandArgument.Split(',');

                ItemNo = int.Parse(args[0]);
                arg = args[1];
            }

            btnReasonYes.Text = arg;
            Arg = arg;

            if (Arg == "USR")
            {
                if (!CheckQuantity())
                    return;

                if (CurrentUser.HasApprover)
                    txtConfirm.Text = Msg.DoYouWantToSubmitToApproval;
                else
                    txtConfirm.Text = Msg.DoYouWantToSubmitToHMC;
            }
            else if (Arg == "MGR")
            {
                txtConfirm.Text = Msg.DoYouWantToSubmitToHMC;
            }
            else if (Arg == "HMC")
            {
                if (!CheckQuantity())
                    return;
                txtConfirm.Text = Msg.DoYouWantToSubmitToSAP;
            }
            else if (Arg == "DEL")
            {
                txtConfirm.Text = Msg.CONFIRM_DEL_LINE;
            }
            else
            {
                throw new NotImplementedException();
            }

            mpeConfirm.Show();
            btnConfirmYes.Focus();
        }

        private bool CheckQuantity()
        {
            if (Order.TotalQty < Config.WEB_QTY_MIN || Order.TotalQty > Config.WEB_QTY_MAX)
            {
                ShowMessage(string.Format(Msg.MinOrderQty, Config.WEB_QTY_MIN, Config.WEB_QTY_MAX));
                return false;
            }
            return true;
        }

        protected void btnReasonOpen_Click(object sender, EventArgs e)
        {
            try
            {
                var btn = (Button)sender;
                var arg = btn.CommandArgument;

                txtReasonHeader.Text = arg == "RejectByApprover" || arg == "RejectByHMC"
                    ? "Reason for rejection"
                    : "Reason for cancellation";
                btnReasonYes.Text = arg == "RejectByApprover" || arg == "RejectByHMC"
                    ? "Reject Order"
                    : "Cancel Order";

                Arg = arg;

                txtReason.Text = string.Empty;
                mpeReason.Show();
                txtReason.Focus();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void btnReasonYes_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> tos = new List<string>();
                List<string> ccs = new List<string>();
                string msg = string.Empty;
                string mailSubject = string.Empty;
                string mailBody = string.Empty;
                string url = Request.ServerVariables["SERVER_NAME"] + "/dev3";

                var status = (Arg == "RejectByApprover" || Arg == "RejectByHMC") ? OrderStatusCode.Rejected : OrderStatusCode.Canceled;
                ChangeOrderStatusAndRefreshPage(status);

                UrlRedirect = Const.URL_TO_CURRENT + Order.OrderNo;

                using (var sv = new ServiceClient())
                {
                    if (Arg == "RejectByApprover" || Arg == "RejectByHMC")
                    {
                        msg = Msg.MSG_SUCC_REJECT_TH + Environment.NewLine + Msg.MSG_SUCC_REJECT_EN;

                        if (Arg == "RejectByApprover")
                        {
                            sv.SendEmailByCaseNo(7, CurrentUser, Order, OrderItems, url, null);
                        }
                        else if (Arg == "RejectByHMC")
                        {
                            sv.SendEmailByCaseNo(10, CurrentUser, Order, OrderItems, url, null);
                        }
                    }
                    else if (Arg == "CancelByUser" || Arg == "CancelByApprover" || Arg == "CancelByHMC")
                    {
                        msg = Msg.MSG_SUCC_CANCEL_TH + Environment.NewLine + Msg.MSG_SUCC_CANCEL_EN;

                        var historys = sv.SelectOrderHistoryByOrderNo(Order.OrderNo);
                        var wasSubmittedToHmc = historys.Where(h => h.OrderStatusCode == OrderStatusCode.SubmittedToHMC).Count() > 0;

                        if (Arg == "CancelByUser")
                        {
                            if (wasSubmittedToHmc)
                            {
                                sv.SendEmailByCaseNo(11, CurrentUser, Order, OrderItems, url, null);
                            }
                        }
                        else if (Arg == "CancelByApprover")
                        {
                            if (wasSubmittedToHmc)
                            {
                                sv.SendEmailByCaseNo(12, CurrentUser, Order, OrderItems, url, null);
                            }
                            else
                            {
                                sv.SendEmailByCaseNo(9, CurrentUser, Order, OrderItems, url, null);
                            }
                        }
                        else if (Arg == "CancelByHMC")
                        {
                            sv.SendEmailByCaseNo(13, CurrentUser, Order, OrderItems, url, null);
                        }
                    }

                    ShowMessage(msg);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void btnConfirmYes_Click(object sender, EventArgs e)
        {
            try
            {
             //   btnConfirmYes.Enabled = false;
                string url = Request.ServerVariables["SERVER_NAME"] + "/dev3";

                using (var sv = new ServiceClient())
                {
                    if (Arg == "USR")
                    {
                        if (SaveData())
                        {
                            ChangeOrderStatusAndRefreshPage(OrderStatusCode.Draft);

                            if (CurrentUser.HasApprover)
                            {
                                ChangeOrderStatusAndRefreshPage(OrderStatusCode.WaitingForApproval);
                                UrlRedirect = Const.URL_TO_CURRENT + Order.OrderNo;

                                sv.SendEmailByCaseNo(3, CurrentUser, Order, OrderItems, url, null);

                                DateTime today = DateTime.Today;
                                string msg = string.Empty;

                                DateTime dueDate = today;

                                if (today.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    int offset = today.DayOfWeek - DayOfWeek.Monday;

                                    DateTime lastMonday = today.AddDays(-offset);
                                    DateTime nextSunday = lastMonday.AddDays(6);

                                    dueDate = nextSunday;
                                }

                                msg = string.Format(Msg.MSG_ORDER_VALID_TH, dueDate.ToString(DataFormat.Date))
                                   + Environment.NewLine
                                   + string.Format(Msg.MSG_ORDER_VALID_EN, dueDate.ToString(DataFormat.Date));

                                ShowMessage(msg);
                            }
                            else
                            {
                                ChangeOrderStatusAndRefreshPage(OrderStatusCode.SubmittedToHMC);
                                sv.SendEmailByCaseNo(14, CurrentUser, Order, OrderItems, url, null);
                                Response.Redirect(Const.URL_TO_CURRENT + Order.OrderNo, false);
                            }
                        }
                    }
                    else if (Arg == "MGR")
                    {
                        ChangeOrderStatusAndRefreshPage(OrderStatusCode.SubmittedToHMC);
                        sv.SendEmailByCaseNo(4, CurrentUser, Order, OrderItems, url, null);
                        Response.Redirect(Const.URL_TO_CURRENT + Order.OrderNo, false);
                    }
                    else if (Arg == "HMC")
                    {
                        if (SaveData())
                        {
                            Customer c = sv.SelectCustomerByPK(Order.CustomerNo, null, null, null);

                            RS_ZIDOC_INPUT_QUOTATION rs = new RS_ZIDOC_INPUT_QUOTATION();
                            rs.Order = Order;
                            rs.OrderItems = OrderItems;
                            rs.Customer = c;
                            rs.User = CurrentUser;

                            rs = sv.BAPI_ZIDOC_INPUT_QUOTATION(rs);

                            if (string.IsNullOrEmpty(rs.IDOC_MESSAGE))
                            {
                                txtSaleConfirmNo.Text = rs.IDOC_QUOTATION;
                                Order.SaleConfirmNo = rs.IDOC_QUOTATION;
                                Order.SaleConfirmDate = DateTime.Now;

                                SaveData();
                                ChangeOrderStatusAndRefreshPage(OrderStatusCode.SubmittedToSAP);
                                sv.SendEmailByCaseNo(5, CurrentUser, Order, OrderItems, url, null);
                                Response.Redirect(Const.URL_TO_CURRENT + Order.OrderNo, false);
                            }
                            else
                            {
                                ShowMessage(rs.IDOC_MESSAGE);
                            }
                        }
                    }
                    else if (Arg == "DEL")
                    {
                        OrderItems = OrderItems.Where(o => o.ItemNo != ItemNo).OrderBy(o => o.ItemNo).ToList();

                        ReOrderItemNoOrderItems();
                        RecalculateOrder(Order, OrderItems, Tax);
                        DataBind();

                        grdOrderItem.DataSource = OrderItems;
                        grdOrderItem.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        public void ShowMessage(string msg)
        {
            txtMsg.Text = msg;
            mpeMsg.Show();
        }

        protected void grdOrderItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            bool canEditItem = IsUser ? CanUserEdit
                : IsHMC ? CanHMCEdit
                : false;

            e.Row.Cells[0].Visible = CanUserEdit;   //delete button
            e.Row.Cells[2].Enabled = canEditItem;   //column product code
            e.Row.Cells[3].Visible = IsHMC;         //column sap code
        }

        protected void btnStatusHistory_Click(object sender, EventArgs e)
        {
            try
            {
                grdOrderHistory.DataSource = OrderHistorys;
                grdOrderHistory.DataBind();
                mpeHistory.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlProduct.SelectedValue == "ALL")
                {
                    ProductNoTemp = ProductNo;
                    LoadProducts(false);
                    ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(ProductNoTemp));
                }

                LoadPackageSize();
                LoadMaterials();

                mpeProduct.Show();
                ddlProduct.Focus();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void ddlPackageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadMaterials();

                mpeProduct.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void btnMsgCancel_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(UrlRedirect))
            {
                Response.Redirect(UrlRedirect, false);
                UrlRedirect = null;
            }
            else
            {
                txtMsg.Text = string.Empty;
                mpeMsg.Hide();
            }
        }

        protected void ddlReason_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlReason.SelectedIndex == 0)
                {
                    rfvRemark.ControlToValidate = "txtReason";

                    mpeReason.Show();
                    txtReason.Focus();
                }
                else
                {
                    rfvRemark.ControlToValidate = "ddlReason";

                    mpeReason.Show();
                    btnReasonYes.Focus();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void ddlShipTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                RecalculateOrder(Order, OrderItems, Tax);
                DataBind();

                grdOrderItem.DataSource = OrderItems;
                grdOrderItem.DataBind();
                lblVat.Text = string.Format("VAT {0:#,##0}%", Tax);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void ctvProductQty_ServerValidate(object sender, ServerValidateEventArgs e)
        {

            bool isValid = !string.IsNullOrEmpty(e.Value);

            if (isValid)
            {
                int val = int.Parse(e.Value);
                isValid = val > 0 && val < 10000000;
            }
            e.IsValid = isValid;
        }

        protected void btnHmcSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (SaveData())
                {
                    ChangeOrderStatusAndRefreshPage(OrderStatusCode.SubmittedToHMC);
                    Response.Redirect(Const.URL_TO_CURRENT + Order.OrderNo, false);
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        #region Attachment


        protected void btnAttachFile_Click(object sender, EventArgs e)
        {
            try
            {
                lblErrorUpload.Text = string.Empty;
                lblAcceptExt.Text = Config.WEB_UP_EXTS;
                lblAttachMaxFile.Text = Config.WEB_UP_MAX_FILE.ToString();
                lblAttachMaxSize.Text = Config.WEB_UP_MAX_SIZE.ToString();

                LoadAttachments();

                pnlAttachmentAdd.Visible = CanAddAttachment;
                pnlAttachmentAdd.DataBind();

                mpeAttachment.Show();

                btnAttachmentCancel.Focus();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        private void LoadAttachmentsCnt()
        {
            using (var sv = new ServiceClient())
            {
                string orderNo = OrderNo == null ? null : Order.OrderNo;

                Attachments = sv.SelectAttachmentsForList(orderNo, TempIdForAttachment);
            }
            btnAttachFile.Text = "ATTACHMENTS (" + Attachments.Count+ ")";
            
            btnAttachFile.ForeColor = System.Drawing.Color.Green;
            //grdAttachment.DataSource = Attachments;
            //grdAttachment.DataBind();
        }

        private void LoadAttachments()
        {
            using (var sv = new ServiceClient())
            {
                string orderNo = OrderNo == null ? null : Order.OrderNo;

                Attachments = sv.SelectAttachmentsForList(orderNo, TempIdForAttachment);
            }
            if (Attachments.Count > 0) { 
            btnAttachFile.Text = "ATTACHMENTS (" + Attachments.Count + ")";
            btnAttachFile.BackColor = System.Drawing.Color.Green;
                btnAttachFile.BorderColor = System.Drawing.Color.Green;
            }
            else
            {
                btnAttachFile.Text = "ATTACHMENTS (" + Attachments.Count + ")";
               // btnAttachFile.BackColor = System.Drawing.Color.Green;
            }
            grdAttachment.DataSource = Attachments;
            grdAttachment.DataBind();
        }

        private void LoadInvoices()
        {
            using (var sv = new ServiceClient())
            {
                if (Order != null && !string.IsNullOrEmpty(Order.SaleConfirmNo))
                {
                    var temp = sv.SelectMailPDFsSelectForList(Order.SaleConfirmNo);
                    Invoices = temp.Where(o => o.SaleConfirmNo.Contains("-")).OrderByDescending(x => x.CreatedTime).ToList();
                   // InvoicesOther = temp.Where(x => x.InvoiceType != null && !x.InvoiceType.Contains("INV")).ToList();
                    
                    //Invoices = temp;
                }
            }
            
            grdInvoice.DataSource = Invoices;
            grdInvoice.DataBind();
          //  GridView1.DataSource = InvoicesOther;
          //  GridView1.DataBind();
        }


        private string FileName
        {
            get { return Session["FileName"] as string; }
            set { Session["FileName"] = value; }
        }

        protected void fulAttachment_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            try
            {
                lblErrorUpload.Text = string.Empty;
                var fulAttachment = (AjaxControlToolkit.AsyncFileUpload)sender;
                var fileName = fulAttachment.FileName;
                string msg = null;

                FileName = null;

                if (string.IsNullOrEmpty(fileName))
                {
                    msg = "AlertUploadFileNameEmp.Text";
                    lblErrorUpload.Text = msg;

                    mpeAttachment.Show();
                    return;
                }

                var fullPath = Server.MapPath("Temp/" + fileName);
                var info = new System.IO.FileInfo(fullPath);
                if (info.Exists)
                    info.Delete();

                if (!string.IsNullOrEmpty(fileName.ToString()))
                {
                    fulAttachment.SaveAs(fullPath);
                    FileName = fullPath;

                    btnAttachmentOK.Enabled = true;
                }
                else
                {
                    btnAttachmentOK.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        private void AttachmentCheck(string fileName, out bool isOk, out string err)
        {
            // check extesion
            string[] exts = Config.WEB_UP_EXTS.Split(',');
            isOk = false;
            err = string.Empty;
            string ext = string.Empty;

            if (fileName != null)
                ext = System.IO.Path.GetExtension(fileName).ToUpper();

            for (int i = 0; i < exts.Length; i++)
            {
                if (!isOk)
                {
                    if (ext == "." + exts[i].Trim().ToUpper())
                    {
                        isOk = true;
                    }
                }
            }

            if (!isOk)
            {
                err = "Please choose file " + Config.WEB_UP_EXTS;
                return;
            }

            // check number of attchment
            isOk = (Attachments == null) || (Attachments.Count < Config.WEB_UP_MAX_FILE);
            if (!isOk)
            {
                err = "Maximum attachment number on each order is " + Config.WEB_UP_MAX_FILE + " files";
                return;
            }
        }

        protected void btnAttachmentUpload_Click(object sender, EventArgs e)
        {
            try
            {
                bool isOkExtension;
                string extText;
                AttachmentCheck(FileName, out isOkExtension, out extText);
                lblErrorUpload.Text = string.Empty;

                if (isOkExtension)
                {
                    string errMsg;
                    var bytes = GetByteArrayFromFileName(FileName, out errMsg);
                    if (!string.IsNullOrEmpty(errMsg))
                    {
                        ShowMessage(errMsg);
                    }
                    else
                    {
                        var file = Path.GetFileName(FileName);

                        Attachment a = new Attachment();
                        a.AttachmentContent = bytes;
                        a.FileName = file;
                        a.CreatedBy = CurrentUser.Username;
                        a.OrderNo = OrderNo == null ? TempIdForAttachment : OrderNo;
                        a.Note = TempIdForAttachment;

                        using (var sv = new ServiceClient())
                        {
                            a = sv.CreateAttachment(a);
                        }

                        FileName = null;
                        LoadAttachments();
                        mpeAttachment.Show();
                    }
                }
                else
                {
                    lblErrorUpload.Text = extText;
                    mpeAttachment.Show();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        protected void grdAttachment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string fullPath = string.Empty;
            try
            {
                var attachmentId = (string)e.CommandArgument;

                if (e.CommandName == "DWNLD")
                {
                    using (var sv = new ServiceClient())
                    {
                        var attachment = sv.SelectAttachmentByPK(attachmentId);
                        fullPath = Server.MapPath("Temp/" + attachment.FileName);

                        File.WriteAllBytes(fullPath, attachment.AttachmentContent);

                        Thread.Sleep(2000);

                        string url = "DownloadFile.ashx?file=" + attachment.FileName;
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "downloadFileExport", "javascript:window.location ='" + ResolveUrl(url) + "';", true);
                        mpeAttachment.Show();
                    }
                }
                else if (e.CommandName == "DEL")
                {
                    mpeAttachmentDeleteConfirm.Show();
                    btnAttachmentDeleteConfirmYes.Focus();
                    btnAttachmentDeleteConfirmYes.CommandArgument = attachmentId;

                }

            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        protected void grdAttachment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[0].Visible = CanAddAttachment;   //delete button
        }

        protected void btnAttachmentDeleteConfirmYes_Click(object sender, EventArgs e)
        {
            try
            {
                var attachmentId = ((Button)sender).CommandArgument;

                using (var sv = new ServiceClient())
                {
                    sv.DeleteAttachmentByPK(attachmentId);
                }

                LoadAttachments();

                mpeAttachmentDeleteConfirm.Hide();
                mpeAttachment.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
        protected void btnAttachmentDeleteConfirmNo_Click(object sender, EventArgs e)
        {
            try
            {
                mpeAttachmentDeleteConfirm.Hide();
                mpeAttachment.Show();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }

        //protected void btnDlvDateYes_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        var attachmentId = ((Button)sender).CommandArgument;

        //        using (var sv = new ServiceClient())
        //        {
        //            sv.DeleteAttachmentByPK(attachmentId);
        //        }

        //        LoadAttachments();

        //        mpeAttachmentDeleteConfirm.Hide();
        //        mpeAttachment.Show();
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowMessage(ex.Message);
        //    }
        //}

        #endregion

        protected void grdInvoice_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "INV_DOWNLOAD")
                {
                    using (var sv = new ServiceClient())
                    {
                        var mailPDF = sv.SelectMailPDFByPK((string)e.CommandArgument);
                        var fileName = mailPDF.SaleConfirmNo + ".pdf";
                        var fullPath = Server.MapPath("Temp/" + fileName);

                        File.WriteAllBytes(fullPath, mailPDF.PDFContent);

                        Thread.Sleep(2000);

                        string url = "DownloadFile.ashx?file=" + fileName;
                        ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "downloadFileExport", "javascript:window.location ='" + ResolveUrl(url) + "';", true);
                    }

                    mpeDownloadInvoice.Show();
                }
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
            }
        }
    }
}