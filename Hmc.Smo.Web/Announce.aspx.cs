﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Hmc.Smo.Web.DBService;
using Hmc.Smo.Common;
using System.Data;
using System.IO;
using System.Threading;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using System.Globalization;

namespace Hmc.Smo.Web
{
    public partial class Announce : BasePage
    {
        #region property


        public DataTable AnnouncesDT
        {
            get { return Session["AnnouncesDT"] as DataTable; }
            set { Session["AnnouncesDT"] = value; }
        }


        public List<SpAnnounceSelectForList_Result> Announces
        {
            get { return Session["Announces"] as List<SpAnnounceSelectForList_Result>; }
            set { Session["Announces"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                List<DropDownValue> InvoiceTypeList = new List<DropDownValue>();
               // InvoiceTypeList.Insert(0, new DropDownValue() { Code = "", Name = "-----All-----" });
                InvoiceTypeList.Insert(0, new DropDownValue() { Code = "1", Name = "Main" });
                InvoiceTypeList.Insert(1, new DropDownValue() { Code = "2", Name = "Inside" });

                ddlType.DataSource = InvoiceTypeList;
                ddlType.DataBind();
                LoadAnnounceList(true);
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }
        private void LoadAnnounceList(bool bind = true)
        {
            using (var sv = new ServiceClient())
            {
                Announces = sv.SelectAnnounceForList(0);
                //announce ss = new announce();
                
                 //    ss.valid_from
                 // ss.Doc_no
                 AnnouncesDT = ConvertToDataTable<SpAnnounceSelectForList_Result>(Announces);
                // AdvanceSalesDT.DefaultView.Sort = "InvoiceDate desc, ShiptmentNo desc, Delivery desc";
                if (bind)
                {
                    grd.DataSource = AnnouncesDT;
                    grd.DataBind();
                }
            }
        }
        protected void btnNewAnnounce_Click(object sender, EventArgs e)
        {
            try
            {
                pnlAttachmentAdd.Visible = true;
                pnlAttachmentAdd.DataBind();

                mpeAdd.Show();

                btnAttachmentCancel.Focus();
            }
            catch (Exception ex)
            {
              //  ShowMessage(ex.Message);
            }
        }
        protected void btnCreateAnnounce_Click(object sender, EventArgs e)
        {
            try
            {
              

                        announce a = new announce();
                //add 2017-06-09
                DateTime? fromDate = null;
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(txtFrom.Text))
                {
                    DateTime rs;
                    bool canParseDlvDate = DateTime.TryParseExact(txtFrom.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out rs);
                    if (canParseDlvDate)
                        fromDate = rs;
                }
                if (!string.IsNullOrEmpty(txtFrom.Text))
                {
                    DateTime rs;
                    bool canParseDlvDate = DateTime.TryParseExact(txtTo.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out rs);
                    if (canParseDlvDate)
                        ToDate = rs;
                }


                a.valid_from = fromDate;
                        a.valid_to = ToDate;
                        a.type = int.Parse(ddlType.SelectedValue);
                          a.announce_text = txtRemark.Text;
                      

                        using (var sv = new ServiceClient())
                        {
                            a = sv.CreateAnnounce(a);
                        }

                   
            }
            catch (Exception ex)
            {
              //  ShowMessage(ex.Message);
            }
        }
      //  private announce a = new announce();
        protected void btnUpdateAnnounce_Click(object sender, EventArgs e)
        {
            try
            {


                announce a = new announce();
                a.announce_id = Convert.ToInt32(annoID.Value);
                DateTime? fromDate = null;
                DateTime? ToDate = null;
                if (!string.IsNullOrEmpty(TextBox2.Text))
                {
                    DateTime rs;
                    bool canParseDlvDate = DateTime.TryParseExact(TextBox2.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out rs);
                    if (canParseDlvDate)
                        fromDate = rs;
                }
                if (!string.IsNullOrEmpty(TextBox3.Text))
                {
                    DateTime rs;
                    bool canParseDlvDate = DateTime.TryParseExact(TextBox3.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out rs);
                    if (canParseDlvDate)
                        ToDate = rs;
                }


                a.valid_from = fromDate;
                a.valid_to = ToDate;
                a.type = int.Parse(DropDownList1.SelectedValue);
                a.announce_text = TextBox1.Text;


                using (var sv = new ServiceClient())
                {
                    a = sv.UpdateAnnounce(a);
                }
                LoadAnnounceList(true);

            }
            catch (Exception ex)
            {
                //  ShowMessage(ex.Message);
            }
        }
        protected void grd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ORDER_DOWNLOAD")
            {
                Boolean platformSupportsTls12 = false;
                foreach (SecurityProtocolType protocol in Enum.GetValues(typeof(SecurityProtocolType)))
                {
                    // Console.WriteLine(protocol.GetHashCode());
                    if (protocol.GetHashCode() == 3072)
                    {
                        platformSupportsTls12 = true;
                    }
                }
               
                if (!ServicePointManager.SecurityProtocol.HasFlag((SecurityProtocolType)3072))
                {
                    if (platformSupportsTls12)
                    {
                        //    Console.WriteLine("Platform supports Tls12, but it is not enabled. Enabling it now.");
                        ServicePointManager.SecurityProtocol |= (SecurityProtocolType)3072;
                    }
                    else
                    {
                        //    Console.WriteLine("Platform does not supports Tls12.");
                    }
                }

             
            }else if (e.CommandName== "pop")
            {
                using (var sv = new ServiceClient())
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                  //  a = new announce();
                 //   a.announce_id = index;
                    // Retrieve the row that contains the button clicked
                    // by the user from the Rows collection.
               //     GridViewRow row = grd.Rows[index];
                    Announces = sv.SelectAnnounceForList(index);
                    List<DropDownValue> InvoiceTypeList = new List<DropDownValue>();
                    // InvoiceTypeList.Insert(0, new DropDownValue() { Code = "", Name = "-----All-----" });
                    InvoiceTypeList.Insert(0, new DropDownValue() { Code = "1", Name = "Main" });
                    InvoiceTypeList.Insert(1, new DropDownValue() { Code = "2", Name = "Inside" });
                    DropDownList1.SelectedIndex = 1;
                    DropDownList1.DataSource = InvoiceTypeList;
                    DropDownList1.DataBind();
                    annoID.Value = index+"";



                    TextBox3.Text = Announces[0].valid_to.Value.ToString("dd/MM/yyyy");
                    TextBox2.Text = Announces[0].valid_from.Value.ToString("dd/MM/yyyy");
                    TextBox1.Text = Announces[0].announce_text;
                    //  TextBox3.Text= row..t

                    //  Panel2.Visible = true;
                    Panel2.DataBind();

                    mpeUpdate.Show();

                    btnAttachmentCancel2.Focus();
                }
            }
        }


        protected void myGrid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button pop = e.Row.FindControl("pop") as Button;
                pop.CommandArgument = e.Row.RowIndex.ToString();

            }
        }

        protected void pop_Click(object sender, EventArgs e)
        {
            try
            {
                //lblErrorUpload.Text = string.Empty;
                //lblAcceptExt.Text = Config.WEB_UP_EXTS;
                //lblAttachMaxFile.Text = Config.WEB_UP_MAX_FILE.ToString();
                //lblAttachMaxSize.Text = Config.WEB_UP_MAX_SIZE.ToString();

                //  LoadAttachments();
           
            }
            catch (Exception ex)
            {
                //  ShowMessage(ex.Message);
            }

        }
    }
}