﻿using Hmc.Smo.Common;
using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class Dashboard : BasePage
    {
        private const string CSS_SORT = "img-sort";
        private const string CTRL_chkSelect = "chkSelect";
        private const string FORMAT_FILENAME = "SmartOrder_{0}.xlsx";
        private const string FORMAT_FILENAME_DATETIME = "yyyyMMddHHmm";
        public static string CUSNO = "";
        #region property


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                using (var sv = new ServiceClient())
                {

                  //  CUSNO = CurrentCustomer.CustomerNo;
                    //var res = sv.DashboardValue("");
                    if (CurrentUser.Role=="3")
                    {
                        var res = sv.DashboardValue("");
                        //var res = sv.DashboardValue(CurrentCustomer.CustomerNo);
                        CUSNO = "";
                        int cnt = 0;
                        DateTime cur = DateTime.Now;
                        txtCurrentMonthName.Text = cur.ToString("MMM");
                        txtPreViousMonthName.Text = cur.AddMonths(-1).ToString("MMM");
                        txtNextName.Text = cur.AddMonths(1).ToString("MMM");

                        foreach (SpDashboard_Result value in res)
                        {
                            if (cnt == 0) {
                                txtPreViousMonth.Text = value.totalqty.ToString("#,##0.0"); txtPreViousMonthName.Text = value.MonthName;
                                Label1.Text =  value.year_str;
                            }
                            if (cnt == 1) {
                                txtCurrentMonth.Text = value.totalqty.ToString("#,##0.0"); txtCurrentMonthName.Text = value.MonthName;
                                Label2.Text = value.year_str;
                            }

                            if (cnt == 2) {
                                txtNext.Text = value.totalqty.ToString("#,##0.0"); txtNextName.Text = value.MonthName;
                                Label3.Text = value.year_str;
                            }
                            cnt++;

                        }
                    }
                    else
                    {
                        CUSNO = CurrentCustomer.CustomerNo;
                        var res = sv.DashboardValue(CurrentCustomer.CustomerNo);
                        //var res = sv.DashboardValue(CurrentCustomer.CustomerNo);
                        int cnt = 0;
                        DateTime cur = DateTime.Now;
                        txtCurrentMonthName.Text = cur.ToString("MMM");
                        txtPreViousMonthName.Text = cur.AddMonths(-1).ToString("MMM");
                        txtNextName.Text = cur.AddMonths(1).ToString("MMM");

                        foreach (SpDashboard_Result value in res)
                        {
                            if (cnt == 0)
                            {
                                txtPreViousMonth.Text = value.totalqty.ToString("#,##0.0"); txtPreViousMonthName.Text = value.MonthName;
                                Label1.Text = value.year_str;
                            }
                            if (cnt == 1)
                            {
                                txtCurrentMonth.Text = value.totalqty.ToString("#,##0.0"); txtCurrentMonthName.Text = value.MonthName;
                                Label2.Text = value.year_str;
                            }

                            if (cnt == 2)
                            {
                                txtNext.Text = value.totalqty.ToString("#,##0.0"); txtNextName.Text = value.MonthName;
                                Label3.Text = value.year_str;
                            }
                            cnt++;

                        }
                    }
                }
                using (var sv = new ServiceClient())
                {

                   

                    //if (CurrentUser.Role == "3")
                    //{
                    //    var rchart = sv.DashboardDeliveryValue("");
                    //}
                    //else
                    //{
                    //    var rchart = sv.DashboardDeliveryValue(CurrentCustomer.CustomerNo);
                    //}
                    //List<object> iData = new List<object>();
                    //Creating sample data    
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Month", System.Type.GetType("System.String"));
                    dt.Columns.Add("Qty", System.Type.GetType("System.Decimal"));



                    var ann = sv.Announce();
                    foreach (SpAnnounce_Result value in ann)
                    {
                        txtAnnounce.Text = value.announce_text;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        [WebMethod]
        public static  string GetChart(string val)
        {
            string jsonString = "";
            try
            {
                StringBuilder sb = new StringBuilder();
                using (var sv = new ServiceClient())
                {
                        List<SpDashboardDelivery_Result>  rchart = sv.DashboardDeliveryValue(CUSNO);
                        jsonString = rchart.ToJSON();
                        //var rchart = sv.DashboardDeliveryValue();
                                        
                    //sb.Append("[");
                    //foreach (var item in rchart)
                    //{
                    //    sb.Append("{");
                    //    System.Threading.Thread.Sleep(50);
                    //    string color = String.Format("#{0:X6}", new Random().Next(0x1000000));
                    //    sb.Append(string.Format("text :'{0}', value:{1}, color: '{2}'", item.MonthName, item.TotalQty, color));
                    //    sb.Append("},");
                    //}
                    //sb = sb.Remove(sb.Length - 1, 1);
                    //sb.Append("]");
                  //  jsonString = rchart.ToJSON();
                }
              //  return Json(jsonString, JsonRequestBehavior.AllowGet);
                return jsonString;
            }
            catch (Exception ex)
            {
                // ShowMessageBase(ex.Message);
                return "";
            }
            
                        
              
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
               // LoadOrders();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

       

    }
    public static class JSONHelper
    {
        public static string ToJSON(this object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj);
        }

        public static string ToJSON(this object obj, int recursionDepth)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.RecursionLimit = recursionDepth;
            return serializer.Serialize(obj);
        }
    }
}