﻿using AjaxControlToolkit;
using Hmc.Smo.Web.DBService;
using System;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class Site : System.Web.UI.MasterPage
    {
        #region IsShowCriteria

        public bool IsShowCriteria
        {
            get
            {
                var isShow = Session["IsShowCriteria"] as bool?;
                return isShow.HasValue && isShow.Value;
            }
        }

        public User CurrentUser
        {
            get { return Session["CurrentUser"] as User; }
            set { Session["CurrentUser"] = value; }
        }
        public Customer CurrentCustomer
        {
            get { return Session["CurrentCustomer"] as Customer; }
            set { Session["CurrentCustomer"] = value; }
        }

        public bool HasLogin
        {
            get { return CurrentUser != null; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //if(CurrentUser==null)
                //{
                //    Response.Redirect("Logon.aspx", true);
                //}
                btnUserFullname.Text = HasLogin ? (CurrentUser.FullName.Length > 20 ? CurrentUser.FullName.Substring(0, 20) : CurrentUser.FullName) : string.Empty;

                if (CurrentUser != null)
                {
                    if (CurrentCustomer != null)
                        btnCompany.Text = HasLogin ? (CurrentCustomer.CustomerName.Length > 20 ? CurrentCustomer.CustomerName.Substring(0, 20) : CurrentCustomer.CustomerName) : string.Empty;
                    else
                        ShowMessgeBox(string.Format("There is no customer {0} in database.", CurrentUser.CustomerNo));
                }

                if (CurrentUser != null)
                {
                    if (CurrentUser.IsAdmin)
                    {
                        menu.Items.Clear();
                        menu.Items.Add(new MenuItem("Template", "", "", "~/OrderTemplateList.aspx"));
                    }
                    //else
                    //{
                    //menu.Items.Clear();
                    //    menu.Items.Add(new MenuItem("Orders", "", "", "~/OrderList.aspx"));
                    //    menu.Items.Add(new MenuItem("Shipment Balance", "", "", "~/ShipmentBalanceList.aspx"));
                    //    menu.Items.Add(new MenuItem("Shipment Detail", "", "", "~/ShipmentDetailList.aspx"));
                    //}
                }

                pnlUserInfo.DataBind();

                if (HasLogin) pnlCriteria.DataBind();
            }
            catch (Exception ex)
            {
                ShowMessgeBox(ex.Message);
            }
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();
                Response.Redirect("Login.aspx", false);
            }
            catch (Exception ex)
            {
                ShowMessgeBox(ex.Message);
            }
        }

        public void ShowMessgeBox(string text)
        {
            var pnlMsg = this.FindControl("pnlMsg") as Panel;
            if (pnlMsg != null)
            {
                var txtMsg = pnlMsg.FindControl("txtMsg") as TextBox;
                if (txtMsg != null)
                    txtMsg.Text = text;
            }

            var mpeMsg = this.FindControl("mpeMsg") as ModalPopupExtender;
            if (mpeMsg != null)
                mpeMsg.Show();
        }
    }
}