﻿using Hmc.Smo.Common;
using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class Report1 : BasePage
    {
        private const string CSS_SORT = "img-sort";
        private const string CTRL_chkSelect = "chkSelect";
        private const string FORMAT_FILENAME = "SmartOrder_{0}.xlsx";
        private const string FORMAT_FILENAME_DATETIME = "yyyyMMddHHmm";
        #region property

        public List<string> SearchCriterias
        {
            get { return Session["SearchCriterias"] as List<string>; }
            set { Session["SearchCriterias"] = value; }
        }
        public List<OrderForHMC> Orders
        {
            get { return Session["Orders"] as List<OrderForHMC>; }
            set { Session["Orders"] = value; }
        }
        public DataTable OrdersDT
        {
            get { return Session["OrdersDT"] as DataTable; }
            set { Session["OrdersDT"] = value; }
        }

        #endregion
        public static string   CUSNO ="";
        public static string ROLE;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
                CUSNO = this.CurrentUser.CustomerNo;
                ROLE = CurrentUser.Role;
                //CurrentUser.CustomerNo;
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }
        [WebMethod]
        public static string R1GetChart(string cType)
        {
            string jsonString = "";
            try
            {
                
                using (var sv = new ServiceClient())
                {
                    if (cType.Equals("Q"))
                    {
                        if (ROLE=="3")
                        {
                            CUSNO = "";
                        }   
                        var rchart = sv.DashboardReport1Quarter(CUSNO);
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        jsonString = serializer.Serialize(rchart);
                       

                    
                    }
                    else
                    {
                        if (ROLE == "3")
                        {
                            CUSNO = "";
                        }
                        var rchart = sv.DashboardReport1(CUSNO);
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        jsonString = serializer.Serialize(rchart);
                    }
                  


                   // jsonString = rchart.ToJSON();
                }
                //  return Json(jsonString, JsonRequestBehavior.AllowGet);
                return jsonString;
            }
            catch (Exception ex)
            {
                
                
                // ShowMessageBase(ex.Message);
                return "";
            }



        }
       

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
               // LoadOrders();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

       

    }

}