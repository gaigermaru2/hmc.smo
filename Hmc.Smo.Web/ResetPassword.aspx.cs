﻿using System;
using Hmc.Smo.Web.DBService;
using Hmc.Smo.Common;

namespace Hmc.Smo.Web
{
    public partial class ResetPassword : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterOnSubmitStatement(this.GetType(), "val", "fnOnUpdateValidators();");
            if (IsPostBack) return;
            try
            {
                IsShowCriteria = false;

                cpvNewPassConfirm.ErrorMessage = Msg.MSG_ERR_PASS_MUST_SAME;

                DataBind();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                using (var sv = new ServiceClient())
                {
                    var cur = PasswordHelper.EncodePasswordToBase64(txtCurPass.Text);
                    var user = sv.UserAuthentication(txtUsername.Text, cur);

                    if (user == null)
                    {
                        ShowMessageBase(Msg.IncorrectUserPassword);
                        txtUsername.Focus();
                    }
                    else
                    {
                        user.Password = PasswordHelper.EncodePasswordToBase64(txtNewPass.Text);
                        user.IsChangePass = false;
                        sv.UpdateUser(user);
                        //ShowMessgeBox(Msg.YourPasswordUpdated);

                        Response.Redirect("Logon.aspx", false);
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }
    }
}