﻿using Hmc.Smo.Common;
using Hmc.Smo.Web.DBService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

namespace Hmc.Smo.Web
{
    public partial class Report3 : BasePage
    {
        private const string CSS_SORT = "img-sort";
        private const string CTRL_chkSelect = "chkSelect";
        private const string FORMAT_FILENAME = "SmartOrder_{0}.xlsx";
        private const string FORMAT_FILENAME_DATETIME = "yyyyMMddHHmm";
        #region property

        public List<string> SearchCriterias
        {
            get { return Session["SearchCriterias"] as List<string>; }
            set { Session["SearchCriterias"] = value; }
        }
        public List<OrderForHMC> Orders
        {
            get { return Session["Orders"] as List<OrderForHMC>; }
            set { Session["Orders"] = value; }
        }
        public DataTable OrdersDT
        {
            get { return Session["OrdersDT"] as DataTable; }
            set { Session["OrdersDT"] = value; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HasSession) return;
            if (IsPostBack) return;

            try
            {
               
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
               // LoadOrders();
            }
            catch (Exception ex)
            {
                ShowMessageBase(ex.Message);
            }
        }

       

    }
}