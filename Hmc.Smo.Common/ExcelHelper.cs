﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.IO;
using System.Data.OleDb;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Hmc.Smo.Common
{
    [Serializable]
    public class OrderItemExcel
    {
        public int Line { get; set; }
        public string SoldTo { get; set; }
        public string ShipTo { get; set; }
        public string PONo { get; set; }
        public string DeliveryDate { get; set; }
        public string ProductNo { get; set; }
        public string ProductText { get; set; }
        public string PackageSize { get; set; }
        public string MaterialNo { get; set; }
        public string UnitPrice { get; set; }
        public string PackageSizeCode { get; set; }
        public string Quantity { get; set; }
        public string ErrorMessage { get; set; }
        public string PaymentTermCode { get; set; }
        public string PaymentTermText { get; set; }
        public string PODate { get; set; }
        public string ShipToText { get; set; }
    }
    public class ExcelHelper
    {
        #region new

        public static string GetPoItemsFromExcel(string fullPath, out List<OrderItemExcel> items)
        {
            items = new List<OrderItemExcel>();
            var dt = LoadDataTableFromExcel(fullPath);
            List<DataRow> list = dt.AsEnumerable().ToList();

            for (int i = 0; i < list.Count; i++)
            {
                var mat = new OrderItemExcel()
                {
                    Line = i + 2,

                    PONo = Convert.ToString(list[i][2]),//c
                    PODate = Convert.ToString(list[i][3]),//d

                    Quantity = Convert.ToString(list[i][6]),//g
                    UnitPrice = Convert.ToString(list[i][7]),//h
                    DeliveryDate = Convert.ToString(list[i][8]),//i

                    ProductNo = Convert.ToString(list[i][10]),//k
                    PackageSizeCode = Convert.ToString(list[i][11]),//l
                    MaterialNo = Convert.ToString(list[i][12]),//m

                    ShipTo = Convert.ToString(list[i][14]),//o
                    PaymentTermCode = Convert.ToString(list[i][15]),//p
                };

                items.Add(mat);
            }

            return null;
        }

        private static DataTable LoadDataTableFromExcel(string filePath)
        {
            bool hasHeader = true;

            using (var pck = new ExcelPackage())
            {
                using (var stream = File.OpenRead(filePath))
                {
                    pck.Load(stream);
                }
                var ws = pck.Workbook.Worksheets.First();
                DataTable tbl = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                }
                var startRow = hasHeader ? 2 : 1;
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                    DataRow row = tbl.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
                return tbl;
            }






            return null;










            //FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            //var last = filePath[filePath.Length - 1];

            //IExcelDataReader excelReader;

            //if (last.ToString().ToUpper() == "X")
            //    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
            //    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            //else
            //    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
            //    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

            //3. DataSet - The result of each spreadsheet will be created in the result.Tables
            //DataSet result = excelReader.AsDataSet();
            //...
            //4. DataSet - Create column names from first row
            //excelReader.IsFirstRowAsColumnNames = true;
            //DataSet result = excelReader.AsDataSet();
            //excelReader.Close();

            //return result;

            ////5. Data Reader methods
            //while (excelReader.Read())
            //{
            //    //excelReader.GetInt32(0);
            //}

            ////6. Free resources (IExcelDataReader is IDisposable)
        }


        #endregion
        public static byte[] DumpExcel(DataTable tbl, string cellHeader, string worksheetName)
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(worksheetName);

                ws.Cells["A1"].LoadFromDataTable(tbl, true);

                using (ExcelRange rng = ws.Cells[cellHeader])
                {
                    rng.Style.Font.Bold = true;
                    rng.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(79, 129, 189));
                    rng.Style.Font.Color.SetColor(System.Drawing.Color.White);
                }

                return pck.GetAsByteArray();
            }
        }
    }
}
