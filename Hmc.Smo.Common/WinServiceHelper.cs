﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Hmc.Smo.Common
{
    public class WinServiceHelper
    {
        private RunType _runType;
        private string _sSource;
        private string _sLog;

        public WinServiceHelper(RunType runType, string sSource)
        {
            _runType = runType;

            _sSource = sSource;
            _sLog = "Application";
        }

        #region WriteLog

        public void WriteLogError(string eventText) { WriteLog(eventText, EventLogEntryType.Error, null); }
        public void WriteLog(string eventText) { WriteLog(eventText, null, null); }
        public void WriteLog(string eventText, EventLogEntryType eventType) { WriteLog(eventText, eventType, null); }
        public void WriteLog(string eventText, int eventId) { WriteLog(eventText, null, eventId); }
        public void WriteLog(string eventText, EventLogEntryType? eventType, int? eventId)
        {
            if (_runType == RunType.Service)
            {
                if (!EventLog.SourceExists(_sSource))
                    EventLog.CreateEventSource(_sSource, _sLog);

                EventLog.WriteEntry(_sSource,
                    eventText,
                    eventType.HasValue ? eventType.Value : EventLogEntryType.Information,
                    eventId.HasValue ? eventId.Value : 0);
            }
            else if (_runType == RunType.Console)
            {
                Console.WriteLine(eventText);
            }
        }

        #endregion

    }
}
