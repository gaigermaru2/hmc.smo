﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hmc.Smo.Common
{
    public class TimeMili
    {
        public const int SECOND = 1000;
        public const int MINUTE = 60 * SECOND;
        public const int HOUR = 60 * MINUTE;
        public const int DAY = 24 * HOUR;
    }

    public static class Const
    {
        private static Dictionary<string, string> _reasonPickers;
        public static Dictionary<string, string> ReasonPickers
        {
            get
            {
                _reasonPickers = new Dictionary<string, string>();
                _reasonPickers.Add(ReasonPicker.Others, "Others");
                _reasonPickers.Add(ReasonPicker.WrongGrade, "Wrong Grade");
                _reasonPickers.Add(ReasonPicker.WrongPrice, "Wrong Price");
                _reasonPickers.Add(ReasonPicker.WrongQuantity, "Wrong Quantity");
                return _reasonPickers;
            }
        }

        public const string URL_TO_CURRENT = "OrderDetail.aspx?OrderNo=";
    }

    public static class DataFormat
    {
        public const string DateTime = "dd/MM/yyyy HH:mm:ss";
        public const string Date = "dd/MM/yyyy";
        public const string Integer = "#,##0";
        public const string Decimal = "#,###,##0.00";
    }
    public static class Msg
    {
        public const string PriceMustBiggerZero = "Price must be bigger than zero.";
        public const string PriceMustBeDecimal = "Price must be decimal.";
        public const string QtyPackageSizeDoesNotMatch = "The quantity and the package size do not match.";
        public const string Max10LinePerOrder = "Maximum line item on each order is 10";
        public const string DoYouWantToSubmitToApproval = "Do you want to submit to your approver?";
        public const string MinOrderQty = "The minimum order is {0:#,##0} kg. and maximum is {1:#,##0} kg.";
        public const string DoYouWantToSubmitToHMC = "Do you want to submit to HMC?";
        public const string DoYouWantToSubmitToSAP = "Do you want to submit to SAP?";
        public const string IncorrectUserPassword = "Incorrect user or password.";
        public const string UserIsLocked = "User is locked. Please notify the administrator.";
        public const string UserIsExpired = "User is expired. Please notify the administrator.";
        public const string YourPasswordUpdated = "Your password have been updated.";

        public const string MSG_ERR_EXCEPTION = "EXC Err: {0}";
        public const string MSG_SUCC_CREATE_UPDATE = "{0} {1} has been created or updated";
        public const string FORMAT_TEXT_MONITOR = "Monitoring the System: {0}";

        public const string MSG_SUCC_CANCEL_TH = "ยกเลิกเรียบร้อย";
        public const string MSG_SUCC_CANCEL_EN = "Order has been cancelled";

        public const string MSG_SUCC_REJECT_TH = "ใบสั่งซื้อถูกปฏิเสธ";
        public const string MSG_SUCC_REJECT_EN = "Order has been rejected";

        public const string MSG_ORDER_VALID_TH = "การสั่งซื้อนี้จะสมบูรณ์เมื่อได้รับการอนุมัติจากผู้มีอำนาจ ภายใน {0} เวลา 23:59 น.";
        public const string MSG_ORDER_VALID_EN = "The order will be complete when it is approved by the authorized person within {0} at 23:59";

        public const string MSG_SUCC_SAVE_TH = "บันทึกเรียบร้อย";
        public const string MSG_SUCC_SAVE_EN = "Order has been saved";

        public const string MSG_ERR_PASS_MUST_SAME = "New Password and Confirm new password must be same";

        public const string CONFIRM_DEL_LINE = "Are you sure you want to remove this item?";
    }

    public static class Role
    {
        public const string CustomerUser = "1";
        public const string CustomerManager = "2";
        public const string HMCSaleCo = "3";
    }

    public static class OrderStatusCode
    {
        public const string All = "0";
        public const string Draft = "1";
        public const string DraftAndWaitingForApproveAndRejected = "2";
        public const string WaitingForApproval = "3";
        public const string SubmittedToHMC = "4";
        public const string SubmittedToSAP = "5";
        public const string QuotationApprove = "6";//old is Completed
        public const string SaleOrderComplete = "61";//new
        public const string Canceled = "7";
        public const string Rejected = "8";
        public const string New = "9";
    }

    public static class ReasonPicker
    {
        public const string Others = "1";
        public const string WrongGrade = "2";
        public const string WrongPrice = "3";
        public const string WrongQuantity = "4";

    }

    public static class Color
    {
        public const string ERROR_COLOR = "#FFAAAA";
        public const string NORMAL_COLOR = "#FFFFFF";
    }
}
